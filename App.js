/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  DeviceEventEmitter
} from 'react-native';
import {  StyleProvider } from 'native-base';
import AppNavigator from './src/components/navigator';
import FlashMessage from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import { FetchApi } from './src/components/api';
import getTheme from './src/assets/themes/components';
import appColor from './src//assets/themes/variables/appColor';
import material from './src//assets/themes/variables/material';



/************************************ Not allow Font Scaling when Phone Setting font size ***********************************/
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

/************************************ Not allow Text input font Scaling when Phone Setting font size  ***********************************/
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingFetch: true,
      mmidToken: '',
      dataProfile: [],
      statusData: []
    };
  }

  /****************************************************** componentDidMount ********************************************************/
  componentDidMount() {

    /************************************ MMID_TOKEN_LOGIN AsyncStorage ***************************************/
    AsyncStorage.getItem("MMID_TOKEN_LOGIN").then(MMIDStorageRes => {
      //console.log(MMIDStorageRes, '----------------MMIDStorageRes')
      this.setState({
        mmidToken: MMIDStorageRes
      })
      this.fetchProfileApi();
      this.fetchStatusApi();
      setInterval(this.fetchStatusApi, 2000, console.log('status-----'));
      setInterval(this.fetchProfileApi, 10000, console.log('fetchProfileApi-----'));
    })
  }

  componentWillUnmount() {
    console.log('componentWillUnmount----');
    this.fetchStatusApi.remove();
    this.fetchProfileApi.remove();
  }

  /****************************************************** fetchProfileApi ********************************************************/
  fetchProfileApi = () =>  {
    let path = "member_profile";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          dataProfile: fetchData
        })
      })
  }

  /****************************************************** fetchStatus ********************************************************/
  fetchStatusApi = () => {
    let path = "member_status_color";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          statusData: fetchData[0].memberStatus,
          isLoadingFetch: false
        })
      })
  }



  render() {
    const { isLoadingFetch, mmidToken, dataProfile, statusData } = this.state;

    return (
      <StyleProvider style={getTheme(appColor, material)}>
      <Fragment >
        {/* <SafeAreaView style={{ backgroundColor: '#000',  flex: 1,}}></SafeAreaView> */}
        <StatusBar barStyle="light-content" backgroundColor="#000" />

        <AppNavigator screenProps={{
          loadingStatus: isLoadingFetch, mmidToken: mmidToken, profile: dataProfile[0], statusIcon: statusData,
          // fetchStatusApi: this.fetchStatusApi
        }}
        />
        <FlashMessage position="bottom" />
      </Fragment>
      </StyleProvider>
    )
  }
}


const styles = StyleSheet.create({

});

export default App;
