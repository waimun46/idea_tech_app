import React, { Component } from 'react';
import {
  StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, ImageBackground, ActivityIndicator,
  Modal, PanResponder, Alert
} from 'react-native';
import { Button, Text } from 'native-base';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import ANT from 'react-native-vector-icons/AntDesign';
import Carousel from 'react-native-snap-carousel';
import { FetchApi, FetchApiNoParam } from '../../api';
import ImageZoom from 'react-native-image-pan-zoom';
import RNFetchBlob from 'rn-fetch-blob';

const slideWidth = 180;
const itemWidth = slideWidth;
const itemHeight = 240;

const slideWidthReviews = 300;
const itemWidthReviews = slideWidthReviews;
const itemHeightReviews = 170;

const sliderWidth = Dimensions.get('window').width;
const { width: viewportWidth } = Dimensions.get('window');
// const itemWidth = slideWidth + horizontalMargin * 2;


// const slideWidthArticle = 250;
// const itemWidthArticle = slideWidthArticle;
const sliderWidthArticle = Dimensions.get('window').width;
const itemHeightArticle = 280;

class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Home',
  };

  constructor(props) {
    super(props);
    this.state = {
      promotionData: [],
      lasterData: [],
      galleryData: [],
      articleData: [],
      promotionProducts: [],
      promotionLoading: true,
      productsLoading: true,
      promotionProductsLoading: false,
      galleryLoading: true,
      articleLoading: true,
      renderPage: false,
      modalVisible: false,
      modalData: { image: '', title: '' },
      modalVisiblePromo: false,
      modalDataPromo: { image: '', title: '', from: '', to: '' },
      modalVisiblePP: false,
      modalDataPP: { image: '', title: '', from: '', to: '' },

    }
  }

  /************************************************************ openModalPromo ***********************************************************/
  openModalPromo(promo_id) {
    const modalDataPromo = this.state.promotionData.find(element => {
      return element.promo_id === promo_id
    })
    this.setState({
      modalVisiblePromo: true,
      modalDataPromo: {
        image: modalDataPromo.promo_path, title: modalDataPromo.promo_name, from: modalDataPromo.promo_frmDt,
        to: modalDataPromo.promo_toDt
      }
    });
  };

  /************************************************************ openModalProduct ***********************************************************/
  openModalProduct(last_id) {
    const modalData = this.state.lasterData.find(element => {
      return element.last_id === last_id
    })
    this.setState({
      modalVisible: true,
      modalData: { image: modalData.last_path, title: modalData.last_name }
    });
  };


  /************************************************************ openModalProduct ***********************************************************/
  openModalPromotionProduct(promo_id) {
    const modalDataPP = this.state.promotionProducts.find(element => {
      return element.promo_id === promo_id
    })
    this.setState({
      modalVisiblePP: true,
      modalDataPP: { image: modalDataPP.promo_path, title: modalDataPP.promo_name, from: modalDataPP.promo_frmDt, to: modalDataPP.promo_toDt }
    });
  };

  /************************************************************ closeImage ***********************************************************/
  closeModal = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false,
      modalVisiblePromo: false,
      modalVisiblePP: false,
    })
  }

  /******************************************************** componentDidMount **************************************************/
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
    this.fetchPromotion();
    this.fetchLasterProducts();
    this.fetchGallery();
    this.fetchArticle();
    this.fetchPromotionProducts();
  }

  /******************************************************** fetchPromotion **************************************************/
  fetchPromotion() {
    let path = "promotion_home";
    // let parameters = { limit: '10',}
    let ApiData = FetchApiNoParam(path);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          promotionData: fetchData,
          promotionLoading: false
        })
      })
  }

  /******************************************************** fetchLesterProducts **************************************************/
  fetchLasterProducts() {
    let path = "latest_product_home";
    // let parameters = { limit: '10', offset: 1 }
    let ApiData = FetchApiNoParam(path);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          lasterData: fetchData,
          productsLoading: false
        })
      })
  }

  /******************************************************** fetchPromotionProducts **************************************************/
  fetchPromotionProducts() {
    let path = "promotion_homeList";
    // let parameters = { limit: '10', offset: 1 }
    let ApiData = FetchApiNoParam(path);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          promotionProducts: fetchData,
          promotionProductsLoading: false
        })
      })
  }

  /******************************************************** fetchGallery **************************************************/
  fetchGallery() {
    let path = "gallery_home";
    // let parameters = { limit: '10', offset: 1 }
    let ApiData = FetchApiNoParam(path);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          galleryData: fetchData,
          galleryLoading: false
        })
      })
  }

  /******************************************************** fetchArticle **************************************************/
  fetchArticle() {
    let path = "articles_list_home";
    // let parameters = { limit: '10', offset: 1 }
    let ApiData = FetchApiNoParam(path);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          articleData: fetchData,
          articleLoading: false
        })
      })
  }

  /******************************************************** _renderItemPromotion **************************************************/
  _renderItemPromotion = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => this.openModalPromo(item.promo_id)}>
        <View style={styles.slideBanner}>
          <View style={styles.bannerwarp}>
            <Image source={{ uri: item.promo_path }} style={styles.bannerSty} />
            {/* <View style={styles.bannertextwarp}>
            <View style={{ width: '50%', flexDirection: 'row', }}>
              <Text numberOfLines={1} style={[styles.bannertitle2, { marginRight: 0 }]}>{item.promo_name}</Text>
            </View>
            <Text numberOfLines={1} style={[styles.bannertitle, { width: '50%', textAlign: 'right', fontSize: 14 }]}>
              {item.promo_frmDt} - {item.promo_toDt}
            </Text>
          </View> */}
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  /******************************************************** _renderItemLasterProduct **********************************************************/
  _renderItemLasterProduct = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <TouchableOpacity onPress={() => this.openModalProduct(item.last_id)}>
        <View style={[styles.slide]}>
          <View style={[styles.exampleContainer,]}>
            <Image source={{ uri: item.last_path }} style={styles.imageSty} />
            {/* <Text numberOfLines={1} style={styles.imagetitle}>{item.last_name}</Text> */}
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  /******************************************************** _renderItemPromotionProduct **********************************************************/
  _renderItemPromotionProduct = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <TouchableOpacity onPress={() => this.openModalPromotionProduct(item.promo_id)}>
        <View style={styles.slide}>
          <View style={styles.exampleContainer}>
            <Image source={{ uri: item.promo_path }} style={styles.imageSty} />
            {/* <View style={{backgroundColor: '#000'}}/> */}
            {/* <Text numberOfLines={1} style={[styles.imagetitle,{position: 'absolute', }]}>{item.promo_name}</Text> */}
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  /******************************************************** _renderItemGallery **********************************************************/
  _renderItemGallery = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <TouchableOpacity onPress={() => navigateActions('galleryDetails', { itemid: item.photo_id })}>
        <View style={styles.slideReview}>
          <View style={styles.exampleContainer}>
            <ImageBackground source={{ uri: item.photo_path }} style={styles.imageSty} imageStyle={{ borderRadius: 8, }} >
              <View style={styles.overlayReview} />
            </ImageBackground>
            <View style={styles.reviewwarp}>
              <View style={styles.reviewwarpcontent}>
                <View style={{ width: '50%', flexDirection: 'row', }}>
                  <Text numberOfLines={1} style={styles.bannertitle2}>{item.title}</Text>
                </View>
                <Text numberOfLines={1} style={[styles.bannertitle, { width: '50%', textAlign: 'right', fontSize: 14 }]}>{item.create_dt}</Text>
              </View>
              <View style={styles.flightwarp}>
                <ANT name="heart" size={20} style={{ color: '#ff5d50', marginRight: 5 }} />
                <Text style={[styles.reviewtitle, { textAlign: 'right', marginRight: 10, textTransform: 'uppercase' }]}>{item.totalLike}</Text>
                <MCI name="message-reply-text" size={20} style={{ color: '#fff', marginRight: 5 }} />
                <Text style={[styles.reviewtitle, { textAlign: 'right', textTransform: 'uppercase' }]}>{item.totalComment}</Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  /******************************************************** _renderItemArticle **********************************************************/
  _renderItemArticle = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <TouchableOpacity onPress={() => navigateActions('articlecontent', { articleID: item.article_id })}>
        <View style={styles.slideArticle}>
          <View style={styles.exampleContainer}>
            <ImageBackground source={{ uri: item.article_path }} style={styles.imageSty} imageStyle={{ borderRadius: 8, }} >
              <View style={styles.overlayArticle} />
            </ImageBackground>
            <View style={styles.articleTextWarp}>
              <Text numberOfLines={1} style={styles.imagetitle2}>{item.article_title}</Text>
              <Text note numberOfLines={1} style={styles.imagetitle3}>{item.create_dt}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  /************************************************************ goEnquiry ***********************************************************/
  goEnquiry(image, title) {
    const navigateActions = this.props.navigation.navigate;
    const token = this.props.screenProps.mmidToken;
    let url = 'https://apps.idealtech.com.my/api/member_enquiry_add.php?';
    let imagePath = image;
    let itemTitle = title
    let that = this;

    this.setState({
      modalVisible: false,
      modalVisiblePP: false,
    })

    //console.log(token, 'feedback token')

    RNFetchBlob.fetch('POST', url, {
      Authorization: "Bearer access-token",
      otherHeader: "foo",
      'Content-Type': 'multipart/form-data',
    }, [
        { name: 'MMID', data: token },
        { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
        { name: 'message', data: itemTitle },
        { name: 'item', data: imagePath }
      ]).then((res) => res.json())
      .then((myJson) => {
        //console.log('myJson', myJson);
        if (myJson[0].status === 1) {
          navigateActions('enquiry')
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => that.setState({
                  modalVisible: false,
                })
              },
            ],
            { cancelable: false },
          );
        }
      }).catch((err) => {
        console.log('error upload img', err);
      })
  }





  render() {
    const { promotionData, lasterData, galleryData, articleData, promotionLoading, productsLoading, galleryLoading, articleLoading,
      renderPage, modalVisible, modalData, modalVisiblePromo, modalDataPromo, promotionProductsLoading, promotionProducts, modalDataPP,
      modalVisiblePP
    } = this.state;
    const navigateActions = this.props.navigation.navigate;
    const token = this.props.screenProps.mmidToken
    // console.log('promotionData----', promotionData)
    // console.log('lasterData----', lasterData)
    // console.log('articleData----', articleData)
    // console.log('articleData----', articleData.length)
    // console.log('promotionProductsLoading-----home', promotionProducts)


    return (
      renderPage &&
      <View>
        <ScrollView>

          {/************************************************* Promotion slider **************************************************/}
          <View style={{ marginBottom: 10 }}>
            {
              promotionLoading ?
                <View style={{ height: 200, justifyContent: 'center' }}>
                  <ActivityIndicator size="large" color="#ccc" />
                </View>
                :
                <Carousel
                  data={promotionData}
                  renderItem={this._renderItemPromotion}
                  sliderWidth={viewportWidth}
                  itemWidth={viewportWidth}
                  // layout={'default'}
                  // firstItem={0}
                  autoplay={true}
                  autoplayDelay={3000}
                  autoplayInterval={3000}
                  loop={true}
                  enableMomentum={true}
                />
            }
          </View>

          {/************************************************ Our Laster Products **************************************************/}
          <View >
            <View style={styles.textWarper}>
              {/* <Text style={styles.title}>Latest Products</Text> */}
              <View style={{ flexDirection: 'row', }}>
                {/* <Text style={styles.datetext}>Nov 2019 - Dec 2019</Text> */}
                <Text style={[styles.title,{width: '70%',}]}>Latest Products</Text>
                {/* <Text style={styles.datetext}></Text> */}
                <TouchableOpacity style={{ width: '30%', paddingTop: 5, }} onPress={() => navigateActions('product')}>
                  <Text style={[styles.viewall, { width: '100%' }]}>View all</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              productsLoading ?
                <View style={{ height: 260, justifyContent: 'center' }}>
                  <ActivityIndicator size="large" color="#ccc" />
                </View>
                :
                <Carousel
                  data={lasterData}
                  renderItem={this._renderItemLasterProduct}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  // inactiveSlideScale={0.90}
                  // inactiveSlideOpacity={0.50}
                  enableMomentum={true}
                  activeSlideAlignment={'start'}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  activeAnimationType={'decay'}
                  // activeAnimationOptions={{
                  //   friction: 4,
                  //   tension: 40
                  // }}
                  autoplay={true}
                  autoplayDelay={4000}
                  autoplayInterval={4000}
                  loop={true}
                />
            }
          </View>


          {/************************************************ Promotions **************************************************/}
          <View >
            <View style={styles.textWarper}>
              {/* <Text style={styles.title}>Promotion</Text> */}
              <View style={{ flexDirection: 'row', }}>
                {/* <Text style={styles.datetext}>Monthly Package Pc</Text> */}
                {/* <Text style={styles.datetext}></Text> */}
                <Text style={[styles.title, {width: '70%'}]}>Monthly Special</Text>
                <TouchableOpacity style={{ width: '30%', paddingTop: 5, }} onPress={() => navigateActions('promotion')}>
                  <Text style={[styles.viewall, { width: '100%' }]}>View all</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              productsLoading ?
                <View style={{ height: 260, justifyContent: 'center' }}>
                  <ActivityIndicator size="large" color="#ccc" />
                </View>
                :
                <Carousel
                  data={promotionProducts}
                  renderItem={this._renderItemPromotionProduct}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  // inactiveSlideScale={0.90}
                  // inactiveSlideOpacity={0.50}
                  enableMomentum={true}
                  activeSlideAlignment={'start'}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  activeAnimationType={'decay'}
                  // activeAnimationOptions={{
                  //   friction: 4,
                  //   tension: 40
                  // }}
                  autoplay={true}
                  autoplayDelay={4000}
                  autoplayInterval={4000}
                  loop={true}
                />
            }
          </View>


          {/******************************************************** Our Gallery **********************************************************/}
          <View >
            <View style={styles.textWarper}>
              <Text style={styles.title}>Our Memories</Text>
              <View style={{ flexDirection: 'row', }}>
                <Text style={styles.datetext}>Share the PC Joy here.</Text>
                <TouchableOpacity style={{ width: '30%', paddingTop: 5, }} onPress={() => navigateActions('gallery')}>
                  <Text style={[styles.viewall, { width: '100%' }]}>View all</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              galleryLoading ?
                <View style={{ height: 170, justifyContent: 'center' }}>
                  <ActivityIndicator size="large" color="#ccc" />
                </View>
                :
                <Carousel
                  data={galleryData}
                  renderItem={this._renderItemGallery}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidthReviews}
                  // inactiveSlideScale={0.99}
                  inactiveSlideOpacity={1}
                  enableMomentum={false}
                  activeSlideAlignment={'center'}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  activeAnimationType={'decay'}
                  // activeAnimationOptions={{
                  //   friction: 4,
                  //   tension: 40
                  // }}
                  autoplay={true}
                  autoplayDelay={4000}
                  autoplayInterval={4000}
                  loop={true}
                />
            }
          </View>

          {/************************************************************** Our Article ****************************************************************/}
          {
            articleData.length === 0 ? null :
              <View style={{ marginBottom: 80 }}>
                <View style={styles.textWarper}>
                  <Text style={styles.title}>Our Article</Text>
                  <View style={{ flexDirection: 'row', }}>
                    <Text style={styles.datetext}>Show some interesting</Text>
                    <TouchableOpacity style={{ width: '30%', paddingTop: 5, }} onPress={() => navigateActions('article')}>
                      <Text style={[styles.viewall, { width: '100%' }]}>View all</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                {
                  articleLoading ?
                    <View style={{ height: 250, justifyContent: 'center' }}>
                      <ActivityIndicator size="large" color="#ccc" />
                    </View>
                    :
                    <Carousel
                      data={articleData}
                      renderItem={this._renderItemArticle}
                      sliderWidth={sliderWidthArticle}
                      itemWidth={itemHeightArticle}
                      // inactiveSlideScale={0.99}
                      inactiveSlideOpacity={1}
                      enableMomentum={false}
                      activeSlideAlignment={'start'}
                      containerCustomStyle={styles.slider}
                      contentContainerCustomStyle={styles.sliderContentContainer}
                      activeAnimationType={'decay'}
                      // activeAnimationOptions={{
                      //   friction: 4,
                      //   tension: 40
                      // }}
                      autoplay={true}
                      autoplayDelay={4000}
                      autoplayInterval={4000}
                      loop={true}
                    />
                }

              </View>
          }

        </ScrollView>

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>

            <View style={{ borderBottomWidth: 3, borderBottomColor: '#fff', width: '100%', marginTop: 40 }}>
              <Text style={styles.titlemodal}>{modalData.title}</Text>
            </View>

            <ImageZoom
              cropWidth={Dimensions.get('window').width}
              cropHeight={Dimensions.get('window').height - 300}
              imageWidth={Dimensions.get('window').width}
              imageHeight={'100%'}
              pinchToZoom={true}
              panToMove={true}
              enableSwipeDown={true}
              enableCenterFocus={true}
              style={{ backgroundColor: '#000', paddingTop: 20 }}
            >
              <Image source={{ uri: modalData.image }} style={styles.imgModal} />
            </ImageZoom>

            {
              token === null ? null :
                <View style={{ marginTop: 30, }}>
                  <Button rounded primary onPress={() => this.goEnquiry(modalData.image, modalData.title)}>
                    <Text style={{ fontWeight: 'bold' }}>ASK ME ?</Text>
                  </Button>
                  {/* <Text style={{ color: '#fff', padding: 20, fontWeight: 'bold', marginTop: 20, fontSize: 16 }}>ASK ME ?</Text> */}
                </View>
            }
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisiblePromo}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>

            <View style={{ borderBottomWidth: 3, borderBottomColor: '#fff', width: '100%', marginTop: 40 }}>
              <Text style={styles.titlemodal}>{modalDataPromo.title}</Text>
            </View>

            <ImageZoom
              cropWidth={Dimensions.get('window').width}
              cropHeight={Dimensions.get('window').height - 450}
              imageWidth={Dimensions.get('window').width}
              imageHeight={Dimensions.get('window').height}
              pinchToZoom={true}
              panToMove={true}
              enableSwipeDown={true}
              enableCenterFocus={true}
              style={{ backgroundColor: '#000' }}
            >
              <Image source={{ uri: modalDataPromo.image }} style={styles.imgModal} />
            </ImageZoom>

            {
              modalDataPromo.from === "" ? null :
                <View>
                  <Text style={styles.datemodal}>{modalDataPromo.from} - {modalDataPromo.to}</Text>
                </View>
            }
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisiblePP}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>

            <View style={{ borderBottomWidth: 3, borderBottomColor: '#fff', width: '100%', marginTop: 20 }}>
              <Text style={styles.titlemodal}>{modalDataPP.title}</Text>
              {
                modalDataPP.from === "" ? null :
                  <View>
                    <Text style={styles.datemodal}>{modalDataPP.from} - {modalDataPP.to}</Text>
                  </View>
              }
            </View>

            <ImageZoom
              cropWidth={Dimensions.get('window').width}
              cropHeight={Dimensions.get('window').height - 300}
              imageWidth={Dimensions.get('window').width}
              imageHeight={'100%'}
              pinchToZoom={true}
              panToMove={true}
              enableSwipeDown={true}
              enableCenterFocus={true}
              style={{ backgroundColor: '#000', paddingTop: 20 }}
            >
              <Image source={{ uri: modalDataPP.image }} style={styles.imgModal} />
            </ImageZoom>

            {
              token === null ? null :
                <View style={{ marginTop: 30, }}>
                  <Button rounded primary onPress={() => this.goEnquiry(modalDataPP.image, modalDataPP.title)}>
                    <Text style={{ fontWeight: 'bold' }}>ASK ME ?</Text>
                  </Button>
                  {/* <Text style={{ color: '#fff', padding: 20, fontWeight: 'bold', marginTop: 20, fontSize: 16 }}>ASK ME ?</Text> */}
                </View>
            }


          </View>
        </Modal>

      </View>
    )
  }

}




const styles = StyleSheet.create({
  slider: { overflow: 'visible', marginLeft: 8 },
  exampleContainer: {
    paddingVertical: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20, position: 'relative'
  },
  slide: { width: itemWidth, height: itemHeight, },
  imagetitle: { position: 'absolute', color: '#fff', bottom: 20, padding: 10, fontWeight: 'bold', },
  articleTextWarp: { position: 'absolute', bottom: 20, padding: 10, },
  imagetitle2: {
    color: '#fff', fontWeight: 'bold', shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, fontSize: 16
  },
  imagetitle3: {
    color: '#fff', shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, fontSize: 14
  },
  sliderContentContainer: { paddingVertical: 10 },
  imageSty: { width: '100%', height: '100%', borderRadius: 8, resizeMode: 'cover' },
  textWarper: { paddingTop: 10, paddingLeft: 10, paddingRight: 10, paddingBottom: 5 },
  title: { fontSize: 25, fontWeight: 'bold', marginBottom: 5 },
  datetext: { width: '70%', color: '#8c8c8c' },
  viewall: { textAlign: 'right', width: '30%', color: '#8c8c8c', fontWeight: 'bold' },
  bannerSty: { width: '100%', height: '100%', borderRadius: 5, },
  slideBanner: {
    height: 200, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  },
  bannerwarp: { paddingVertical: 3, position: 'relative' },
  bannertextwarp: { position: 'absolute', top: 10, flexDirection: 'row', padding: 10, },
  bannertitle: {
    color: '#fff', fontWeight: 'bold', shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, fontSize: 16
  },
  bannertitle2: {
    backgroundColor: '#fff', color: '#000', paddingRight: 15, paddingLeft: 15, borderRadius: 10,
    marginRight: 10, fontWeight: 'bold', shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20, fontSize: 16
  },
  slideReview: { height: itemHeightReviews, },
  slideArticle: { height: itemHeightArticle, },
  overlayReview: { backgroundColor: '#000', opacity: 0.5, height: 150, width: '100%', borderRadius: 8 },
  overlayArticle: { backgroundColor: '#000', opacity: 0, height: 260, width: '100%', borderRadius: 8 },
  reviewwarp: { position: 'absolute', top: 15, },
  reviewwarpcontent: { flexDirection: 'row', padding: 10, },
  reviewtitle: {
    color: '#fff', fontWeight: 'bold', shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20, fontSize: 14
  },
  flightwarp: { paddingRight: 10, flexDirection: 'row', justifyContent: 'flex-end', },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative', },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: '100%', resizeMode: 'contain', },
  titlemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 22, backgroundColor: '#333',
    width: '100%', textAlign: 'center',
  },
  datemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 16, backgroundColor: '#333',
    width: '100%', textAlign: 'center', paddingTop: 0
  },






});

export default HomeScreen;


