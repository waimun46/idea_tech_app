import React, { Component } from 'react';
import { StyleSheet, View, Button, Dimensions, Image, TouchableOpacity, ScrollView, FlatList, Modal } from 'react-native';
import ANT from 'react-native-vector-icons/AntDesign';
import FA from 'react-native-vector-icons/FontAwesome';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import ETY from 'react-native-vector-icons/Entypo';
import { Thumbnail, Card, CardItem, Body, Right, Left, Text, } from 'native-base';
import bg from '../../../assets/background/bgRound.png';
import { FetchApi } from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import RNRestart from 'react-native-restart';



class ProfileScreen extends Component {
  static navigationOptions = {
    title: 'Profile',
  };

  constructor(props) {
    super(props);
    this.state = {
      isImageViewVisible: false,
      modalData: { image: '', },
      data: [],
      mmidToken: '',
      renderPage: false,
      msgUnRead: [],
      purchaseUnRead: [],
      enquiryUnRead: []


    }
  }

  /******************************************************* componentDidMount *********************************************************/
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
    /************************************ MMID_TOKEN_LOGIN AsyncStorage ***************************************/
    AsyncStorage.getItem("MMID_TOKEN_LOGIN").then(MMIDStorageRes => {
      //console.log(MMIDStorageRes, '----------------MMIDStorageRes')
      this.setState({
        mmidToken: MMIDStorageRes
      })
      this.fetchProfile();
      this.fetchMessageUnRead();
      this.fetchPurchaseUnRead();
      this.fetchEnquiryUnRead();

      const { navigation } = this.props;
      this.focusListener = navigation.addListener('didFocus', () => {
        this.fetchProfile();
        this.fetchMessageUnRead();
        this.fetchPurchaseUnRead();
        this.fetchEnquiryUnRead();
      });
    })
  }

  /******************************************************* componentWillUnmount *********************************************************/
  componentWillUnmount() {
    console.log('componentWillUnmount----');
    this.fetchProfile().remove();
    this.fetchMessageUnRead().remove();
    this.fetchPurchaseUnRead().remove();
    this.fetchEnquiryUnRead().remove();
  }

  /******************************************************* fetchProfile *********************************************************/
  fetchProfile() {
    let path = "member_profile";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        console.log(myJson, 'myJson');
        this.setState({
          data: myJson[0]
        })
      })
  }

  /******************************************************* fetchMessageUnRead *********************************************************/
  fetchMessageUnRead() {
    let path = "member_message_total_unread";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData-----fetchMessageUpdate');

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        this.setState({
          msgUnRead: myJson[0],
        })
      })
  }

  /******************************************************* fetchPurchaseUnRead *********************************************************/
  fetchPurchaseUnRead() {
    let path = "member_order_timeline_total_unread";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData-----fetchMessageUpdate');

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        this.setState({
          purchaseUnRead: myJson[0],
        })
      })
  }

    /******************************************************* fetchEnquiryUnRead *********************************************************/
    fetchEnquiryUnRead() {
      let path = "member_enquiry_total_unread";
      let parameters = { MMID: this.state.mmidToken };
      let ApiData = FetchApi(path, parameters);
      console.log(ApiData, 'ApiData-----fetchEnquiryUnRead');
  
      fetch(ApiData).then((res) => res.json())
        .then((myJson) => {
          this.setState({
            enquiryUnRead: myJson[0],
          })
        })
    }

  /******************************************************* logoutFuc *********************************************************/
  logoutFuc() {
    //console.log('logoutFuc-navi')
    AsyncStorage.removeItem('MMID_TOKEN_LOGIN');
    RNRestart.Restart()
  }

  // openImage(id) {
  //   const modalData = cardData.find(element => {
  //     return element.id === id
  //   })
  //   this.setState({
  //     isImageViewVisible: true,
  //     modalData: { image: modalData.image, }
  //   });
  // };


  // closeImage = () => {
  //   console.log('openImage')
  //   this.setState({
  //     isImageViewVisible: false
  //   })
  // }

  // _renderItem = ({ item, index }) => {
  //   const navigateActions = this.props.navigation.navigate;
  //   console.log(index, 'index')
  //   return (
  //     <View style={styles.card}>

  //       <View style={[styles.cardWarp,]}>
  //         <View style={{ flexDirection: 'row' }}>
  //           <View style={{ width: '70%' }}>
  //             <Text style={styles.warranty}>Warranty Card</Text>
  //             <Text note style={styles.noteText}>INVOICE NO.</Text>
  //             <Text style={[styles.noteContent, { color: '#000' }]}>{item.no}</Text>
  //           </View>

  //           <View style={{ width: '30%', }}>
  //             <TouchableOpacity onPress={() => this.openImage(item.id)}>
  //               <Image source={{ uri: item.image }} style={styles.invioceImg} />
  //             </TouchableOpacity>
  //           </View>
  //         </View>
  //         <View style={{ flexDirection: 'row', marginTop: 15 }}>
  //           <View style={{ width: '60%' }}>
  //             <Text note style={[styles.noteSty, { fontWeight: 'bold', fontSize: 14 }]}>{item.phone}</Text>
  //             <Text style={[styles.noteContent, { color: '#000' }]}>{item.name}</Text>
  //           </View>
  //           <View style={{ width: '40%', alignItems: 'flex-end', }}>
  //             <Text note style={styles.noteText}>CREATE DATE</Text>
  //             <Text style={[styles.noteContent, { color: '#1BA23B' }]}>{item.expires}</Text>
  //           </View>
  //         </View>

  //         <View style={{ alignItems: 'flex-end', marginTop: 10, }}>
  //           <TouchableOpacity style={styles.editSty} onPress={() => navigateActions('warranty')}>
  //             <Text style={styles.editText}>Edit</Text>
  //           </TouchableOpacity>
  //         </View>

  //       </View>

  //     </View>

  //   );
  // }




  render() {
    const { data, mmidToken, renderPage, msgUnRead , purchaseUnRead, enquiryUnRead} = this.state;
    const navigateActions = this.props.navigation.navigate;
    // const profiles = this.props.screenProps.profile || [] 
    //console.log('mmidToken', this.props.screenProps.mmidToken)
    //console.log('enquiryUnRead', enquiryUnRead)

    return (
      renderPage &&
      <ScrollView >

        <View >
          {/* <View style={styles.circleContainer} /> */}
          <Image source={bg} style={styles.circleContainer} />
          <View style={styles.container}>
            <Card style={{ borderRadius: 20, borderWidth: .5, }}>

              {/************************** card profile image header ****************************/}
              <CardItem header style={styles.headerImg}>
                <View style={styles.imgWarpProfile}>
                  <Thumbnail source={{ uri: data.photo }} style={styles.thumbnailImg} />
                </View>
              </CardItem>

              {/************************** icon edit ****************************/}
              <CardItem style={styles.topIconWrap}>
                <View style={{ width: '100%', alignItems: 'flex-end' }}>
                  <TouchableOpacity onPress={() => navigateActions('editProfile', { memberData: data})}>
                    <FA name="edit" size={25} />
                  </TouchableOpacity>
                </View>
                {/* <View style={{ flexDirection: 'row', }}>
                  <View style={{ width: '50%', alignItems: 'flex-start' }}>
                    <TouchableOpacity>
                      <FA name="cog" size={25} />
                    </TouchableOpacity>
                  </View>
                  <View style={{ width: '50%', alignItems: 'flex-end' }}>
                    <TouchableOpacity>
                      <FA name="edit" size={25} />
                    </TouchableOpacity>
                  </View>
                </View> */}
              </CardItem>

              {/************************** profile contenn ****************************/}
              <CardItem bordered style={styles.profileWarp}>
                <View style={{ width: '100%', marginTop: 10, marginBottom: 10 }}>
                  <Text style={styles.name}>{data.name}</Text>
                  <Text style={styles.profileContent}>{data.contact}</Text>
                  <Text style={styles.profileContent}>{data.email}</Text>
                </View>
              </CardItem>

              {/************************** Warranty Card ****************************/}
              {/* <CardItem bordered>
                <View >
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '50%' }}>
                      <Text style={styles.warrantyCardTitle}>Warranty Card</Text>
                    </View>
                    <View style={{ width: '50%', alignItems: 'flex-end' }}>
                      <TouchableOpacity onPress={() => navigateActions('warranty')}>
                        <Text style={styles.addMore}>ADD MORE +</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.warrantyWarp}>
                    <ScrollView horizontal={true}>
                      <FlatList
                        data={cardData}
                        renderItem={this._renderItem}
                        keyExtractor={(item, index) => item.key}
                        horizontal={true}
                        extraData={this.state}
                      />
                    </ScrollView>
                  </View>
                </View>
              </CardItem> */}

               {/************************** Notification ****************************/}
               <CardItem style={{ backgroundColor: 'transperant', borderBottomWidth: .5, borderBottomColor: '#cccccc94' }}>
                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => navigateActions('notification')}>
                  <Left>
                    <FA name="bell" size={25} />
                    <Text style={{ fontWeight: 'bold' }}>Notification Center</Text>
                    {/* <Text note style={{}}>Unread: {purchaseUnRead.TotalUnReadMsg}</Text> */}
                  </Left>
                  <Right>
                    <FA name="angle-right" size={25} style={{ alignItems: 'flex-end' }} />
                  </Right>
                </TouchableOpacity>
              </CardItem>

              {/************************** Purchase ****************************/}
              <CardItem style={{ backgroundColor: 'transperant', borderBottomWidth: .5, borderBottomColor: '#cccccc94' }}>
                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => navigateActions('purchase', { token: mmidToken })}>
                  <Left>
                    <ETY name="shopping-cart" size={25} />
                    <Text style={{ fontWeight: 'bold' }}>Purchase Record</Text>
                    <Text note style={{}}>Unread: {purchaseUnRead.TotalUnReadMsg}</Text>
                  </Left>
                  <Right>
                    <FA name="angle-right" size={25} style={{ alignItems: 'flex-end' }} />
                  </Right>
                </TouchableOpacity>
              </CardItem>

              {/************************** Post ****************************/}
              <CardItem style={{ backgroundColor: 'transperant', borderBottomWidth: .5, borderBottomColor: '#cccccc94' }}>
                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => navigateActions('post', { token: mmidToken })}>
                  <Left>
                    <MCI name="message-reply-text" size={25} />
                    <Text style={{ fontWeight: 'bold' }}>My Gallery</Text>
                  </Left>
                  <Right>
                    <FA name="angle-right" size={25} style={{ alignItems: 'flex-end' }} />
                  </Right>
                </TouchableOpacity>
              </CardItem>

              {/************************** Issues Status ****************************/}
              <CardItem style={{ backgroundColor: 'transperant', borderBottomWidth: .5, borderBottomColor: '#cccccc94' }}>
                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => navigateActions('issue', { unreadToken: msgUnRead.MMID })}>
                  <Left>
                    <ETY name="database" size={25} />
                    <Text style={{ fontWeight: 'bold' }}>Issues Status</Text>
                    <Text note style={{}}>Unread: {msgUnRead.TotalUnReadMsg}</Text>
                  </Left>
                  <Right>
                    <FA name="angle-right" size={25} style={{ alignItems: 'flex-end' }} />
                  </Right>
                </TouchableOpacity>
              </CardItem>

              {/************************** Enquiry ****************************/}
              <CardItem style={{ backgroundColor: 'transperant', borderBottomWidth: .5, borderBottomColor: '#cccccc94' }}>
                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => navigateActions('enquiry', { unreadToken: enquiryUnRead.MMID })}>
                  <Left>
                    <ETY name="info" size={25} />
                    <Text style={{ fontWeight: 'bold' }}>ASK ME</Text>
                    <Text note style={{}}>Unread: {enquiryUnRead.TotalUnReadMsg}</Text>
                  </Left>
                  <Right>
                    <FA name="angle-right" size={25} style={{ alignItems: 'flex-end' }} />
                  </Right>
                </TouchableOpacity>
              </CardItem>

              {/************************** Logout ****************************/}
              <CardItem style={{ backgroundColor: 'transperant', }}>
                <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} onPress={() => this.logoutFuc()}>
                  <Left>
                    <ETY name="log-out" size={25} />
                    <Text style={{ fontWeight: 'bold' }}>Logout</Text>
                  </Left>
                </TouchableOpacity>
              </CardItem>

            </Card>

          </View>
        </View>


        {/************************** image modal ****************************/}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isImageViewVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeImage} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <Image source={{ uri: this.state.modalData.image }} style={styles.imgModal} />
          </View>
        </Modal>




      </ScrollView>
    )
  }

}




const styles = StyleSheet.create({
  circleContainer: { width: '100%', height: 200, resizeMode: 'stretch' },
  container: { paddingLeft: 20, paddingRight: 20, width: '100%', paddingBottom: 80, marginTop: -80 },
  bannerSty: { width: '100%', height: '100%', borderRadius: 5, },
  cardWarp: {
    height: 200, width: 280, backgroundColor: '#E0E0E0', borderRadius: 15, paddingTop: 20, paddingBottom: 20, paddingLeft: 15, paddingRight: 15,
    shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5,
  },
  card: { paddingTop: 10, paddingLeft: 0, paddingRight: 20, paddingBottom: 20, },
  warranty: { fontWeight: 'bold', fontSize: 22, marginBottom: 5 },
  noteText: { color: '#000', marginTop: 3, fontSize: 12 },
  invioceImg: { width: '100%', height: 60, resizeMode: 'stretch' },
  noteContent: { marginTop: 3, fontSize: 14, },
  editSty: { width: '35%', backgroundColor: 'red', borderRadius: 20, padding: 5 },
  editText: { color: '#fff', textAlign: 'center', fontWeight: 'bold', fontSize: 12 },
  headerImg: { position: 'relative', marginBottom: 50, justifyContent: 'center', backgroundColor: 'transparent' },
  imgWarpProfile: {
    width: '100%', alignItems: 'center', position: 'absolute', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 10,
  },
  thumbnailImg: { width: 150, height: 150, borderRadius: 100 },
  topIconWrap: { position: 'absolute', width: '100%', justifyContent: 'center', top: 20, marginBottom: 50, backgroundColor: 'transparent' },
  profileWarp: { backgroundColor: 'transparent', marginTop: 10, },
  name: { textAlign: 'center', marginBottom: 5, fontWeight: 'bold', fontSize: 20 },
  profileContent: { textAlign: 'center', marginBottom: 5, color: 'gray' },
  warrantyCardTitle: { fontSize: 30, fontWeight: 'bold' },
  addMore: { fontSize: 15, fontWeight: 'bold', paddingTop: 10 },
  warrantyWarp: { marginTop: 15, marginBottom: 10 },
  iosShadow: {
    shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 10,
  },
  androidShadow: {
    shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 80, right: 20 },
  imgModal: { width: '100%', height: 400, resizeMode: 'contain' },

});

export default withNavigation(ProfileScreen);