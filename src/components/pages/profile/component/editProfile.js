import React, { Component } from 'react';
import {
  StyleSheet, View, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView, TextInput, Alert, Modal,
  ActivityIndicator,
} from 'react-native';
import RNRestart from 'react-native-restart';
import { Thumbnail, Card, CardItem, Body, Right, Left, Text, Button } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import ANT from 'react-native-vector-icons/AntDesign';
import ImagePicker from 'react-native-image-picker';
import bg from '../../../../assets/background/BG.png';
import { showMessage, hideMessage } from "react-native-flash-message";
import RNFetchBlob from 'rn-fetch-blob';

const localData = [
  { emptyProfile: require('../../../../assets/icon/empty.png') }
]

class EditProfileScreen extends Component {
  static navigationOptions = {
    title: 'Edit Profile',
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      srcImg: null,
      imgUri: '',
      fileName: '',
      imgData: null,
      memberDetail: this.props.navigation.getParam('memberData'),
      isSubmit: false
    }
  }

  componentDidMount() {
    this.setState({
      name: this.state.memberDetail.name
    })
  }



  /******************************************************* onChangeTextInput *********************************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'name') { this.setState({ name: text }) }
    if (field === 'email') { this.setState({ email: text }) }
    if (field === 'phone') { this.setState({ phone: text }) }
  }

  /******************************************************* choosePicture *********************************************************/
  choosePicture = () => {
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          srcImg: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          imgData: response.data
        });
      }
    });
  };

  /******************************************************* reloadPage *********************************************************/
  reloadPage() {
    const navigateActions = this.props.navigation.navigate;
    // RNRestart.Restart()
    navigateActions('profile')
    this.setState({
      isSubmit: false,
      name: '',
      email: '',
      srcImg: null
    })
  }

  /******************************************************* onSubmit *********************************************************/
  onSubmit() {
    console.log('onSubmit');
    const { name, email, chooseImg, imgData } = this.state;
    let outputJson = {
      name: name,
      email: email,
    }
    console.log('outputJson', outputJson);
    this.setState({
      isSubmit: true
    })

    const token = this.state.memberDetail.MMID;
    let url = 'https://apps.idealtech.com.my/api/member_update.php?';
    //console.log('token', token)

    RNFetchBlob.fetch('POST', url, {
      Authorization: "Bearer access-token",
      otherHeader: "foo",
      'Content-Type': 'multipart/form-data',
    }, [
        { name: 'image', filename: 'image.jpeg', type: 'image/jpeg', data: imgData },
        { name: 'name', data: name },
        { name: 'email', data: email },
        { name: 'MMID', data: token },
        { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' }
      ]).then((res) => res.json())
      .then((myJson) => {
        console.log('myJson', myJson);
        if (myJson[0].status === 1) {
          Alert.alert('Update Successful', '',
            [
              { text: 'OK', onPress: () => this.reloadPage() },
            ],
            { cancelable: false },
          );
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => () => that.setState({
                  isSubmit: false,
                  name: '',
                  email: '',
                  srcImg: null
                })
              },
            ],
            { cancelable: false },
          );
        }
      })
      .catch((err) => {
        console.log('error upload img', err);
      })


    // if (name !== "") {
    //   this.setState({
    //     name: this.state.memberDetail.name,
    //     isSubmit: false
    //   })
    // }
    // else {

    // }
    // if (name === "") {
    //   this.setState({
    //     error: showMessage({
    //       message: "Please Fill In Your Name",
    //       description: "Error Messages",
    //       type: "danger",
    //       icon: 'warning'
    //     }),
    //     isSubmit: false
    //   })
    // }
    // else if (email === "") {
    //   this.setState({
    //     error: showMessage({
    //       message: "Please Fill In Email",
    //       description: "Error Messages",
    //       type: "danger",
    //       icon: 'warning'
    //     }),
    //     isSubmit: false
    //   })
    // }
    // else {

    // }
  }

  /******************************************************* onSubmitWithoutImg *********************************************************/
  onSubmitWithoutImg() {
    console.log('onSubmitWithoutImg');
    const { name, email, chooseImg, imgData } = this.state;
    let outputJson = {
      name: name,
      email: email,
    }
    console.log('outputJson', outputJson);
    //console.log('outputJson', outputJson);
    this.setState({
      isSubmit: true
    })

    const token = this.state.memberDetail.MMID;
    let url = 'https://apps.idealtech.com.my/api/member_update.php?';
    //console.log('token', token)
    let that = this;

    RNFetchBlob.fetch('POST', url, {
      Authorization: "Bearer access-token",
      otherHeader: "foo",
      'Content-Type': 'multipart/form-data',
    }, [
        { name: 'name', data: name },
        { name: 'email', data: email },
        { name: 'MMID', data: token },
        { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' }
      ]).then((res) => res.json())
      .then((myJson) => {
        //console.log('myJson', myJson);
        if (myJson[0].status === 1) {
          Alert.alert('Update Successful', '',
            [
              { text: 'OK', onPress: () => that.reloadPage() },
            ],
            { cancelable: false },
          );
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => () => that.setState({
                  isSubmit: false,
                  name: '',
                  email: '',
                  srcImg: null
                })
              },
            ],
            { cancelable: false },
          );
        }
      })
      .catch((err) => {
        console.log('error upload img', err);
      })

    // if (name !== "") {
    //   this.setState({
    //     name: this.state.memberDetail.name,
    //     isSubmit: false
    //   })
    // }
    // else {

    // }
    // if (name === "") {
    //   this.setState({
    //     error: showMessage({
    //       message: "Please Fill In Your Name",
    //       description: "Error Messages",
    //       type: "danger",
    //       icon: 'warning'
    //     }),
    //     isSubmit: false
    //   })
    // }
    // else if (email === "") {
    //   this.setState({
    //     error: showMessage({
    //       message: "Please Fill In Email",
    //       description: "Error Messages",
    //       type: "danger",
    //       icon: 'warning'
    //     }),
    //     isSubmit: false
    //   })
    // }
    // else {

    // }
  }




  render() {
    const { name, email, phone, srcImg, memberDetail, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;
    console.log(memberDetail.photo, 'memberData----data');

    return (
      <ImageBackground source={bg} style={styles.ContainerImage}>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }
        <ScrollView keyboardShouldPersistTaps='always'>
          <View style={styles.container}>
            <Card style={styles.cardWarp}>

              {/************************** header title ****************************/}
              <CardItem header bordered style={{ backgroundColor: 'transparent', }}>
                <View>
                  <Text style={styles.headerTitle}>Edit Your Personal Profile Here</Text>
                </View>
              </CardItem>

              {/************************** Update Photo ****************************/}
              <CardItem style={styles.headerImg} bordered>
                <View>
                  <View style={styles.imgWarpProfile}>
                    {
                      srcImg === null ?
                        <Thumbnail source={{ uri: memberDetail.photo }} style={styles.thumbnailImg} />
                        :
                        <Thumbnail source={srcImg} style={styles.thumbnailImg} />
                    }
                  </View>
                  <View style={styles.update}>
                    <TouchableOpacity onPress={this.choosePicture}>
                      <Text style={styles.blueText}>Update Photo</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </CardItem>

              {/************************** Form ****************************/}
              <CardItem style={{ backgroundColor: 'transperant' }}>
                <View style={{ width: '100%' }}>
                  {/************************** Name ****************************/}
                  <View style={{ width: '100%', }}>
                    <Text style={styles.blueText}>Name ：</Text>
                    <TextInput
                      value={name}
                      placeholder={memberDetail.name}
                      placeholderTextColor="#8c8c8c"
                      style={styles.inputsty}
                      onChangeText={(text) => this.onChangeTextInput(text, 'name')}
                      autoCorrect={false}
                      autoCapitalize="none"
                    />
                  </View>
                  {/************************** Email ****************************/}
                  {/* <View style={{ width: '100%', }}>
                    <Text style={styles.blueText}>Email ：</Text>
                    <TextInput
                      value={email}
                      placeholder="example@gmail.com"
                      placeholderTextColor="#8c8c8c"
                      style={styles.inputsty}
                      onChangeText={(text) => this.onChangeTextInput(text, 'email')}
                      autoCorrect={false}
                      autoCapitalize="none"
                    />
                  </View> */}
                  {/************************** Phone ****************************/}
                  <View style={{ width: '100%', }}>
                    <Text style={styles.blueText}>Phone :</Text>
                    <TextInput
                      value={memberDetail.contact}
                      placeholder={memberDetail.contact}
                      placeholderTextColor="#8c8c8c"
                      style={styles.inputsty}
                      onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
                      autoCorrect={false}
                      autoCapitalize="none"
                      editable={false}
                    />
                  </View>
                  {/************************** Reset Password ****************************/}
                  <View style={styles.resetPss}>
                    <TouchableOpacity style={styles.touchWarp} onPress={() => navigateActions('resetpass')}>
                      <View style={{ flexDirection: 'row' }}>
                        <ANT name="lock" size={20} style={[styles.blueText, { marginRight: 10 }]} />
                        <Text style={{ color: '#8c8c8c' }}>Reset Password</Text>
                      </View>
                      <ANT name="right" size={20} color='#ccc' />
                    </TouchableOpacity>
                  </View>
                  {/************************** updataButton ****************************/}
                  <View style={styles.updataButton}>
                    <Button full dark style={styles.btnwarp} onPress={() => srcImg === null ? this.onSubmitWithoutImg() : this.onSubmit()}>
                      <Text style={styles.btntext}>Update Profile</Text>
                    </Button>
                  </View>
                </View>
              </CardItem>

            </Card>
          </View>
        </ScrollView>
      </ImageBackground>
    )
  }

}




const styles = StyleSheet.create({
  ContainerImage: { width: '100%', height: '100%', },
  container: { paddingLeft: 20, paddingRight: 20, width: '100%', paddingBottom: 80, },
  cardWarp: { borderRadius: 20, borderWidth: .5, marginTop: 40, },
  headerTitle: { color: '#57C972', fontSize: 12 },
  headerImg: { marginBottom: 10, justifyContent: 'center', backgroundColor: 'transparent', marginTop: 10 },
  imgWarpProfile: {
    width: '100%', alignItems: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 10,
  },
  thumbnailImg: { width: 120, height: 120, borderRadius: 100 },
  update: { alignItems: 'center', marginTop: 30, marginBottom: 15 },
  blueText: { color: '#2979FF' },
  inputsty: { height: 50, borderColor: '#ccc', borderBottomWidth: .5, padding: 10, paddingLeft: 0, color: '#000', marginBottom: 20 },
  updataButton: { width: '100%', marginBottom: 10, marginTop: 10 },
  btnwarp: { borderRadius: 8, height: 50, marginBottom: 10 },
  btntext: { color: '#fff', fontSize: 20 },
  iosShadow: {
    shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 10,
  },
  androidShadow: {
    shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  },
  resetPss: { width: '100%', marginBottom: 20, borderBottomWidth: .5, borderBottomColor: '#ccc' },
  touchWarp: { flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 20, paddingTop: 5, },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },





});

export default EditProfileScreen;





