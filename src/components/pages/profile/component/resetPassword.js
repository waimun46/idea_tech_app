import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import { Text, Button } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import FA from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from "react-native-flash-message";
import { FetchApi } from '../../../api';

class ResetPassword extends Component {
  static navigationOptions = {
    title: 'Reset Password',
  };

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      isSubmit: false
    }
  }


  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'password') { this.setState({ password: text }) }
    if (field === 'confirmPassword') { this.setState({ confirmPassword: text }) }
  }

  /****************************************** successSubmit ********************************************/
  successSubmit() {
    const navigateActions = this.props.navigation.navigate;
    this.setState({
      isSubmit: false,
      password: '',
      confirmPassword: ''
    })
    navigateActions('home')
  }

  /****************************************** onSubmit ********************************************/
  onSubmit() {
    const { password, confirmPassword } = this.state;
    let outputJson = { password: password, confirmPassword: confirmPassword }
    //console.log('login outputJson', outputJson);

    this.setState({
      isSubmit: true
    })

    if (password === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your New Password",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (confirmPassword === "") {
      this.setState({
        errors: showMessage({
          message: "Please Fill In Your Confirm New Password",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (confirmPassword !== password) {
      this.setState({
        errors: showMessage({
          message: "Fill in Password Is Not Same",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      const token = this.props.screenProps.mmidToken;
      let path = "member_reset_password";
      let parameters = { MMID: token, password: this.state.password };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then((myJson) => {
          //console.log(myJson, 'myJson');
          if (myJson[0].status === 1) {
            Alert.alert('Your password already success reset.', '',
              [
                { text: 'OK', onPress: () => that.successSubmit() },
              ],
              { cancelable: false },
            );
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => () => that.setState({
                    isSubmit: false,
                    password: '',
                    confirmPassword: '',
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
    }
  }



  render() {
    const { password, confirmPassword, isSubmit } = this.state;

    return (
      <ScrollView>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={{ flex: 1, alignItems: 'center', }}>

          {/************************** code input ****************************/}
          <View style={{ width: '80%', marginTop: 30 }}>
            <TextInput
              value={password}
              placeholder="New Password"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'password')}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
            />

            <TextInput
              value={confirmPassword}
              placeholder="Confirm New Password"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'confirmPassword')}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
            />

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>Submit</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    )
  }

}




const styles = StyleSheet.create({
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20, },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});

export default ResetPassword;