import React, { Component } from 'react';
import { View, Image, StyleSheet, ScrollView, ImageBackground, FlatList, TouchableOpacity, Linking } from 'react-native';
import { Text, Card, CardItem, } from 'native-base';
import bg from '../../../assets/background/bgw.jpg';
import ANT from 'react-native-vector-icons/AntDesign';
import ETP from 'react-native-vector-icons/Entypo';


const data = [
  {
    type: 'Social Media',
    media: [
      {
        image: require('../../../assets/official/fb.png'), title: 'Official Facebook',
        link: 'https://www.facebook.com/IDEALTECHPC/'
      },
      {
        image: require('../../../assets/official/in.png'), title: 'Official Instagram',
        link: 'https://www.instagram.com/idealtechmy/?hl=en'
      },
      {
        image: require('../../../assets/official/youtube.png'), title: 'Official Youtube',
        link: 'https://www.youtube.com/channel/UCQOugnh7G6uVoQGN7Fxt4bg/videos'
      },
    ]
  },
  {
    type: 'E-commerce',
    media: [
      {
        image: require('../../../assets/official/shopee.png'), title: 'Official Shopee',
        link: 'https://shopee.com.my/idealtechpc'
      },
      {
        image: require('../../../assets/official/lazada.png'), title: 'Official Lazada',
        link: 'https://www.lazada.com.my/shop/ideal-tech-pc/?langFlag=en&pageTypeId=1'
      },
    ]
  }
]


class OfficialSites extends Component {
  static navigationOptions = {
    title: 'Official Sites',
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /************************************************* _renderItem *************************************************/
  _renderItem = ({ item }) => {
    return (
      <Card style={{
        marginBottom: 40,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
      }}>
        <TouchableOpacity onPress={() => { Linking.openURL(item.link) }}>
          <CardItem cardBody style={{ padding: 4, paddingTop: 4, paddingBottom: 4, }}>
            <Image source={item.image} style={{ width: '100%', resizeMode: 'cover' }} />
          </CardItem>

          <CardItem style={{ backgroundColor: '#000', }}>
            <ETP name={
              item.title === 'Official Facebook' ? "facebook" :
                item.title === 'Official Instagram' ? "instagram" :
                  item.title === 'Official Youtube' ? "youtube" :
                    item.title === 'Official Shopee' ? "shopping-bag" :
                      item.title === 'Official Lazada' ? "shopping-cart" : null
            } size={30}
              style={{ color: '#dbc895' }}
            />
            <Text style={{ color: '#fff', padding: 10, fontWeight: 'bold' }} >{item.title}</Text>
          </CardItem>
        </TouchableOpacity>
      </Card>
    )
  }

  render() {
    console.log('data-----', data)
    return (
      <ScrollView>
        <View style={{ backgroundColor: '#eee' }}>
          <ImageBackground source={bg} style={styles.imagebg}>
            <View style={styles.overlay} />
            <View style={styles.content} >
              <Text style={{ color: '#dbc895', fontSize: 28, fontWeight: 'bold', marginBottom: 5, }}>Official Sites</Text>
              <Text style={{ color: '#dbc895' }} >Social Media</Text>
            </View>
          </ImageBackground>

          <View style={{ alignItems: 'center', padding: 30 }}>
            <FlatList
              data={data[0].media}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          <ImageBackground source={bg} style={styles.imagebg}>
            <View style={styles.overlay} />
            <View style={styles.content} >
              <Text style={{ color: '#dbc895', fontSize: 28, fontWeight: 'bold', marginBottom: 5, }}>E-commerce</Text>
              {/* <Text style={{ color: '#dbc895' }} >E-commerce</Text> */}
            </View>
          </ImageBackground>

          <View style={{ alignItems: 'center', padding: 20 }}>
            <FlatList
              data={data[1].media}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  imagebg: { width: '100%', height: 150, resizeMode: 'cover', },
  overlay: { backgroundColor: '#000', opacity: 0.25, height: 150, width: '100%', },
  content: { position: 'absolute', height: 150, width: '100%', justifyContent: 'center', alignItems: 'center', },

});

export default OfficialSites;
