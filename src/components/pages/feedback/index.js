import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, TouchableOpacity, ScrollView, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import { Text, Button, Thumbnail, Textarea } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { showMessage, hideMessage } from "react-native-flash-message";

const localData = [
  { emptyImg: require('../../../assets/icon/imgempty.png') }
]

class FeedbackScreen extends Component {
  static navigationOptions = {
    title: 'Feedback',
  };

  constructor(props) {
    super(props);
    this.state = {
      message: '',
      ImageSource: null,
      Image_TAG: '',
      ImgData: null,
      isSubmit: false
    };
  }

  /*********************************************************** onChangeTextInput *************************************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'message') { this.setState({ message: text }) }
  }

  /*********************************************************** choosePicture *************************************************************/
  choosePicture = () => {
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          ImageSource: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          ImgData: response.data
        });
      }
    });
  };

  /*********************************************************** onSubmit *************************************************************/
  onSubmit() {
    const { message, ImageSource, ImgData, Image_TAG } = this.state;
    const navigateActions = this.props.navigation.navigate;
    //console.log('onSubmit')
    let outputJson = {
      message: message,
      ImgData: ImgData,
      // ImageSource: ImageSource,
    }
    //console.log(outputJson, '----------outputJson');

    this.setState({
      isSubmit: true
    })

    if (message === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Issues",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        })
      })
    }
    else {
      const token = this.props.screenProps.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_message_add.php?';
      //console.log(token, 'feedback token');

      let that = this;

      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'image', filename: 'image.jpeg', type: 'image/jpeg', data: ImgData },
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
          { name: 'message', data: message }
        ]).then((res) => res.json())
        .then((myJson) => {
          //console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            navigateActions('issue');
            that.setState({
              isSubmit: false,
              message: ''
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    message: ''
                  })
                },
              ],
              { cancelable: false },
            );
          }
        }).catch((err) => {
          console.log('error upload img', err);
        })
    }

  }

  /******************************************************* onSubmitWithoutImg *********************************************************/
  onSubmitWithoutImg() {
    const { message, ImageSource, ImgData, Image_TAG } = this.state;
    const navigateActions = this.props.navigation.navigate;
    //console.log('onSubmitWithoutImg')
    let outputJson = {
      message: message,
      ImgData: ImgData,
      // ImageSource: ImageSource,
    }
    // console.log(outputJson, '----------outputJson');

    this.setState({
      isSubmit: true
    })

    if (message === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Issues",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        })
      })
    }
    else {
      const token = this.props.screenProps.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_message_add.php?';
      //console.log(token, 'feedback token')
      let that = this;

      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
          { name: 'message', data: message }
        ]).then((res) => res.json())
        .then((myJson) => {
          //console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            navigateActions('issue')
            that.setState({
              isSubmit: false,
              message: ''
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    message: ''
                  })
                },
              ],
              { cancelable: false },
            );
          }
        }).catch((err) => {
          console.log('error upload img', err);
        })
    }

  }



  render() {
    const { message, ImageSource, isSubmit } = this.state;

    return (
      <ScrollView>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        {/************************** Upload Photo ****************************/}
        <View style={styles.container}>
          <View style={styles.iconContainer}>
            <Thumbnail source={ImageSource === null ? localData[0].emptyImg : ImageSource} style={styles.thumbnailImg} />
            <View style={{ marginTop: 20 }}>
              <TouchableOpacity onPress={this.choosePicture}>
                <Text style={styles.blueText}>Upload Photo</Text>
              </TouchableOpacity>
              <Text note style={styles.opsty}>Optional</Text>
            </View>
          </View>

          {/************************** message Textarea ****************************/}
          <View style={styles.contentWarp}>
            <Textarea
              rowSpan={5}
              bordered
              value={message}
              placeholder="Please fill up your issues..."
              placeholderTextColor="#ccc"
              onChangeText={(text) => this.onChangeTextInput(text, 'message')}
              style={{ borderRadius: 5 }}
              autoCapitalize="none"
              autoCorrect={false}
            />

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => ImageSource === null ? this.onSubmitWithoutImg() : this.onSubmit()}>
              <Text style={styles.btntext}>Send</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20 },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  iconContainer: { alignItems: 'center', width: '100%', },
  contentWarp: { width: '80%', marginTop: 30 },
  thumbnailImg: { width: '100%', height: 200, borderRadius: 0 },
  blueText: { color: '#2979FF' },
  opsty: { textAlign: 'center', fontSize: 12, marginTop: 5 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },



});



export default FeedbackScreen;
