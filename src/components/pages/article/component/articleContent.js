import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, } from 'react-native';
import { H3, Text } from 'native-base';
import { FetchApi } from '../../../api';
import HTML from 'react-native-render-html';



class ArticleContent extends Component {
  static navigationOptions = {
    title: 'Article Details',
  };

  constructor(props) {
    super(props);
    this.state = {
      articleContent: []
    };
  }

  /************************************************* componentDidMount ***************************************************/
  componentDidMount() {
    this.fetchArticleContent();
  }

  /************************************************* fetchArticleContent ***************************************************/
  fetchArticleContent() {
    let path = "articles";
    let parameters = { article_id: this.props.navigation.state.params.articleID }
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          articleContent: fetchData[0]
        })
      })
  }



  render() {
    const { articleContent } = this.state;
    const htmlContent = articleContent.article_content;
    let navigateActionsData = this.props.navigation.state.params;
    console.log(navigateActionsData.articleID, 'articleID')
    console.log(articleContent, 'articleContent-----')
    return (
      <ScrollView>
        <View>
          <Image source={{ uri: articleContent.article_path }} style={styles.imgSty} />
        </View>
        <View style={styles.container}>
          <View style={styles.titleWarp}>
            <H3 style={styles.title}>{articleContent.article_title}</H3>
            <Text note style={{ marginTop: 20 }}>{articleContent.update_dt}</Text>
          </View>
          <HTML
            html={htmlContent}
            tagsStyles={{
              img: { width: '100%', resizeMode: 'contain' },
              // p: {padding: 20,}
            }}
            ignoredStyles={['width', 'height']}
          />
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  imgSty: { width: '100%', height: 200, resizeMode: 'cover' },
  container: { marginTop: 30, marginBottom: 30 },
  titleWarp: { paddingRight: 20, paddingLeft: 20 },
  title: { fontWeight: 'bold', lineHeight: 28 },
  subcontentSty: { marginTop: 10, color: '#444444' },
  flatlistContent: { color: '#444444' },
  flatlistTitle: { fontWeight: 'bold', marginBottom: 10, },

});


export default ArticleContent;
