import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import { Text, Button } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from "react-native-flash-message";
import { FetchApi } from '../../../api';

class SmsLogin extends Component {
  static navigationOptions = {
    title: 'SMS Login',
  };
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      isSubmit: false
    }
  }


  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
  }

  /************************************************* onSubmit ***************************************************/
  onSubmit() {
    const { phone } = this.state;
    const navigateActions = this.props.navigation.navigate;

    this.setState({
      isSubmit: true
    })

    if (phone === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Phone Number",
          description: "Error Messages",
          type: "danger",
          icon: 'warning',
        }),
        isSubmit: false
      })
    }
    else {
      let path = "member_login_generate_code";
      let parameters = { contact: this.state.phone };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then((postData) => {
          console.log('postData-----', postData);
          if (postData[0].status  === 1) {
            that.setState({
              isSubmit: false,
              phone: '',
            })
            return navigateActions('smsTacCode', { phoneNumber: phone });
          }
          else {
            Alert.alert(postData[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    phone: '',
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
    }

  }

  render() {
    const { phone, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    return (
      <ScrollView>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={styles.container}>
          <View style={styles.iconContainer}>
            <View style={styles.iconWarp}>
              <FA name="envelope" size={75} color='#fff' />
            </View>
          </View>

          {/************************** Phone input ****************************/}
          <View style={styles.contentWarp}>
            <TextInput
              value={phone}
              placeholder="Mobile Number (EXP: 0120000000)"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType={'numeric'}
            />

            <Text note>
              Please enter your phone number in field above, once completed, you will receive a phone SMS containing a confirmation code shortly.
            </Text>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>GENERATE OTP</Text>
            </Button>

          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20 },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 16 },
  iconWarp: { width: 130, height: 130, borderRadius: 100, backgroundColor: '#cee0ed', alignItems: 'center', justifyContent: 'center' },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  contentWarp: { width: '80%', marginTop: 30 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});

export default SmsLogin;
