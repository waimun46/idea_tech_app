import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TextInput } from 'react-native';
import { Text, Button } from 'native-base';


class NewPassword extends Component {
  static navigationOptions = {
    title: 'New Password',
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }


  render() {
    const navigateActions = this.props.navigation.navigate;
    //console.log('newPassword', this.props.navigation.state.params.newPass)
    return (
      <ScrollView>

        <View style={styles.container}>

          {/************************** number get in sms ****************************/}
          <View style={styles.iconContainer}>
            <View style={styles.iconWarp}>
              <Text style={styles.textPass}>{this.props.navigation.state.params.newPass}</Text>
            </View>
          </View>

          {/************************** sms notice ****************************/}
          <View style={styles.contentWarp}>
            <Text note>This is your temporary New Password and You will receive a phone SMS containing a temporary New Password shortly.</Text>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => navigateActions('home')}>
              <Text style={styles.btntext}>Back to Home</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20 },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  iconWarp: {
    borderRadius: 5, backgroundColor: '#c3c3c3', alignItems: 'center', justifyContent: 'center',
    paddingLeft: 50, paddingRight: 50, paddingTop: 20, paddingBottom: 20,
  },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  contentWarp: { width: '80%', marginTop: 30 },
  textPass: { fontSize: 30, },

});

export default NewPassword;
