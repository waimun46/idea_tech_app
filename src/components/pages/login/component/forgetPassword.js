import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import { Text, Button } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import FA from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import { FetchApi } from '../../../api';

class ForgetPassword extends Component {
  static navigationOptions = {
    title: 'Forgot Password',
  };

  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      isSubmit: false
    }
  }


  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
  }

  /****************************************** onSubmit ********************************************/
  onSubmit() {
    const { phone, } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = { phone: phone, }
    //console.log('login outputJson', outputJson);

    this.setState({
      isSubmit: true
    })

    if (phone === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Phone Number",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      let path = "forgot_password";
      let parameters = { contact: phone };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then((myJson) => {
          //console.log(myJson, 'myJson');
          if (myJson[0].status === 1) {
            AsyncStorage.setItem('MMID_TOKEN_FORGET_PASSWORD', myJson[0].MMID);
            navigateActions('verifycode')
            that.setState({
              isSubmit: false,
              phone: ''
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({ isSubmit: false, phone: '' })
                },
              ],
              { cancelable: false },
            );
          }
        })
    }

  }



  render() {
    const { phone, isSubmit } = this.state;
    return (
      <ScrollView>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={styles.container}>
          <View style={styles.iconContainer}>
            <View style={styles.iconWarp}>
              <FA name="lock" size={75} color='#fff' />
            </View>
          </View>

          {/************************** Phone input ****************************/}
          <View style={styles.contentWarp}>
            <TextInput
              value={phone}
              placeholder="Phone Number"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType={'numeric'}
            />

            <Text note>
              Please enter your phone number in field above, once completed, you will receive a phone SMS containing a confirmation code shortly.
              This confirmation code will be used for the final step in resetting your phone number.
            </Text>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>Enter</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    )
  }

}




const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20 },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  iconWarp: { width: 130, height: 130, borderRadius: 100, backgroundColor: '#c3c3c3', alignItems: 'center', justifyContent: 'center' },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  contentWarp: { width: '80%', marginTop: 30 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});

export default ForgetPassword;