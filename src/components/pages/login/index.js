import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import RNRestart from 'react-native-restart';
import { Button, Toast } from 'native-base';
import logo from '../../../assets/logo/logoblack.png';
import { showMessage, hideMessage } from "react-native-flash-message";
import { FetchApi } from '../../api';
import FA from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';

class LoginScreen extends Component {
  static navigationOptions = {
    title: 'Login',
  };

  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      password: '',
      error: '',
      isSubmit: false
    }
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
    if (field === 'password') { this.setState({ password: text }) }
  }

  /****************************************************** successPurchaseList ********************************************************/
  successLogin() {
    const navigateActions = this.props.navigation.navigate;
    this.setState({
      isSubmit: false,
      phone: '',
      password: ''
    })
    RNRestart.Restart()
  }


  /************************************************* onSubmit ***************************************************/
  onSubmit() {
    const { phone, password } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = { phone: phone, password: password };
    //console.log('login outputJson', outputJson);

    this.setState({
      isSubmit: true
    })

    if (phone === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Phone Number",
          description: "Error Messages",
          type: "danger",
          icon: 'warning',
        }),
        isSubmit: false
      })
    }
    else if (password === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Password",
          description: "Error Messages",
          type: "danger",
          icon: 'warning',
        }),
        isSubmit: false
      })
    }
    else {
      let path = "member_login";
      let parameters = { contact: this.state.phone, password: this.state.password };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then((postData) => {
          //console.log('postData-----', postData);
          if (postData[0].status === 1) {
            AsyncStorage.setItem('MMID_TOKEN_LOGIN', postData[0].MMID);
            return this.successLogin()
          }
          else {
            Alert.alert(postData[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    phone: '',
                    password: ''
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
    }

  }

  render() {
    const { phone, password, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    return (

      <ScrollView>

        {/*************************************** submit loading ****************************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={styles.container}>
          {/*************************************** logo ****************************************/}
          <View style={styles.topwarp}>
            <View >
              <Image source={logo} style={[styles.logo, styles.logoWarp]} />
            </View>
            <Text style={styles.textlogo}>Welcome Friends !</Text>
          </View>

          {/*************************************** form ****************************************/}
          <View style={styles.formcontrol}>

            {/************************** Phone input **************************/}
            <TextInput
              value={phone}
              placeholder="Mobile Number (EXP: 0120000000)"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
              autoCorrect={false}
              autoCapitalize="none"
              //secureTextEntry={true}
              keyboardType={'numeric'}
            />

            {/************************** Password input ****************************/}
            <TextInput
              value={password}
              placeholder="Password"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'password')}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
            />

            {/************************** Forgot Password ****************************/}
            <View style={{ alignItems: 'flex-start' }}>
              <TouchableOpacity onPress={() => navigateActions('forgotpass')}>
                <Text style={styles.forgot}>Forgot Password ?</Text>
              </TouchableOpacity>
            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>Login</Text>
            </Button>


            {/************************** sms button login ****************************/}
            <Button full info  style={styles.btnwarpSms} onPress={() => navigateActions('smslogin')}>
              <FA name='envelope' style={{color: '#fff', paddingRight: 20}} size={25} />
              <Text style={styles.btntext}>Login With SMS</Text>
            </Button>

            {/************************** Sign Up ****************************/}
            <View style={styles.signupwarp}>
              <Text>Dont Have Account ?  </Text>
              <TouchableOpacity onPress={() => navigateActions('signup')}>
                <Text style={styles.signupText}>Sign Up</Text>
              </TouchableOpacity>
            </View>

          </View>

        </View>
      </ScrollView>


    )
  }

}




const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', paddingBottom: 20, },
  topwarp: { marginTop: 20, width: '100%', alignItems: 'center', },
  // logoWarp: {
  //   shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
  //   shadowRadius: 6.27, elevation: 10,
  // },
  logo: { width: 150, height: 149, },
  textlogo: { textAlign: 'center', fontSize: 20, marginTop: 20 },
  formcontrol: { width: '80%', marginTop: 20, marginBottom: 10 },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20 },
  forgot: { color: '#2979FF' },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btnwarpSms: { marginTop: 20, borderRadius: 0, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  signupwarp: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 30, },
  signupText: { color: '#57C972' },
  iosShadow: {
    shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 10,
  },
  androidShadow: {
    shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },






});

export default LoginScreen;