import React, { PureComponent } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import { Text, Button } from 'native-base';
import { showMessage, hideMessage } from "react-native-flash-message";
import Countdown from 'react-countdown-now';
import AsyncStorage from '@react-native-community/async-storage';
import { FetchApi } from '../../../api';

class RegisterTacCode extends PureComponent {
  static navigationOptions = {
    title: 'Activated Member',
  };

  constructor(props) {
    super(props);
    this.state = {
      code: '',
      mmidToken: '',
      restartTime: Date.now() + 180000,
      isSubmit: false
    }
  }

  /****************************************************** componentDidMount ********************************************************/
  componentDidMount() {
    /************************************ MMID_TOKEN AsyncStorage ***************************************/
    AsyncStorage.getItem("MMID_TOKEN").then(MMIDStorageRes => {
      //console.log(MMIDStorageRes, '----------------MMIDStorageRes')
      this.setState({
        mmidToken: MMIDStorageRes
      })
    })
  }

  /****************************************************** onChangeTextInput ********************************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'code') { this.setState({ code: text }) }
  }

  /****************************************************** resendTacCode ********************************************************/
  resendTacCode() {
    let path = "member_recode";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    this.setState({
      restartTime: Date.now() + 180000,
    })

    let that = this;

    fetch(ApiData).then((res) => res.json())
      .then(function (myJson) {
        //console.log(myJson, 'myJson-----------');
        if (myJson[0].status === 1) {
          Alert.alert('Tac Code already SMS your phone number', '',
            [
              {
                text: 'OK', onPress: () => that.setState({ isSubmit: false, })
              },
            ],
            { cancelable: false },
          );
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => that.setState({ isSubmit: false, })
              },
            ],
            { cancelable: false },
          );
        }
      })
  }

  /**************************************************** when time out will show resend btn ******************************************************/
  renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return (
        <TouchableOpacity style={styles.resendBtn} onPress={() => this.resendTacCode()}>
          <Text note style={{ color: '#0095ff', }}>Resend TAC Code</Text>
        </TouchableOpacity>
      );
    }
    else {
      return (
        <View style={styles.timeCount}>
          <Text style={styles.secondText}>{minutes}:{seconds}</Text>
        </View>
      );
    }
  }

  /************************************************************ onSubmit **************************************************************/
  onSubmit() {
    const { code } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = { code: code, }
    //console.log('RegisterTacCode outputJson', outputJson);

    this.setState({
      isSubmit: true
    })

    if (code === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your TAC Code",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      let path = "member_verifiy";
      let parameters = { MMID: this.state.mmidToken, tac: this.state.code };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then(function (myJson) {
          if (myJson[0].status === 1) {
            navigateActions('login')
            that.setState({
              isSubmit: false,
              code: '',
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    code: '',
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
    }

  }



  render() {
    const { code, restartTime, isSubmit } = this.state;
    //console.log(this.state.mmidToken, 'mmidToken-------')
    return (
      <ScrollView>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={{ flex: 1, alignItems: 'center', }}>
          {/************************** code input ****************************/}
          <View style={{ width: '80%', marginTop: 30 }}>
            <TextInput
              value={code}
              placeholder="******"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'code')}
              autoCorrect={false}
              autoCapitalize="none"
            />

            <Text note>
              We have sent you an SMS with a TAC Code to the number above,
              To complete your phone number verification, please enter the 6-digit activation TAC Code.
            </Text>

            {/************************** Countdown ****************************/}
            <View style={{ marginTop: 10 }}>
              <Countdown
                date={restartTime}
                renderer={this.renderer}
                key={restartTime}
              />
            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>Verify</Text>
            </Button>

          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  inputsty: {
    height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 20,
    textAlign: 'center', letterSpacing: 10,
  },
  btnwarp: { marginTop: 20, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  resendBtn: { height: 30, justifyContent: 'center', alignItems: 'flex-end' },
  timeCount: { height: 30, justifyContent: 'flex-end', alignItems: 'flex-end' },
  secondText: { color: 'red', fontSize: 18, textAlign: 'right' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },



});

export default RegisterTacCode;
