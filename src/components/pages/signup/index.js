import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, TextInput, Modal, ActivityIndicator, Alert } from 'react-native';
import { Button } from 'native-base';
import logo from '../../../assets/logo/logoblack.png';
import { showMessage, hideMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import { FetchApi } from '../../api';

class SignupScreen extends Component {
  static navigationOptions = {
    title: 'Sign Up',
  };

  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      name: '',
      email: '',
      password: '',
      confirm_password: '',
      errors: '',
      isSubmit: false
    }
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'phone') { this.setState({ phone: text }) }
    if (field === 'name') { this.setState({ name: text }) }
    if (field === 'email') { this.setState({ email: text }) }
    if (field === 'password') { this.setState({ password: text }) }
    if (field === 'confirm_password') { this.setState({ confirm_password: text }) }
  }

  /****************************************** onSubmit ********************************************/
  onSubmit() {
    const { phone, name, password, confirm_password, email } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = {
      phone: phone,
      name: name,
      email: email,
      password: password,
      confirm_password: confirm_password,
    }
    //console.log('sign up outputJson', outputJson);

    this.setState({
      isSubmit: true
    })

    if (phone === "") {
      this.setState({
        errors: showMessage({
          message: "Please Fill In Your Phone Number",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (name === "") {
      this.setState({
        errors: showMessage({
          message: "Please Fill In Your Name",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (password === "") {
      this.setState({
        errors: showMessage({
          message: "Please Fill In Your Password",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (confirm_password === "") {
      this.setState({
        errors: showMessage({
          message: "Please Fill In Your Confirm Password",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (confirm_password !== password) {
      this.setState({
        errors: showMessage({
          message: "Fill in Password Is Not Same",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      let path = "member_register";
      let parameters = {
        name: name,
        email: email,
        password: password,
        contact: phone
      };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then(function (myJson) {
          //console.log(myJson, 'myJson-----------');
          if (myJson[0].status === 1) {
            AsyncStorage.setItem('MMID_TOKEN', myJson[0].MMID);
            Alert.alert('Register Successful', '',
              [
                {
                  text: 'OK', onPress: () => navigateActions('registertaccode',
                    that.setState({
                      isSubmit: false,
                      phone: '',
                      email: '',
                      name: '',
                      password: '',
                      confirm_password: ''
                    }))
                },
              ],
              { cancelable: false },
            );
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    phone: '',
                    email: '',
                    name: '',
                    password: '',
                    confirm_password: ''
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
    }

  }


  render() {

    const { phone, password, email, confirm_password, name, isSubmit } = this.state;
    const navigateActions = this.props.navigation.navigate;

    return (

      <ScrollView keyboardShouldPersistTaps='always'>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={styles.container}>
          {/*************************************** logo ****************************************/}
          <View style={styles.topwarp}>
            <View style={styles.logoWarp}>
              <Image source={logo} style={styles.logo} />
            </View>
            <Text style={styles.textlogo}>Sign Up Today !</Text>
          </View>

          {/*************************************** form ****************************************/}
          <View style={styles.formcontrol}>

            {/************************** Phone input **************************/}
            <TextInput
              value={phone}
              placeholder="Phone Number (EXP: 0120000000)"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
              autoCorrect={false}
              autoCapitalize="none"
              //secureTextEntry={true}
              keyboardType={'numeric'}
            />

            {/************************** Name input **************************/}
            <TextInput
              value={name}
              placeholder="Name"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'name')}
              autoCorrect={false}
              autoCapitalize="none"
            />

            {/************************** Email input **************************/}
            {/* <TextInput
              value={email}
              placeholder="Email"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'email')}
              autoCorrect={false}
              autoCapitalize="none"
            /> */}


            {/************************** Password input ****************************/}
            <TextInput
              value={password}
              placeholder="Password"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'password')}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
            />

            {/************************** Confirm Password input ****************************/}
            <TextInput
              value={confirm_password}
              placeholder="Confirm Password"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'confirm_password')}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
            />

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>Sign Up</Text>
            </Button>


          </View>

        </View>
      </ScrollView>


    )
  }

}




const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center' },
  topwarp: { marginTop: 20, width: '100%', alignItems: 'center' },
  // logoWarp: {
  //   shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
  //   shadowRadius: 6.27, elevation: 10,
  // },
  logo: { width: 150, height: 149, },
  textlogo: { textAlign: 'center', fontSize: 20, marginTop: 20 },
  formcontrol: { width: '80%', marginTop: 20, marginBottom: 10 },
  inputsty: { height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 8, color: '#000', marginBottom: 15 },
  btnwarp: { marginTop: 10, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  iosShadow: {
    shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34,
    shadowRadius: 6.27, elevation: 10,
  },
  androidShadow: {
    shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },





});

export default SignupScreen;