import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList, TextInput, Dimensions } from 'react-native';
import { WebView } from 'react-native-webview';
import { Card, CardItem, Thumbnail, Left, Body, Right } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import FA from 'react-native-vector-icons/FontAwesome';



const localData = [
  {
    img: 'https://wallpapershome.com/images/pages/ico_h/19758.jpg', time: '2 hours ago',
    icon: "https://sm.askmen.com/askmen_in/article/f/facebook-p/facebook-profile-picture-affects-chances-of-gettin_fr3n.jpg",
    content: 'Jost Naw Campoter fer thed manthy, anly fer yaur ploy Ploy fes yaur gemy ta fuq.', like: '54', comment: '54'
  },
  {
    img: 'https://ak0.picdn.net/shutterstock/videos/14730910/thumb/1.jpg', time: '2 hours ago',
    icon: "https://sm.askmen.com/askmen_in/article/f/facebook-p/facebook-profile-picture-affects-chances-of-gettin_fr3n.jpg",
    content: 'Jost Naw Campoter fer thed manthy, anly fer yaur ploy Ploy fes yaur gemy ta fuq.', like: '54', comment: '54'
  },
  {
    img: 'https://www.bankingtech.com/files/2017/03/Berlin-Germany1.jpg', time: '2 hours ago',
    icon: "https://sm.askmen.com/askmen_in/article/f/facebook-p/facebook-profile-picture-affects-chances-of-gettin_fr3n.jpg",
    content: 'Jost Naw Campoter fer thed manthy, anly fer yaur ploy Ploy fes yaur gemy ta fuq.', like: '54', comment: '54'
  }
]



class FeedWallScreen extends Component {
  static navigationOptions = {
    title: 'Feed Wall',
  };

  constructor(props) {
    super(props);
    this.state = {
      comment: [],
      renderPage: false,

    }

  }
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
  }

  /************************************************* onChangeTextInput ***************************************************/
  onChangeTextInput(index, text) {
    console.log(text)
    let { comment } = this.state;
    comment[index] = text;
    this.setState({
      comment
    })

  }

  /************************************************* onSubmit ***************************************************/
  onSubmit(index) {
    const { comment } = this.state;
    let outputJson = {
      comment: comment[index]
    }
    console.log('outputJson', outputJson)
  }

  /************************************************* _renderItem ***************************************************/
  _renderItem = ({ item, index }) => {
    const { comment } = this.state;
    return (
      <Card key={index} style={{ marginBottom: 10, marginLeft: 0, marginRight: 0 }}>
        {/************************** top ****************************/}
        <CardItem>
          <Left>
            <ANT name="clockcircleo" size={20} style={styles.topTimeIcon} />
            <Text style={{ color: '#9F9F9F' }}>{item.time}</Text>
          </Left>
          <Right style={styles.topRight}>
            <TouchableOpacity>
              <View style={styles.topRightLike}>
                <ANT name="heart" size={20} style={styles.topRightIcon} />
                <Text style={{ color: '#9F9F9F' }}>{item.like}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={{ flexDirection: 'row' }}>
                <MCI name="message-reply-text" size={20} style={styles.topRightCommentIcon} />
                <Text style={{ color: '#777777' }}>{item.comment}</Text>
              </View>
            </TouchableOpacity>
          </Right>
        </CardItem>

        {/************************** center ****************************/}
        <CardItem cardBody style={{ borderBottomWidth: .5, borderBottomColor: '#DEDEDE' }}>
          <Body>
            <View style={{ height: 200, width: '100%', }}>
              <Image source={{ uri: item.img }} style={{ height: 200, width: '100%', }} />
            </View>
            <View style={{ padding: 20 }}>
              <Text style={{ color: 'gray' }}>{item.content}</Text>
            </View>
          </Body>
        </CardItem>

        {/************************** bottom ****************************/}
        <CardItem >
          <Body style={{ flexDirection: 'row' }}>
            <View style={{ width: '10%', alignItems: 'center' }}>
              <Thumbnail source={{ uri: item.icon }} style={{ width: 40, height: 40, }} />
            </View>

            <View style={{ paddingLeft: 10, width: '90%', }}>
              <View style={{ borderWidth: .5, borderColor: '#C1C1C1', borderRadius: 8, flexDirection: 'row', }}>
                <TextInput
                  value={comment}
                  placeholder="Add a comment as msTan…"
                  placeholderTextColor="#99A2B3"
                  style={{ paddingLeft: 10, height: 40, flex: 1, }}
                  onChangeText={(text) => this.onChangeTextInput(index, text)}
                  autoCorrect={false}
                  autoCapitalize="none"
                />
                <TouchableOpacity onPress={() => this.onSubmit(index)}>
                  <FA name="send" size={20} style={{ color: '#3591ED80', padding: 10 }} />
                  {/* <Text style={{ color: '#3591ED80',padding: 10  }}>Post</Text> */}
                </TouchableOpacity>
              </View>
            </View>

          </Body>

        </CardItem>
      </Card>
    )
  }

  render() {
    const { renderPage } = this.state;

    return (
      renderPage &&
      <View style={{
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
      }}>
        {/* <FlatList
            data={localData}
            extraData={this.state}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => item.key}
          /> */}


        <WebView
          automaticallyAdjustContentInsets
          source={{ uri: 'https://www.facebook.com/IDEALTECHPC/' }}
          decelerationRate='normal'
          scalesPageToFit
          javaScriptEnabled
          domStorageEnabled
          scrollEnabled
        />


      </View>

    )
  }

}




const styles = StyleSheet.create({
  topTimeIcon: { marginRight: 10, color: '#9F9F9F' },
  topRight: { flexDirection: 'row', justifyContent: 'flex-end', },
  topRightLike: { flexDirection: 'row', marginRight: 10 },
  topRightIcon: { marginRight: 5, color: '#ccc' },
  topRightCommentIcon: { marginRight: 5, color: 'gray' },
  _2p3a: { width: '100%', minWidth: '100%' }
});

export default FeedWallScreen;