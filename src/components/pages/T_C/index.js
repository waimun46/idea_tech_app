import React, { Component } from 'react';
import { View, ImageBackground, StyleSheet, ScrollView, Image, FlatList } from 'react-native';
import { Text, Card, CardItem, } from 'native-base';
import HTML from 'react-native-render-html';
import { FetchApiNoParam, FetchApi } from '../../api';
import bg from '../../../assets/background/bgw.jpg';



class TermsConditions extends Component {
  static navigationOptions = {
    title: 'Terms & Conditions',
  };
  constructor(props) {
    super(props);
    this.state = {
      tcData: []
    };
  }

  componentDidMount() {
    this.fetchTcData()
  }

  /************************************************* fetchArticleData ***************************************************/
  fetchTcData() {
    let path = "terms";
    let ApiData = FetchApiNoParam(path);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          tcData: fetchData[0],
          isLoading: false,
        })
      })
  }

  render() {
    const { tcData } = this.state;
    const htmlContent = tcData.article_content;
    return (
      <ScrollView>
        <ImageBackground source={bg} style={styles.imagebg}>
          <View style={styles.overlay} />
          <View style={styles.content} >
            <Text style={{ color: '#dbc895', fontSize: 28, fontWeight: 'bold', marginBottom: 5, }}>{tcData.article_title}</Text>
            {/* <Text style={{ color: '#dbc895' }} note>MISSION, HISTORY & VISION</Text> */}
          </View>
        </ImageBackground>
        <HTML
          html={htmlContent}
          tagsStyles={{
            img: { width: '100%', resizeMode: 'contain' },
            p: { padding: 20, }
          }}
          ignoredStyles={['width', 'height']}
          classesStyles={{
            about: { padding: 24, },
            warp: { marginBottom: 34, },
            titlewarp: { fontSize: 26, fontWeight: 'bold', marginBottom: 4, },
            subtitle: { fontSize: 16, color: 'gray', fontWeight: 'bold', },
            content: { marginTop: 8, color: '#848d95' },
            hrstyle: { width: '40%', backgroundColor: '#dbc895', height: 2.4 }
          }}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imagebg: { width: '100%', height: 150, resizeMode: 'cover', },
  overlay: { backgroundColor: '#000', opacity: 0.25, height: 150, width: '100%', },
  content: { position: 'absolute', height: 150, width: '100%', justifyContent: 'center', alignItems: 'center', },

});

export default TermsConditions;
