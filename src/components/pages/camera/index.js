import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import { RNCamera, FaceDetector } from 'react-native-camera';
import MTC from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';


class CameraScreen extends Component {
  static navigationOptions = {
    title: 'Camera',
  };

  constructor(props) {
    super(props);
    this.state = {
      imageData: null,
      cameraType: RNCamera.Constants.Type.back,
      path: null,
      isFlashOn: true
    }
  }

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
      AsyncStorage.setItem('IMAGE_CAMERA', data.uri);
      this.setState({
        path: data.uri,
      })

    }
  };

  flashToggle = () => {
    this.setState({ isFlashOn: !this.state.isFlashOn })
  }

  // takePicture = async function() {
  //   console.log('TAKE');
  //   console.log(this.camera, 'CAP');
  //   if (this.camera) {
  //     try {
  //       const options = { quality: 0.5,  base64: true};
  //       const data = await this.camera.takePictureAsync(options)

  //       console.log(data.uri, 'image');
  //       // this.setCameraCaptureVisible(false);
  //     } catch (error) {
  //      // This logs the error
  //       console.log(error, 'error')
  //     }

  //   }
  // };

  // takePicture = () => {
  //   console.log('take')
  //   if (this.camera) {
  //     const options = { quality: 0.5, base64: true, fixOrientation: true };
  //     this.camera.takePictureAsync(options).then(data => {
  //       const { uri, base64 } = data;
  //       this.setState({ imageData: { uri, base64 } });
  //     }).catch(console.log)
  //   }
  // };



  handleCameraTypeChange = () => {
    const { back, front } = RNCamera.Constants.Type;
    this.setState({ cameraType: this.state.cameraType === back ? front : back });
  }

  render() {

    const { imageData, cameraType, path } = this.state;

    console.log('path------', path)


    return (
      <View style={{ flex: 1, }}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          defaultTouchToFocus
          mirrorImage={true}
          type={cameraType}
          flashMode={this.state.isFlashOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
          //type={RNCamera.constants.Type.back}
          //aspect={RNCamera.constants.Aspect.fill}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}

        >
          <View style={{
            width: '100%', flexDirection: 'row', justifyContent: 'space-between', top: 0, padding: 10,
            position: 'absolute',
          }}>

            <TouchableOpacity onPress={() => this.flashToggle}>
              <MTC name="flashlight" size={40} style={{ color: '#fff' }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.cameraTypeIconContainer} onPress={this.handleCameraTypeChange}>
              <MTC name="camera-retake" size={40} style={{ color: '#fff' }} />
            </TouchableOpacity>

          </View>

          {/* <View style={{bottom: 150,position: 'absolute',}}>
            <Image
              source={{ uri: this.state.path }}
              style={{width: 100, height: 100, backgroundColor: '#fff',  }}
            />
            <Text
              style={styles.cancel}
              onPress={() => this.setState({ path: null })}
            >Cancel
          </Text>
          </View> */}

          <TouchableOpacity style={styles.capture} onPress={this.takePicture.bind(this)}>
            <View style={styles.shapeCapture}></View>
          </TouchableOpacity>

        </RNCamera>

        {/* {imageData && (
          <View style={styles.content}>
            <Image style={styles.preview} source={{ uri: imageData.uri }} />
          </View>
        )}
        {!imageData && (
          <View style={styles.content}>
            <RNCamera
              ref={ref => { this.camera = ref; }}
              style={styles.preview}
              type={cameraType}
            >
              <TouchableOpacity style={styles.cameraTypeIconContainer} onPress={this.handleCameraTypeChange}>
               <Text>reflash</Text>
              </TouchableOpacity>
            </RNCamera>
          </View>
        )}
        <View style={styles.capture}>
          <TouchableOpacity onPress={this.takePicture}>
          <Text >[CAPTURE]</Text>
          </TouchableOpacity>
        </View> */}

      </View>
    )
  }

}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  preview: {
    flex: 1,
    // justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    bottom: 40,
    position: 'absolute',
    backgroundColor: '#fff',
    borderRadius: 50,
    color: '#000',
    padding: 2,
  },
  shapeCapture: { width: 70, height: 70, backgroundColor: '#fff', borderRadius: 50, borderWidth: 1, borderColor: '#000' },
  cameraTypeIconContainer: {
    // backgroundColor: '#000',
    color: '#000',


  },
  cancel: {
    position: 'absolute',
    right: 20,
    top: 20,
    backgroundColor: 'transparent',
    color: '#FFF',
    fontWeight: '600',
    fontSize: 17,
  }
});


export default CameraScreen;