import React, { Component } from 'react';
import {
  StyleSheet, View, ScrollView, TextInput, TouchableOpacity, FlatList, Image, Dimensions, RefreshControl, Modal, Alert,
  ActivityIndicator, ImageBackground, KeyboardAvoidingView, NativeModules, StatusBarIOS, Platform, Linking
} from 'react-native';
import { H3, Text, Thumbnail } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import ANT from 'react-native-vector-icons/AntDesign';
import { FetchApi } from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { showMessage, hideMessage } from "react-native-flash-message";
import bg from '../../../assets/background/askme.png';
import logo from '../../../assets/logo/logoblack.png';
import ImageZoom from 'react-native-image-pan-zoom';
import Hyperlink from 'react-native-hyperlink'

const { StatusBarManager } = NativeModules;

const WINDOW_HEIGHT = Dimensions.get('window').height - 200;
const BASE_PADDING = 10;

class Enquiry extends Component {
  static navigationOptions = {
    title: 'ASK ME',
  };

  constructor(props) {
    super(props);
    this.state = {
      post: '',
      message: [],
      mmidToken: '',
      ImageSource: null,
      ImgData: null,
      refreshing: true,
      isSubmit: false,
      enquiryListData: [],
      msgUpdate: [],
      modalVisible: false,
      modalData: { image: '', itemImg: '' },
      imageChoose: false,
      statusBarHeight: 0,
      clearArray: [],
      submitLoading: false
    };
  }

  /******************************************************* componentDidMount *********************************************************/
  componentDidMount() {
    /************************************ MMID_TOKEN_LOGIN AsyncStorage ***************************************/
    AsyncStorage.getItem("MMID_TOKEN_LOGIN").then(MMIDStorageRes => {
      //console.log(MMIDStorageRes, '----------------MMIDStorageRes')
      this.setState({
        mmidToken: MMIDStorageRes,
      })
      // this.fetchIssueMessageApi();
      this.fetchEnquiryList();
      this.fetchEnquiryUpdate();
    })

    if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((statusBarFrameData) => {
        this.setState({ statusBarHeight: statusBarFrameData.height });
      });
      this.statusBarListener = StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
        this.setState({ statusBarHeight: statusBarData.frame.height });
      });
    }
  }

  // componentWillUnmount() {
  //   this.statusBarListener.remove();
  // }

  /************************************************************ openModalUpload ***********************************************************/
  openModalUpload() {
    this.setState({
      modalVisible: true,
      modalData: { image: this.state.ImageSource, },
      imageChoose: true
    });
  }

  /************************************************************ closeUploadImage ***********************************************************/
  closeUploadImage() {
    this.setState({
      ImageSource: null
    })
  }

  /************************************************************ openModalProduct ***********************************************************/
  openModal(item) {
    this.setState({
      modalVisible: true,
      modalData: { image: item.photo, itemImg: item.item },
      imageChoose: false
    });
  };

  /************************************************************ closeImage ***********************************************************/
  closeModal = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false
    })
  }

  /******************************************************* fetchIssueMessageApi *********************************************************/
  // fetchIssueMessageApi() {
  //   let path = "member_message_list";
  //   let parameters = { MMID: this.state.mmidToken };
  //   let ApiData = FetchApi(path, parameters);
  //   // console.log(ApiData, 'ApiData');

  //   fetch(ApiData).then((res) => res.json())
  //     .then((myJson) => {
  //       this.setState({
  //         message: myJson,
  //         refreshing: false
  //       })
  //     })
  // }

  /******************************************************* fetchEnquiryList *********************************************************/
  fetchEnquiryList() {
    let path = "member_enquiry_list";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData-----fetchMessageUpdate');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          enquiryListData: fetchData,
          clearArray: fetchData[0],
          refreshing: false,
        })
      })
  }

  /******************************************************* fetchEnquiryUpdate *********************************************************/
  fetchEnquiryUpdate() {
    let path = "member_enquiry_update";
    let parameters = { MMID: this.state.mmidToken };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData-----fetchMessageUpdate');

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        this.setState({
          msgUpdate: myJson,
          refreshing: false
        })
      })
  }

  /*********************************************************** onChangeTextInput *************************************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'post') { this.setState({ post: text }) }
  }

  /*********************************************************** choosePicture *************************************************************/
  choosePicture = () => {
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    this.setState({
      imageChoose: true
    })

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          ImageSource: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          ImgData: response.data
        });
      }
    });
  };

  /*********************************************************** onSubmit *************************************************************/
  onSubmit() {
    console.log('onSubmit')
    const { post, ImgData, } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = {
      post: post,
      ImgData: ImgData,
    }
    //console.log(outputJson, '----------outputJson');

    this.setState({
      isSubmit: true,
      submitLoading: true
    })

    if (post === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Issues",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false,
        submitLoading: false,
      })
    }
    else {
      const token = this.state.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_enquiry_add.php?';
      //console.log(token, 'feedback token')
      let that = this;
      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'image', filename: 'image.jpeg', type: 'image/jpeg', data: ImgData },
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
          { name: 'message', data: post }
        ]).then((res) => res.json())
        .then((myJson) => {
          //console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            that.setState({
              ImageSource: null,
              isSubmit: false,
              post: '',
              submitLoading: false
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    post: '',
                    ImageSource: null,
                    submitLoading: false,
                  })
                },
              ],
              { cancelable: false },
            );
          }
        }).catch((err) => {
          console.log('error upload img', err);
        })
    }

  }

  /******************************************************* onSubmitWithoutImg *********************************************************/
  onSubmitWithoutImg() {
    console.log('onSubmitWithoutImg')
    const { post, ImgData, } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = {
      post: post,
      ImgData: ImgData,
      // ImageSource: ImageSource,
    }
    //console.log(outputJson, '----------outputJson');

    this.setState({
      isSubmit: true,
      submitLoading: true
    })

    if (post === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Issues",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false,
        submitLoading: false,
      })
    }
    else {
      const token = this.state.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_enquiry_add.php?';
      //console.log(token, 'feedback token')

      let that = this;

      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
          { name: 'message', data: post }
        ]).then((res) => res.json())
        .then((myJson) => {
          //console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            that.setState({
              ImageSource: null,
              isSubmit: false,
              submitLoading: false,
              post: ''
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({
                    isSubmit: false,
                    post: '',
                    ImageSource: null,
                    submitLoading: false,
                  })
                },
              ],
              { cancelable: false },
            );
          }
        }).catch((err) => {
          console.log('error upload img', err);
        })
    }

  }

  /******************************************************* renderItem *********************************************************/
  _renderItem = ({ item, index }) => {
    return (
      this.state.clearArray.status === 0 ?
        <View style={styles.MainContainer}>
          <ANT name="message1" size={60} style={styles.iconsty} />
          <Text style={styles.emptysty}>No Messages</Text>
        </View>
        :
        <View style={{
          width: '100%', alignItems: item.type === "Admin" ? 'flex-start' : 'flex-end', marginBottom: 20,
          flexDirection: item.type === "Admin" ? 'row' : null
        }}
        >
          {
            item.type === "Admin" ?
              <Image source={logo} style={{ width: 40, height: 40, marginRight: 10, }} />
              : null
          }
          <View style={{ width: '60%' }}>
            <View style={item.type === "Admin" ? styles.admin : styles.member}>
              {
                item.photo === null ? (
                  null
                ) : (
                    <TouchableOpacity onPress={() => this.openModal(item)}>
                      <Image source={{ uri: item.photo }} style={styles.photoSty} resizeMode="contain" />
                    </TouchableOpacity>
                  )
              }
              {
                item.item !== null ?
                  <TouchableOpacity onPress={() => this.openModal(item)}>
                    <Image source={{ uri: item.item }} style={styles.photoSty} resizeMode="contain" />
                  </TouchableOpacity> : null
              }

              {/* <TouchableOpacity onPress={() => this.openModal(item)}>
                    <Image source={{ uri: item.content }} style={styles.photoSty} resizeMode="contain" />
                </TouchableOpacity> */}
                
              <Hyperlink linkDefault={true} linkStyle={{ color: '#005cc5', fontWeight: '300', fontSize: 14 }}>
                <Text style={{
                  color: item.type === "Admin" ? '#fff' : '#000', textAlign: item.type === "Admin" ? 'left' : 'right', fontWeight: 'bold',
                }}>{item.content}</Text>
              </Hyperlink>

              <TouchableOpacity onPress={() => { Linking.openURL(item.linkUrl) }}>
                <Text note style={{
                  color: item.type === "Admin" ? '#cfeafe' : '#379fef', textAlign: item.type === "Admin" ? 'left' : 'right',
                }}>{item.linkUrl}</Text>
              </TouchableOpacity>

              <Text note style={{
                color: item.type === "Admin" ? '#fff' : '#ccc', textAlign: item.type === "Admin" ? 'left' : 'right',
                fontSize: 12, marginTop: 10,
              }}>{item.contentTime}</Text>
            </View>
            <Text note style={{ marginTop: 10, textAlign: item.type === "Admin" ? 'left' : 'right' }}>{item.role}</Text>
          </View>
        </View>
    )
  }

  /******************************************************* ListEmpty *********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="filetext1" size={60} style={styles.iconsty} />
        <Text style={styles.emptysty}>No Comment</Text>
      </View>

    );
  };



  render() {
    const { post, message, ImageSource, isSubmit, enquiryListData, modalVisible, modalData, imageChoose,
      statusBarHeight, clearArray, submitLoading } = this.state;
    //console.log('enquiryListData', enquiryListData)
    console.log('submitLoading', submitLoading)


    return (
      // <View style={{ backgroundColor: '#F3F4F6', flex: 1, }}>
      <KeyboardAvoidingView style={{ flex: 1, }} behavior={Platform.OS === 'ios' ? 'padding' : null} enabled keyboardVerticalOffset={44 + statusBarHeight}>
        {/* <KeyboardAvoidingView style={{ flex: 1, }} behavior='padding' enabled keyboardVerticalOffset={44 + statusBarHeight}> */}
        <ImageBackground source={bg} style={styles.containerImage} >
          {/************************** submit loading ****************************/}
          {
            isSubmit ?
              <Modal
                transparent={true}
                animationType={'none'}
                visible={isSubmit}
                onRequestClose={() => { console.log('close modal') }}>
                <View style={styles.modalBackground}>
                  <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator size="large" animating={isSubmit} />
                  </View>
                </View>
              </Modal>
              : null
          }

          {/************************** chat list ****************************/}
          <FlatList
            ref={ref => this.flatList = ref}
            data={enquiryListData}
            extraData={this.state}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ padding: 20, }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.fetchEnquiryList()}
              />}
            // onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
            // onLayout={() => this.flatList.scrollToEnd({ animated: true })}
            inverted={-1}
          />

          {/************************** show in chat list image upload ****************************/}
          {
            ImageSource !== null ? (
              <View style={{ alignItems: 'flex-end', }}>
                <View style={[styles.selectImagePost, { position: 'relative' }]}>
                  <TouchableOpacity onPress={() => this.closeUploadImage()} style={styles.closeUpload}>
                    <ANT name="close" size={18} style={{ color: '#fff', }} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.openModalUpload()}>
                    <Thumbnail source={ImageSource} style={styles.imgPost} resizeMode="contain" />
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
                null
              )
          }

          {/************************** chat and upload image  ****************************/}
          <View style={styles.inpurtWarp}>
            <View style={styles.inputContent}>
              <TouchableOpacity onPress={this.choosePicture}>
                <FA name="camera" size={20} style={styles.postIconCamera} />
              </TouchableOpacity>
              <TextInput
                defaultValue={post}
                placeholder="Type your message here"
                placeholderTextColor="#fff"
                style={styles.inputSty}
                onChangeText={(text) => this.onChangeTextInput(text, 'post')}
                autoCorrect={false}
                autoCapitalize="none"
              />
              <TouchableOpacity disabled={submitLoading === true ? true : false}
                onPress={() => ImageSource === null ? this.onSubmitWithoutImg() : this.onSubmit()}>
                <FA name="send" size={20} style={[styles.postIcon, {
                  color: submitLoading === true ? '#eee' : '#0095ff',
                  opacity: submitLoading === true ? .5 : 1,
                }]} />
              </TouchableOpacity>
            </View>
          </View>

          {/************************** popup select upload image  ****************************/}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.imgModalWarp}>
              <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
                <ANT name="close" size={30} style={{ color: '#fff', }} />
              </TouchableOpacity>
              <ImageZoom
                cropWidth={Dimensions.get('window').width}
                cropHeight={Dimensions.get('window').height - 300}
                imageWidth={Dimensions.get('window').width}
                imageHeight={'100%'}
                pinchToZoom={true}
                panToMove={true}
                enableSwipeDown={true}
                enableCenterFocus={true}
                style={{ backgroundColor: '#000', }}
              >
                {
                  imageChoose === true ?
                    <Image source={ImageSource} style={styles.imgModal} /> : modalData.itemImg !== null ?
                      <Image source={{ uri: modalData.itemImg }} style={styles.imgModal} /> :
                      <Image source={{ uri: modalData.image }} style={styles.imgModal} />
                }
              </ImageZoom>
            </View>
          </Modal>

        </ImageBackground>
      </KeyboardAvoidingView>

    );
  }
}

const styles = StyleSheet.create({
  inpurtWarp: { padding: 10, width: '100%', backgroundColor: '#000' },
  inputSty: { paddingLeft: 10, height: 40, flex: 1, color: '#fff' },
  inputContent: { borderWidth: 0, flexDirection: 'row', },
  postIcon: { padding: 10 },
  postIconCamera: { color: '#fff', padding: 10 },
  admin: {
    width: '100%', padding: 15, backgroundColor: '#000', borderRadius: 10, borderTopLeftRadius: 0,
    shadowColor: "#000", shadowOffset: { width: 0, height: 1, }, shadowOpacity: 0.22, shadowRadius: 2.22, elevation: 3,
  },
  member: {
    width: '100%', padding: 10, backgroundColor: '#fff', borderRadius: 10, borderTopRightRadius: 0,
    shadowColor: "#000", shadowOffset: { width: 0, height: 1, }, shadowOpacity: 0.22, shadowRadius: 2.22, elevation: 3,
  },
  photoSty: { height: 150, backgroundColor: '#000', marginBottom: 10, flex: 1 },
  selectImagePost: { backgroundColor: '#000', padding: 10, width: 100, height: 100, borderTopLeftRadius: 5, borderTopRightRadius: 5, marginRight: 10 },
  imgPost: { width: '100%', height: '100%', borderRadius: 5 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },
  containerImage: { width: '100%', height: '100%', resizeMode: 'cover' },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: '100%', resizeMode: 'contain', },
  titlemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 22, backgroundColor: '#333',
    width: '100%', textAlign: 'center',
  },
  closeUpload: { backgroundColor: 'red', borderRadius: 50, position: 'absolute', right: -5, top: -10 },
  MainContainer: { justifyContent: 'center', alignItems: 'center', flex: 1, height: WINDOW_HEIGHT },
  iconsty: { textAlign: 'center', marginBottom: 10, color: '#ccc' },
  emptysty: { textAlign: 'center', color: '#a9a9a9' },

});


export default Enquiry;
