import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, ActivityIndicator } from 'react-native';
import { FetchApi } from '../../../api';
import Timeline from 'react-native-timeline-flatlist';
import ANT from 'react-native-vector-icons/AntDesign';

class TimeLineStatus extends Component {
  static navigationOptions = {
    title: 'Purchase Timeline',
  };
  constructor(props) {
    super(props);
    this.state = {
      timeline: [],
      isLoading: true

    };

  }

  /******************************************************* componentDidMount *********************************************************/
  componentDidMount() {
    this.fetchPurchaseStatusTimeline();
  }


  /******************************************************* fetchPurchaseStatusTimeline *********************************************************/
  fetchPurchaseStatusTimeline() {
    // this.props.navigation.state.params.invoiceId 
    let path = "member_order_timeline_list";
    let parameters = { MMID: this.props.navigation.state.params.tokenMMID, invno: this.props.navigation.state.params.invoiceId };
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        //console.log(myJson, 'myJson');
        this.setState({
          timeline: myJson,
          isLoading: false
        })
      })
  }



  render() {
    const { timeline, isLoading } = this.state;
    console.log('timeline', timeline)

    return (
      <View style={{ flex: 1, }}>
        {
          isLoading ?
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color="#ccc" />
            </View>
            :
            timeline[0].empty === "No Record Found." ?
              <View style={styles.MainContainer}>
                <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
                <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>Timeline is Empty</Text>
              </View>
              :
              <ScrollView>
                <Timeline
                  data={timeline}
                  circleSize={20}
                  circleColor='#28a745'
                  lineColor='#28a745'
                  timeContainerStyle={{ minWidth: 80, width: 80 }}
                  timeStyle={styles.timeStyle}
                  descriptionStyle={{ color: 'gray' }}
                  options={{
                    style: styles.styleOption
                  }}
                  detailContainerStyle={styles.detailContainerStyle}
                  titleStyle={styles.titleStyle}
                  // renderFullLine={true}
                  innerCircle={'dot'}

                />
              </ScrollView>
        }
      </View>

    );
  }
}

const styles = StyleSheet.create({
  containerLoading: { flex: 1, justifyContent: 'center' },
  timeStyle: { textAlign: 'center', color: '#000', fontWeight: 'bold', padding: 5, fontSize: 12 },
  styleOption: { paddingTop: 20, paddingLeft: 5, paddingRight: 5, },
  detailContainerStyle: { borderBottomColor: '#ccc', borderBottomWidth: .5, paddingTop: 10, paddingBottom: 10, },
  titleStyle: { fontWeight: 'normal', color: 'gray', fontSize: 14 },
  MainContainer: { justifyContent: 'center', flex: 1, alignItems: 'center', },

})


export default TimeLineStatus;
