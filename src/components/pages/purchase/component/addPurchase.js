import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, ScrollView, Modal, Alert, ActivityIndicator } from 'react-native';
import { Text, Button, Thumbnail, Textarea } from 'native-base';
import RNRestart from 'react-native-restart';
import ImagePicker from 'react-native-image-picker';
import { showMessage, hideMessage } from "react-native-flash-message";
import ANT from 'react-native-vector-icons/AntDesign';
import DatePicker from 'react-native-datepicker';
import RNFetchBlob from 'rn-fetch-blob';
import moment from 'moment';

class AddPurchase extends Component {
  static navigationOptions = {
    title: 'Add Purchase',
  };

  constructor(props) {
    super(props);
    this.state = {
      invoice: '',
      date: moment().format('YYYY-MM-DD'),
      srcImg: null,
      imgUri: '',
      fileName: '',
      imgData: null,
      isSubmit: false
    };

  }

  /****************************************************** onChangeTextInput ********************************************************/
  onChangeTextInput(text, field) {
    console.log(text)
    if (field === 'invoice') { this.setState({ invoice: text }) }
  }

  /****************************************************** successPurchaseList ********************************************************/
  successPurchaseList() {
    const navigateActions = this.props.navigation.navigate;
    this.setState({
      isSubmit: false
    })
    navigateActions('purchase')
  }

  /****************************************************** choosePicture ********************************************************/
  choosePicture = () => {
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          srcImg: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          imgData: response.data
        });
      }
    });
  };

  /******************************************************* onSubmit *********************************************************/
  onSubmit() {
    console.log('onSubmit');
    const { date, invoice, imgData } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = {
      invoice: invoice,
      date: date,
    }
    //console.log('outputJson', outputJson);
    this.setState({
      isSubmit: true
    })
    if (invoice === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Name",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (date === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Email",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      const token = this.props.screenProps.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_purchase_record_add.php?';
      console.log('token', token)

      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'image', filename: 'image.jpeg', type: 'image/jpeg', data: imgData },
          { name: 'wp_date', data: date },
          { name: 'invno', data: invoice },
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' }
        ]).then((res) => res.json())
        .then((myJson) => {
          console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            Alert.alert('Invoice Update Successful, Waiting Admin To Verify', '',
              [
                { text: 'OK', onPress: () => this.successPurchaseList() },
              ],
              { cancelable: false },
            );
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => this.setState({
                    isSubmit: false,
                    invoice: '',
                    date: '',
                    srcImg: null
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
        .catch((err) => {
          console.log('error upload img', err);
        })
    }
  }

  /******************************************************* onSubmitWithoutImg *********************************************************/
  onSubmitWithoutImg() {
    console.log('onSubmitWithoutImg');
    const { date, invoice, imgData } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = {
      invoice: invoice,
      date: date,
    }
    // console.log('outputJson', outputJson);
    this.setState({
      isSubmit: true
    })
    if (invoice === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Name",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else if (date === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Email",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      const token = this.props.screenProps.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_purchase_record_add.php?';
      console.log('token', token)

      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'wp_date', data: date },
          { name: 'invno', data: invoice },
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' }
        ]).then((res) => res.json())
        .then((myJson) => {
          console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            Alert.alert('Invoice Update Successful, Waiting Admin To Verify', '',
              [
                { text: 'OK', onPress: () => this.successPurchaseList() },
              ],
              { cancelable: false },
            );
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => this.setState({
                    isSubmit: false,
                    invoice: '',
                    date: '',
                    srcImg: null
                  })
                },
              ],
              { cancelable: false },
            );
          }
        })
        .catch((err) => {
          console.log('error upload img', err);
        })
    }
  }

  render() {
    const { date, srcImg, invoice, isSubmit } = this.state;
    return (
      <ScrollView>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={styles.container}>
          <View style={styles.contentWarp}>
            {/************************** Upload Photo ****************************/}
            <View style={{ marginBottom: 20 }}>
              <Text style={styles.blueText}>Upload Photo ：</Text>
              <TouchableOpacity onPress={this.choosePicture}>
                {
                  srcImg !== null ? (
                    <Thumbnail source={srcImg} style={styles.thumbnailImg} />
                  ) : (
                      <View style={styles.uploadBtn}>
                        <ANT name="pluscircle" size={40} style={{ color: '#ccc', marginBottom: 10 }} />
                        <Text note>Please upload your photo to gallery</Text>
                      </View>
                    )
                }
              </TouchableOpacity>
            </View>

            {/************************** Invoice Number ****************************/}
            <View style={styles.titleWarp}>
              <Text style={styles.blueText}>Invoice Number ：</Text>
              <TextInput
                value={invoice}
                placeholder="Your invoice number here"
                placeholderTextColor="#B5B7BC"
                style={styles.inputSty}
                onChangeText={(text) => this.onChangeTextInput(text, 'invoice')}
                autoCorrect={false}
                autoCapitalize="none"
              />
            </View>

            {/************************** Create Date ****************************/}
            <View style={styles.titleWarp}>
              <Text style={styles.blueText}>Invoice Date：</Text>
              <DatePicker
                style={{ width: '100%' }}
                date={date}
                mode="date"
                showIcon={false}
                placeholder="select date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: { position: 'absolute', marginLeft: 0, width: 16, height: 16, },
                  dateInput: { marginLeft: 0, borderWidth: 0, alignItems: 'flex-start', },
                  placeholderText: { color: 'gray', },
                }}
                onDateChange={(date) => { this.setState({ date: date }) }}
              />
            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => srcImg === null ? this.onSubmitWithoutImg() : this.onSubmit()}>
              <Text style={styles.btntext}>Send</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputSty: { height: 50, color: '#000', fontSize: 14 },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  contentWarp: { width: '80%', marginTop: 30 },
  thumbnailImg: { width: '100%', height: 150, borderRadius: 0, marginTop: 15, },
  uploadBtn: {
    width: '100%', height: 150, borderRadius: 0, marginTop: 15, backgroundColor: '#F7F7F7', justifyContent: 'center',
    alignItems: 'center'
  },
  blueText: { color: '#2979FF' },
  titleWarp: { width: '100%', marginBottom: 20, borderBottomColor: '#ccc', borderBottomWidth: .5, },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});


export default AddPurchase;
