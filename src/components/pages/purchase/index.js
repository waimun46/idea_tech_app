import React, { Component } from 'react';
import { View, FlatList, StyleSheet, TouchableOpacity, RefreshControl, Alert, } from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Badge } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import ANT from 'react-native-vector-icons/AntDesign';
import { FetchApi } from '../../api';




class PurchaseScreen extends Component {
  static navigationOptions = {
    title: 'Purchase Record',
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      emptyRecord: [],
      refreshing: true,
      isDisabled: true,

    };
  }

  /******************************************************* componentDidMount *********************************************************/
  componentDidMount() {
    this.fetchPurchaseList();
  }

  /******************************************************* fetchPurchaseList *********************************************************/
  fetchPurchaseList() {
    // this.props.navigation.state.params.token 
    let path = "member_purchase_record_list";
    let parameters = { MMID: this.props.navigation.state.params.token, };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        //console.log(myJson, 'myJson');
        this.setState({
          data: myJson,
          emptyRecord: myJson[0],
          refreshing: false
        })
      })
  }


  /******************************************************* deleteInvoiceList *********************************************************/
  deleteInvoiceList(item, index) {
    const navigateActions = this.props.navigation.navigate;
    let path = "member_purchase_record_del";
    let parameters = { MMID: this.props.navigation.state.params.token, inv_no: item.inv_no };
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    let that = this;

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        console.log(myJson, 'myJson');
        if (myJson[0].status === 1) {
          console.log(myJson, 'remove-------------remove');
          Alert.alert('Remove Successful', '',
            [
              { text: 'OK', onPress: console.log('ok') },
            ],
            { cancelable: false },
          );
          that.setState({
            refreshing: true
          })
        }
        else {
          alert(myJson[0].error)
        }
      })
  }

  /******************************************************* _renderItem *********************************************************/
  _renderItem = ({ item, index }) => {
    const { isDisabled } = this.state;
    const navigateActions = this.props.navigation.navigate;
    return (
      this.state.emptyRecord.dataRecord === 0 ? this.ListEmpty() :
        <List style={{ backgroundColor: '#fff' }}>
          <ListItem thumbnail>
            <Left>
              {
                item.inv_file1 === null ? (
                  <Thumbnail square source={require('../../../assets/icon/imgempty.png')} />
                ) : (
                    <Thumbnail square source={{ uri: item.inv_file1 }} />
                  )
              }
            </Left>
            <Body>
              <Text>{item.inv_no}</Text>
              <View style={{ flexDirection: 'row', marginBottom: 5, marginTop: 5 }}>
                <Text note numberOfLines={1} style={{ color: 'gray' }}>status :</Text>
                <Text note numberOfLines={1} style={{
                  color: item.inv_status === 'Verified' ? 'green' : item.inv_status === 'Pending' ? '#6c757d' : '#dc3545',
                  fontWeight: 'bold'
                }}>{item.inv_status}</Text>
              </View>
              <Text note numberOfLines={1}>{item.inv_date}</Text>
            </Body>
            <Right style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
              <TouchableOpacity onPress={() => navigateActions('timeline', { invoiceId: item.inv_no, tokenMMID: this.props.navigation.state.params.token })}
                style={{ marginRight: 20 }}>
                {/* <ANT name="barschart" size={25} style={{ color: '#ffc107' }} /> */}
                <Badge style={{ backgroundColor: 'gray' }}><Text>{item.CountUnReadMsg}</Text></Badge>
              </TouchableOpacity>
              {
                item.inv_status === 'Verified' ? (
                  <TouchableOpacity disabled={item.inv_status === 'Verified' ? isDisabled : isDisabled === false}>
                    <ANT name="closecircle" size={25} style={{ color: item.inv_status === 'Verified' ? '#ccc' : 'red' }} />
                  </TouchableOpacity>
                ) : (
                    <TouchableOpacity onPress={this.deleteInvoiceList.bind(this, item)}>
                      <ANT name="closecircle" size={25} style={{ color: item.inv_status === 'Verified' ? '#ccc' : 'red' }} />
                    </TouchableOpacity>
                  )
              }
            </Right>
          </ListItem>
        </List >
    )
  }

  /******************************************************* ListEmpty *********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>Data Empty</Text>
      </View>
    );
  };


  render() {
    const { data, refreshing } = this.state;
    const navigateActions = this.props.navigation.navigate;
    //console.log('data-----', data)


    return (
      <View style={{ backgroundColor: '#EFEFEF', flex: 1 }}>

        {/************************** list data ****************************/}
        <FlatList
          data={data}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{ paddingBottom: 130, }}
          ListEmptyComponent={this.ListEmpty}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.fetchPurchaseList()}
            />}
        />

        {/************************** add comment button ****************************/}
        <View style={styles.addComment}>
          <TouchableOpacity onPress={() => navigateActions('addPurchase')}>
            <View style={styles.addBtn}>
              <FA name="plus" size={25} style={{ color: '#fff', }} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
  addComment: { position: 'absolute', bottom: 50, right: 20, },
  addBtn: {
    backgroundColor: '#0095ff', width: 50, height: 50, justifyContent: 'center',
    alignItems: 'center', borderRadius: 50, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  }

});

export default PurchaseScreen;
