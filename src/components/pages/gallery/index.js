import React, { Component } from 'react';
import {
  StyleSheet, Text, View, Button, Dimensions, Image, TouchableOpacity, ImageBackground, RefreshControl,
  SafeAreaView, ActivityIndicator, FlatList,
} from 'react-native';
import { Tab, Tabs, ScrollableTab } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import ANT from 'react-native-vector-icons/AntDesign';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar, CustomTabBar } from 'react-native-scrollable-tab-view-forked';
import GalleryContent from './component/galleryContent';
import { FetchApiNoParam, FetchApi } from '../../api';
import { withNavigation } from 'react-navigation';


class GalleryScreen extends Component {
  static navigationOptions = {
    title: 'Our Memories',
  };

  constructor(props) {
    super(props);
    this.state = {
      galleryData: [],
      offsetValue: 1,
      loadingScroll: false,
      refreshing: true,
      renderPage: false,
      loadingScroll: false,
      activeKey: '',
      activePage: 0,
      dateDate: [],
    }
  } 

  /****************************************** componentDidMount ********************************************/
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
    // setInterval(this.fetchGalleryData, 2000);
    this.fetchDateTabs();
    // const { navigation } = this.props;
    // console.log('navigation---GalleryContent', navigation)
    // this.focusListener = navigation.addListener('didFocus', () => {
    //   console.log('componentDidMount----fetchGalleryDataListTest');
    //   this.fetchGalleryDataListReflash();
    // });
  }

  // componentWillUnmount() {
  //   console.log('componentWillUnmount----');
  //   this.fetchGalleryDataListReflash().remove();
  // }

  /****************************************** fetchDateTabs ********************************************/
  fetchDateTabs() {
    let path = "gallery_list_date";
    let ApiData = FetchApiNoParam(path);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          dateDate: fetchData,
          loadingScroll: false,

        })
      })
  }


  /****************************************** fetchGalleryData ********************************************/
  // fetchGalleryData = () => {
  //   let path = "gallery_list";
  //   let parameters = { limit: '20', offset: this.state.offsetValue }
  //   let ApiData = FetchApi(path, parameters);
  //   console.log(ApiData, 'ApiData');

  //   fetch(ApiData).then((res) => res.json())
  //     .then((fetchData) => {
  //       this.setState({
  //         galleryData: fetchData[0].date_list,
  //         // galleryData: this.state.galleryData.concat(fetchData[0].date_list),
  //         loadingScroll: false,
  //         // refreshing: false,
  //       })
  //     })
  // }


  // activeKey(obj) {
  //   console.log('activeKey----', obj)
  //   this.setState({
  //     activeKey: obj.ref.props.data.DateMonth
  //   })
  // }

  // onChangeTab(obj) {
  //   console.log('change', obj);
  //   this.setState({
  //     activePage: obj.ref.props.heading,
  //   }, this.fetchGalleryDataList)
  // }



  render() {
    const { galleryData, refreshing, renderPage, dateDate,  } = this.state;
    const navigateActions = this.props.navigation.navigate;
    const token = this.props.screenProps.mmidToken;
    // console.log('galleryData---------', galleryData)
    // console.log('activeKey', this.state.activePage)
    // console.log('dateDate---------', dateDate)


    return (
      <View style={{ backgroundColor: '#EFEFEF', flex: 1 }}>
        {
          renderPage &&
          // <ScrollableTabView
          //   renderTabBar={() => (
          //     <ScrollableTabBar
          //       style={styles.scrollStyle}
          //       tabStyle={styles.tabStyle}
          //     />
          //   )}
          //   tabBarTextStyle={styles.tabBarTextStyle}
          //   tabBarInactiveTextColor={'#666666'}
          //   tabBarActiveTextColor={'#2979FF'}
          //   tabBarUnderlineStyle={styles.underlineStyle}
          //   initialPage={0}
          //   onChangeTab={(obj) => this.activeKey(obj)}
          // >
          //   {
          //     galleryData.map((item, index) => {
          //       return (
          //         <GalleryContent key={item.DateMonth} tabLabel={item.DateMonth} data={galleryData[index]} navigateActions={navigateActions}
          //           renderFooter={this.renderFooter} ListEmpty={this.ListEmpty} handleLoadMore={this.handleLoadMore}
          //           fetchGalleryData={this.fetchGalleryData}
          //         // refreshing={refreshing}
          //         />

          //       )
          //     })
          //   } 
          // </ScrollableTabView>
          <Tabs renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#fff' }} />}
            // onChangeTab={(obj) => this.onChangeTab(obj)}
          >
            {
              dateDate.map((item, index) => {
                return (
                  <Tab heading={item.date_list} tabStyle={{ backgroundColor: '#fff' }} activeTabStyle={{ backgroundColor: '#fff' }}  >
                    <GalleryContent  dateGallery={item.date_list} navigateActions={navigateActions} />
                  </Tab>
                )
              })
            }
          </Tabs>
        }


        {/************************** add comment button ****************************/}
        {
          token === null ? null :
            <View style={styles.addComment}>
              <TouchableOpacity onPress={() => navigateActions('galleryPost')}>
                <View style={styles.addBtn}>
                  <FA name="plus" size={25} style={{ color: '#fff', }} />
                </View>
              </TouchableOpacity>
            </View>
        }

      </View>
    )
  }

}




const styles = StyleSheet.create({
  activeTabStyle: { color: '#2979FF' },
  tabStyle: { paddingRight: 15, width: '100%', color: '#2979FF', paddingLeft: 15, },
  scrollStyle: {
    backgroundColor: 'white', width: '100%', paddingRight: 0, height: 50, paddingLeft: 0,
    justifyContent: 'center',
  },
  tabBarTextStyle: { fontSize: 14, fontWeight: 'bold', lineHeight: 50 },
  underlineStyle: { height: 3, backgroundColor: '#2979FF', borderRadius: 3, width: 30, alignItems: 'center' },
  addComment: { position: 'absolute', bottom: 50, right: 20, },
  addBtn: {
    backgroundColor: '#0095ff', width: 50, height: 50, justifyContent: 'center',
    alignItems: 'center', borderRadius: 50, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 10,
  },
  loadingFooter: { marginTop: 10, alignItems: 'center' },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: 'gray', alignItems: 'center', justifyContent: 'center' },




});

export default withNavigation(GalleryScreen);