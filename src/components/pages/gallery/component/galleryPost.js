import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, ScrollView, ActivityIndicator, Modal, Alert } from 'react-native';
import { Text, Button, Thumbnail, Textarea } from 'native-base';
import RNRestart from 'react-native-restart';
import ImagePicker from 'react-native-image-picker';
import { showMessage, hideMessage } from "react-native-flash-message";
import ANT from 'react-native-vector-icons/AntDesign';
import RNFetchBlob from 'rn-fetch-blob';


class GalleryPost extends Component {
  static navigationOptions = {
    title: 'Post Gallery',
  };

  constructor(props) {
    super(props);
    this.state = {
      message: '',
      srcImg: null,
      title: '',
      imgData: null,
      galleryData: [],
      isSubmit: false
    };
  }

  // componentDidMount(){
  //   setInterval(this.props.fetchGalleryData, 5000, console.log('status-----'));
  // }


  /************************************************* onChangeTextInput ***************************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'title') { this.setState({ title: text }) }
    if (field === 'message') { this.setState({ message: text }) }
  }

  /******************************************************** choosePicture ***************************************************/
  choosePicture = () => {
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // console.log(response.fileName);
        // console.log(response, '----------response');
        this.setState({
          srcImg: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          imgData: response.data
        });
      }
    });
  };

  /******************************************************** onSubmit **********************************************************/
  onSubmit() {
    const { message, srcImg, title, imgData } = this.state;
    const navigateActions = this.props.navigation.navigate;
    let outputJson = {
      title: title,
      message: message,
      srcImg: srcImg,
    }
    //console.log('outputJson', outputJson);

    this.setState({
      isSubmit: true
    })

    if (title === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Title",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    // else if (message === "") {
    //   this.setState({
    //     error: showMessage({
    //       message: "Please Fill In Your message",
    //       description: "Error Messages",
    //       type: "danger",
    //       icon: 'warning'
    //     }),
    //     isSubmit: false
    //   })
    // }
    else if (srcImg === null) {
      this.setState({
        error: showMessage({
          message: "Please Upload Your Image",
          description: "Error Messages",
          type: "danger",
          icon: 'warning'
        }),
        isSubmit: false
      })
    }
    else {
      // navigateActions('galleryDetails')
      const token = this.props.screenProps.mmidToken;
      let url = 'https://apps.idealtech.com.my/api/member_gallery_add.php?';
      //console.log('token', token)

      let that = this;

      RNFetchBlob.fetch('POST', url, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'image', filename: 'image.jpeg', type: 'image/jpeg', data: imgData },
          { name: 'gp_title', data: title },
          { name: 'content', data: message },
          { name: 'MMID', data: token },
          { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' }
        ]).then((res) => res.json())
        .then((myJson) => {
          console.log('myJson', myJson);
          if (myJson[0].status === 1) {
            // navigateActions('gallery') 
            that.setState({
              isSubmit: false,
              title: '',
              message: '',
              srcImg: null
            })
            // RNRestart.Restart()
            navigateActions('gallery')
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => this.setState({
                    isSubmit: false,
                    title: '',
                    message: '',
                    srcImg: null
                  })
                },
              ],
              { cancelable: false },
            )
          }
        })
        .catch((err) => {
          console.log('error upload img', err);
        })
    }

  }


  render() {
    const { message, srcImg, title, isSubmit } = this.state;

    console.log('fetchGalleryData', this.props.fetchGalleryData)

    return (
      <ScrollView>

        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }

        <View style={styles.container}>
          <View style={styles.contentWarp}>
            {/************************** message Textarea ****************************/}
            <View style={styles.titleWarp}>
              <Text style={styles.blueText}>Title ：</Text>
              <TextInput
                value={title}
                placeholder="Your title here"
                placeholderTextColor="#B5B7BC"
                style={styles.inputSty}
                onChangeText={(text) => this.onChangeTextInput(text, 'title')}
                autoCorrect={false}
                autoCapitalize="none"
              />
            </View>

            {/************************** Content area ****************************/}
            {/* <View>
              <Text style={styles.blueText}>Content area ：</Text>
              <Textarea
                rowSpan={5}
                bordered
                value={message}
                placeholder="Please fill up your message..."
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'message')}
                style={{ borderRadius: 5, marginTop: 15 }}
                autoCapitalize="none"
                autoCorrect={false}
              />
            </View> */}

            {/************************** Upload Photo ****************************/}
            <View style={{ marginTop: 20 }}>
              <Text style={styles.blueText}>Upload Photo ：</Text>
              <TouchableOpacity onPress={this.choosePicture}>
                {
                  srcImg !== null ? (
                    <Thumbnail source={srcImg} style={styles.thumbnailImg} />
                  ) : (
                      <View style={styles.uploadBtn}>
                        <ANT name="pluscircle" size={40} style={{ color: '#ccc', marginBottom: 10 }} />
                        <Text note>Please upload your photo to gallery</Text>
                      </View>
                    )
                }
              </TouchableOpacity>
            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>Post</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  inputSty: { height: 50, color: '#000', fontSize: 14 },
  btnwarp: { marginTop: 30, borderRadius: 8, height: 50, },
  btntext: { color: '#fff', fontSize: 20 },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  contentWarp: { width: '80%', marginTop: 30 },
  thumbnailImg: { width: '100%', height: 300, borderRadius: 0, marginTop: 15, },
  uploadBtn: {
    width: '100%', height: 300, borderRadius: 0, marginTop: 15, backgroundColor: '#F7F7F7', justifyContent: 'center',
    alignItems: 'center'
  },
  blueText: { color: '#2979FF' },
  titleWarp: { width: '100%', marginBottom: 20, borderBottomColor: '#ccc', borderBottomWidth: .5, },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});


export default GalleryPost;
