import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Image, TouchableOpacity, FlatList, ScrollView, ImageBackground, RefreshControl, ActivityIndicator } from 'react-native';
import FA from 'react-native-vector-icons/FontAwesome';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import ANT from 'react-native-vector-icons/AntDesign';
import { FetchApiNoParam, FetchApi } from '../../../api';
import { withNavigation } from 'react-navigation';

const numColumns = 3;



class GalleryContent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isShowToTop: false,
      renderPage: false,
      galleryPhoto: [],
      offsetValue: 1,
      loadingScroll: false,
      galleryPhotoList: []
    }
  }

  /****************************************** componentDidMount ********************************************/
  componentDidMount() {
    // setInterval(this.fetchGalleryDataList, 2000);
    const { navigation } = this.props;
    console.log('navigation---GalleryContent', navigation)
    this.focusListener = navigation.addListener('didFocus', () => {
      console.log('componentDidMount----fetchGalleryDataListTest');
      this.fetchGalleryDataListReflash();
      // this.fetchGalleryDataList();
    });
    this.fetchGalleryDataList();
    
  }

  /****************************************** componentWillUnmount ********************************************/
  componentWillUnmount() {
    console.log('componentWillUnmount----');
    this.fetchGalleryDataListReflash().remove();
    this.fetchGalleryDataList().remove();
  }

  /****************************************** fetchGalleryDataList ********************************************/
  fetchGalleryDataList() {
    let path = "gallery_list_photo";
    let parameters = { YearMonth: this.props.dateGallery, limit: '20', offset: this.state.offsetValue }
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData ----fetchGalleryDataList');
    console.log('photo---', this.props.dateGallery)

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          galleryPhoto: this.state.galleryPhoto.concat(fetchData),
          loadingScroll: false,
        })
      })
  }

  /****************************************** fetchGalleryDataListReflash ********************************************/
  fetchGalleryDataListReflash() {
    let path = "gallery_list_photo";
    let parameters = { YearMonth: this.props.dateGallery, limit: '20', offset: this.state.offsetValue }
    let ApiData = FetchApi(path, parameters);
    // console.log(ApiData, 'ApiData ----fetchGalleryDataList');
    // console.log('photo---', this.props.dateGallery)

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          galleryPhoto: fetchData,
        })
      })
  }

  /************************************************************ ListEmpty ***********************************************************/
  handleScroll = (e) => {
    //console.log(e.nativeEvent.contentOffset.y);
    let offsetY = e.nativeEvent.contentOffset.y;
    if (offsetY > 50) {
      this.setState({
        isShowToTop: true
      })
    } else {
      this.setState({
        isShowToTop: false
      })
    }
  }

  /************************************************************ _scrollTop ***********************************************************/
  _scrollTop = () => {
    this.refs.gallery.scrollToOffset({ offset: 0, animated: true })
  }

  /****************************************** _renderItem ********************************************/
  _renderItem = ({ item }) => {
    return (
      <View style={styles.listWarp}>
        <TouchableOpacity onPress={() => this.props.navigateActions('galleryDetails', {
          itemid: item.photo_id
        })}>
          <ImageBackground source={{ uri: item.photo_path }} style={styles.imgWarp}>
            <View style={styles.overlay} />
            <View style={styles.content}>
              <View style={{ flexDirection: 'row', }}>
                <View style={{ flexDirection: 'row', marginRight: 10 }}>
                  <ANT name="heart" size={20} style={{ color: '#ff5d50', marginRight: 5 }} />
                  <Text style={{ color: '#fff' }}>{item.totalLike}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <MCI name="message-reply-text" size={20} style={{ color: '#fff', marginRight: 5 }} />
                  <Text style={{ color: '#fff' }}>{item.totalComment}</Text>
                </View>
              </View>
            </View>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    )
  }

  /************************************************************ ListEmpty ***********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>No data</Text>
      </View>
    );
  };

  /******************************************************** renderFooter ********************************************************/
  renderFooter = () => {
    console.log('renderFooter')
    return (
      <TouchableOpacity style={{ marginTop: 20 }} onPress={this.handleLoadMore}>
        <View style={[styles.loadingFooter]}>
          {
            this.state.loadingScroll ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                <ActivityIndicator color="#ccc" />
              </View>
              : null
          }
          <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
        </View>
      </TouchableOpacity>
    )
  }

  /******************************************************** handleLoadMorePromotion ********************************************************/
  handleLoadMore = () => {
    console.warn('handleLoadMore');
    this.setState({
      offsetValue: this.state.offsetValue + 1,
      loadingScroll: true
    }, this.fetchGalleryDataList)

  }



  render() {

    const { isShowToTop, galleryPhoto } = this.state;
    console.log('dateGallery', this.props.dateGallery)
    console.log('galleryPhoto', galleryPhoto.length)
    console.log('kkkk', this.props.dateGallery)


    return (
      <View style={{ flex: 1 }}>
        <View >
          <FlatList
            data={galleryPhoto}
            renderItem={this._renderItem}
            numColumns={numColumns}
            keyExtractor={(item, index) => index.toString()}
            //onEndReached={this.props.handleLoadMore}
            //onEndReachedThreshold={0}ß
            ListFooterComponent={galleryPhoto.length > 19 ? this.renderFooter : null}
            //ListFooterComponent={this.props.renderFooter}
            ListEmptyComponent={this.ListEmpty}
            contentContainerStyle={{ paddingBottom: 30 }}
            ref="gallery"
            // onScroll={this.handleScroll}
            initialNumToRender={0}
          // refreshControl={
          //   <RefreshControl
          //     refreshing={this.props.refreshing}
          //     onRefresh={this.props.fetchGalleryData}
          //   />}
          />
        </View>


        {/************************** scroll top buttom ****************************/}
        {
          isShowToTop ?
            <TouchableOpacity onPress={this._scrollTop} style={styles.scrollBtn}>
              <ANT name='up' style={styles.iconTop} />
            </TouchableOpacity>
            : null
        }


      </View>
    )
  }

}




const styles = StyleSheet.create({
  listWarp: { width: '33.33%', height: 130, padding: 1, marginBottom: 1 },
  imgWarp: { width: '100%', height: '100%', },
  overlay: { backgroundColor: '#000', opacity: 0.4, height: 129, width: '100%', },
  content: { position: 'absolute', height: 130, width: '100%', alignItems: 'center', justifyContent: 'center', },
  scrollBtn: {
    position: 'absolute', flex: 0.1, right: 20, bottom: 30, borderRadius: 50,
    backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
    color: 'red', textAlign: 'center'
  },
  iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: 'gray', alignItems: 'center', justifyContent: 'center' },




});

export default withNavigation(GalleryContent);