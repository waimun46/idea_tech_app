import React, { Component } from 'react';
import {
  StyleSheet, View, TouchableOpacity, FlatList, ScrollView, ImageBackground, TextInput, Alert, RefreshControl,
  Modal, ActivityIndicator, Dimensions, KeyboardAvoidingView, NativeModules, StatusBarIOS, Platform
} from 'react-native';
import { List, ListItem, Left, Body, Right, Thumbnail, Text, Spinner } from 'native-base';
import FA from 'react-native-vector-icons/FontAwesome';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import ANT from 'react-native-vector-icons/AntDesign';
import { FetchApi } from '../../../api';
import { showMessage, hideMessage } from "react-native-flash-message";
import { withNavigation } from 'react-navigation';

const { StatusBarManager } = NativeModules;

const localData = [
  { emptyProfile: require('../../../../assets/icon/empty.png') }
]

//const heightImage = Dimensions.get('window').height - 350;


class GalleryDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: '',
      topList: [],
      commentList: [],
      isLoading: true,
      refreshing: false,
      isSubmit: false,
      likeStatus: [],
      loadingScroll: false,
      offsetValue: 1,
      isListEmpty: true,
      dataList: [],
      dataComment: [],
      statusBarHeight: 0,
    }
  }

  /******************************************************* componentDidMount *********************************************************/
  componentDidMount() {
    if (this.props.screenProps.mmidToken !== null) {
      this.fetchGalleryListMMID();
    } else {
      this.fetchGalleryList();
    }
    // this.fetchGalleryListMMID();
    this.fetchGalleryComment();
    // const { navigation } = this.props;
    // this.focusListener = navigation.addListener('didFocus', () => {
    //   this.fetchGalleryComment();
    // });
    if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((statusBarFrameData) => {
        this.setState({ statusBarHeight: statusBarFrameData.height });
      });
      this.statusBarListener = StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
        this.setState({ statusBarHeight: statusBarData.frame.height });
      });
    }
  }

  /****************************************** componentWillUnmount ********************************************/
  // componentWillUnmount() { 
  //   // console.log('componentWillUnmount----');
  //   // this.fetchGalleryComment().remove();
  //   this.statusBarListener.remove();
  // }

  /******************************************************* fetchGalleryComment *********************************************************/
  fetchGalleryComment() {
    const photoID = this.props.navigation.getParam('itemid');
    let path = "gallery_detail_comment";
    let parameters = { photo_id: photoID, };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');
    //console.log('fetchGalleryComment');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          dataComment: fetchData,
          isLoading: false,
          refreshing: false,
          loadingScroll: false,
        })
      })
  }

  /******************************************************* fetchGalleryList *********************************************************/
  fetchGalleryList() {
    const photoID = this.props.navigation.getParam('itemid');
    let path = "gallery_detail";
    let parameters = { photo_id: photoID, };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');
    //console.log('fetchGalleryList');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          likeStatus: fetchData[0],
          topList: fetchData[0],
          commentList: fetchData,
          isLoading: false,
          refreshing: false,
          loadingScroll: false,
        })
      })
  }
  /******************************************************* fetchGalleryListMMID *********************************************************/
  fetchGalleryListMMID() {
    const photoID = this.props.navigation.getParam('itemid');
    let path = "gallery_detail";
    let parameters = { photo_id: photoID, MMID: this.props.screenProps.mmidToken, };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');
    //console.log('fetchGalleryListMMID');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          likeStatus: fetchData[0],
          topList: fetchData[0],
          commentList: fetchData,
          isLoading: false,
          refreshing: false,
          loadingScroll: false,
        })
      })
  }

  /******************************************************* onChangeTextInput *********************************************************/
  onChangeTextInput(text, field) {
    console.log(text)
    if (field === 'post') { this.setState({ post: text }) }
  }

  /******************************************************* addLikeUnlogin *********************************************************/
  addLikeUnlogin() {
    Alert.alert('Please Login To Like This Post', '',
      [
        { text: 'OK', onPress: console.log('ok like') },
      ],
      { cancelable: false },
    );
  }

  /******************************************************* addLike *********************************************************/
  addLike() {
    const photoID = this.props.navigation.getParam('itemid');
    let path = "gallery_add_like";
    let parameters = { MMID: this.props.screenProps.mmidToken, photo_id: photoID, };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    let that = this
    const items = this.state.likeStatus
    const totals = this.state.addLike

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        if (myJson[0].status === 1) {
          // Alert.alert('Like Successful', '',
          //   [
          //     { text: 'OK', onPress: console.log('ok like') },
          //   ],
          //   { cancelable: false },
          // );
          this.state.likeStatus.likeStatus = 1;
          this.state.likeStatus.totalLike = this.state.likeStatus.totalLike + 1
          items = this.state.likeStatus.likeStatus || this.state.likeStatus.totalLike
          console.log(items, 'items')
          that.setState({
            likeStatus: items,
          })
        }
        else {
          alert('You are already add like')
        }
      })
  }

  /******************************************************* addComment *********************************************************/
  addComment() {
    const { post } = this.state;
    this.setState({
      isSubmit: true,
    })

    if (post === "") {
      this.setState({
        error: showMessage({
          message: "Please Fill In Your Comment",
          description: "Error Messages",
          type: "danger",
          icon: 'warning',
        }),
        isSubmit: false
      })
    }
    else {
      const photoID = this.props.navigation.getParam('itemid');
      let path = "gallery_add_comment";
      let parameters = { MMID: this.props.screenProps.mmidToken, photo_id: photoID, comment: post };
      let ApiData = FetchApi(path, parameters);
      //console.log(ApiData, 'ApiData');

      let that = this;

      fetch(ApiData).then((res) => res.json())
        .then((myJson) => {
          if (myJson[0].status === 1) {
            that.setState({
              isSubmit: false,
              post: '',
              refreshing: true
            })
          }
          else {
            Alert.alert(myJson[0].error, '',
              [
                {
                  text: 'OK', onPress: () => this.setState({
                    isSubmit: false,
                    post: '',
                  })
                },
              ],
              { cancelable: false },
            )
          }
        })
    }
  }

  /******************************************************* _renderItem *********************************************************/
  _renderItem = ({ item, index }) => {
    return (
      <List>
        <ListItem avatar>
          <Left>
            <Thumbnail source={{ uri: item.photo }} />
          </Left>
          <Body>
            <View style={styles.commentContent}>
              <Text >{item.commenter}</Text>
              <Text note style={{ color: '#99A2B3' }}>{item.timeAgo}</Text>
            </View>
            <Text style={styles.commentText}>{item.comment}</Text>
          </Body>
        </ListItem>
      </List>
    )
  }

  /******************************************************* ListEmpty *********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="filetext1" size={60} style={styles.iconsty} />
        <Text style={styles.emptysty}>No Comment</Text>
      </View>

    );
  };

  /******************************************************** handleLoadMore ********************************************************/
  handleLoadMore = () => {
    console.warn('handleLoadMore---PromotionData');
    this.setState({
      offsetValue: this.state.offsetValue + 1,
      loadingScroll: true,
      refreshing: false,
    }, this.fetchGalleryComment)
    // setTimeout(() => {
    //   this.setState({
    //     offset: this.state.offset + 1,
    //     loadingScroll: true
    //   }, this.fetchLasterData)

    // }, 1000)

  }

  /******************************************************** renderFooter ********************************************************/
  renderFooter = () => {
    return (
      this.state.dataList.totalComment === 0 ? null :
        <TouchableOpacity style={{ marginTop: 20 }} onPress={this.handleLoadMore}>
          <View style={[styles.loadingFooter]}>
            {
              this.state.loadingScroll ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                  <ActivityIndicator color="#ccc" />
                </View>
                : null
            }
            <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
          </View>
        </TouchableOpacity>
    )
  }


  render() {
    const { post, topList, commentList, refreshing, isSubmit, likeStatus, isLoading, dataComment, statusBarHeight } = this.state;
    const profilePhoto = this.props.screenProps.profile.photo;
    const token = this.props.screenProps.mmidToken;

    return (
      <KeyboardAvoidingView style={{ flex: 1, }} behavior={Platform.OS === 'ios' ? 'padding' : null} enabled keyboardVerticalOffset={44 + statusBarHeight}>
      {/* <KeyboardAvoidingView style={{ flex: 1, }} behavior='padding' enabled keyboardVerticalOffset={44 + statusBarHeight}> */}
        <View style={{ width: '100%', flex: 1, }}>
          {/************************** submit loading ****************************/}
          {
            isSubmit ?
              <Modal
                transparent={true}
                animationType={'none'}
                visible={isSubmit}
                onRequestClose={() => { console.log('close modal') }}>
                <View style={styles.modalBackground}>
                  <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator size="large" animating={isSubmit} />
                  </View>
                </View>
              </Modal>
              : null
          }

          {
            isLoading ? (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <Spinner color='#ccc' />
              </View>
            ) : (
                <ScrollView
                  ref={ref => this.scrollView = ref}
                  keyboardShouldPersistTaps='always'
                onContentSizeChange={() => this.scrollView.scrollToEnd({ animated: true })}
                >
                  <ImageBackground source={{ uri: topList.photo_path }} style={styles.imagebg}>
                    <View style={styles.overlay} />

                    <View style={styles.content} >
                      <View style={{ justifyContent: 'flex-start', padding: 20, }}>
                        <Text style={styles.imageTitle}>{topList.photo_author}</Text>
                      </View>
                      <View style={{ padding: 20, justifyContent: 'flex-end', flex: 1 }}>
                        <Text style={styles.imageTitle}>{topList.photo_title}</Text>
                        <View style={styles.lineSty} />
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ alignItems: 'flex-start', width: '50%' }}>
                            <Text style={styles.dateSty}>{topList.create_dt}</Text>
                          </View>
                          <View style={{ alignItems: 'flex-end', width: '50%' }}>
                            <View style={{ flexDirection: 'row', }}>
                              <TouchableOpacity onPress={() => token === null ? this.addLikeUnlogin() : this.addLike()}>
                                <View style={styles.likeWarp}>
                                  <ANT name="heart" size={20} style={{ color: likeStatus.likeStatus === 1 ? '#ff5d50' : '#ccc', marginRight: 5 }} />
                                  <Text style={{ color: '#fff' }}>{topList.totalLike}</Text>
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity>
                                <View style={{ flexDirection: 'row' }}>
                                  <MCI name="message-reply-text" size={20} style={{ color: '#fff', marginRight: 5 }} />
                                  <Text style={{ color: '#fff' }}>{topList.totalComment}</Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </ImageBackground>


                  <View style={{ marginTop: 5, marginBottom: 150 }}>
                    <FlatList
                      data={dataComment}
                      renderItem={this._renderItem}
                      keyExtractor={(item, index) => index.toString()}
                      //ListFooterComponent={this.renderFooter}
                      ListEmptyComponent={this.ListEmpty}
                      refreshControl={
                        <RefreshControl
                          refreshing={refreshing}
                          onRefresh={this.fetchGalleryComment()}
                        />}
                      // inverted={-1}
                    />
                    {
                      // dataComment.length === 0 ? null :
                      //   <TouchableOpacity style={{ marginTop: 20 }} onPress={this.handleLoadMore}>
                      //     <View style={[styles.loadingFooter]}>
                      //       {
                      //         this.state.loadingScroll ?
                      //           <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                      //             <ActivityIndicator color="#ccc" />
                      //           </View>
                      //           : null
                      //       }
                      //       <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
                      //     </View>
                      //   </TouchableOpacity>
                    }
                  </View>
                </ScrollView>
              )
          }

          {/********************************** post comment ************************************/}
          {
            token === null ?
              null :
              <View style={styles.postWarp}>
                <View style={{ flexDirection: 'row', }}>
                  <View style={{ width: '15%' }}>
                    {
                      profilePhoto === "" || profilePhoto === null ?
                        <Thumbnail source={localData[0].emptyProfile} />
                        :
                        <Thumbnail source={{ uri: profilePhoto }} />
                    }
                  </View>
                  <View style={styles.inpurtWarp}>
                    <View style={styles.inputContent}>
                      <TextInput
                        defaultValue={post}
                        placeholder="Add a comment..."
                        placeholderTextColor="#99A2B3"
                        style={{ paddingLeft: 10, height: 40, flex: 1, }}
                        onChangeText={(text) => this.onChangeTextInput(text, 'post')}
                        autoCorrect={false}
                        autoCapitalize="none"
                      />
                      <TouchableOpacity onPress={() => this.addComment()}>
                        <FA name="send" size={20} style={styles.postIcon} />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
          }

        </View>
      </KeyboardAvoidingView>
    )
  }

}




const styles = StyleSheet.create({
  imagebg: { width: '100%', height: 400, resizeMode: 'cover', },
  imageTitle: { color: '#fff', fontSize: 22, fontWeight: 'bold', marginBottom: 10 },
  lineSty: { width: 100, height: 3, backgroundColor: '#fff', marginBottom: 20 },
  dateSty: { color: '#fff', fontWeight: 'bold' },
  likeWarp: { flexDirection: 'row', marginRight: 10, },
  commentContent: { flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' },
  commentText: { marginTop: 10, color: 'gray', fontSize: 14 },
  postWarp: {
    position: 'absolute', bottom: 0, width: '100%', paddingTop: 15, paddingBottom: 15, paddingRight: 20, paddingLeft: 20,
    borderTopWidth: .5, borderColor: '#ccc', backgroundColor: '#fff'
  },
  inpurtWarp: { paddingLeft: 10, width: '85%', marginTop: 5 },
  inputContent: { borderWidth: .5, borderColor: '#C1C1C1', borderRadius: 8, flexDirection: 'row', },
  postIcon: { color: '#0095ff', padding: 10 },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 50, },
  iconsty: { textAlign: 'center', marginBottom: 10, color: '#ccc' },
  emptysty: { textAlign: 'center', color: '#a9a9a9' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: 'gray', alignItems: 'center', justifyContent: 'center' },
  overlay: { backgroundColor: '#000', opacity: 0.25, height: 400, width: '100%', },
  content: { position: 'absolute', height: 400, width: '100%', },

});

export default withNavigation(GalleryDetails);