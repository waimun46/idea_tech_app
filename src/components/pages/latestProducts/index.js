import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator, Modal } from 'react-native';
import { Card, Text, Button } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import { FetchApi } from '../../api';
import ImageZoom from 'react-native-image-pan-zoom';
import RNFetchBlob from 'rn-fetch-blob';

const numColumns = 2;

class LatestProductScreen extends Component {
  static navigationOptions = {
    title: 'Latest Products',
  };
  constructor(props) {
    super(props);
    this.state = {
      lasterData: [],
      offsetValue: 1,
      loadingScroll: false,
      dataEmpty: [],
      isShowToTop: false,
      renderPage: false,
      isLoading: true,
      modalVisible: false,
      modalData: { image: '', title: '' },
    };
  }

  /********************************************************** componentDidMount ************************************************************/
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
    this.fetchLasterData();
  }


  /********************************************************** fetchLasterData ************************************************************/
  fetchLasterData() {
    let path = "latest_product";
    let parameters = { limit: '20', offset: this.state.offsetValue }
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          lasterData: this.state.lasterData.concat(fetchData),
          loadingScroll: false,
          dataEmpty: fetchData[0],
          isLoading: false
        })
      })
  }


  /************************************************* Render Item in renderItemLaster *************************************************/
  renderItemLaster = ({ item }) => {
    return (
      <Card style={{ width: '50%', padding: 1, marginLeft: 0, marginRight: 0 }}>
        <TouchableOpacity onPress={() => this.openModal(item.last_id)}>
          <View style={{}}>
            <Image source={{ uri: item.last_path }} style={{ height: 250, width: '100%', resizeMode: 'cover' }} />
          </View>
          <View style={{ backgroundColor: '#000', padding: 10, }}>
            <Text style={{ textAlign: 'center', color: '#fff', fontWeight: 'bold' }} numberOfLines={1}>{item.last_name}</Text>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  /******************************************************** handleLoadMore ********************************************************/
  handleLoadMore = () => {
    console.warn('handleLoadMore---PromotionData');
    this.setState({
      offsetValuePromo: this.state.offsetValuePromo + 1,
      loadingScroll: true
    }, this.fetchPromotionData)
    // setTimeout(() => {
    //   this.setState({
    //     offset: this.state.offset + 1,
    //     loadingScroll: true
    //   }, this.fetchLasterData)

    // }, 1000)

  }

  /******************************************************** renderFooter ********************************************************/
  renderFooter = () => {
    return (
      <TouchableOpacity style={{ marginTop: 20 }} onPress={this.handleLoadMore}>
        <View style={[styles.loadingFooter]}>
          {
            this.state.loadingScroll ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                <ActivityIndicator color="#ccc" />
              </View>
              : null
          }
          <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
        </View>
      </TouchableOpacity>
    )
  }

  /************************************************************ ListEmpty ***********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>No data</Text>
      </View>
    );
  };

  /************************************************************ ListEmpty ***********************************************************/
  handleScroll = (e) => {
    //console.log(e.nativeEvent.contentOffset.y);
    let offsetY = e.nativeEvent.contentOffset.y;
    if (offsetY > 50) {
      this.setState({
        isShowToTop: true
      })
    } else {
      this.setState({
        isShowToTop: false
      })
    }
  }

  /************************************************************ _scrollTop ***********************************************************/
  _scrollTop = () => {
    this.refs.promotion.scrollToOffset({ offset: 0, animated: true })
  }

  /************************************************************ openModal ***********************************************************/
  openModal(last_id) {
    const modalData = this.state.lasterData.find(element => {
      return element.last_id === last_id
    })
    this.setState({
      modalVisible: true,
      modalData: { image: modalData.last_path, title: modalData.last_name }
    });
  };

  /************************************************************ closeImage ***********************************************************/
  closeImage = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false
    })
  }

  /************************************************************ goEnquiry ***********************************************************/
  goEnquiry(image, title) {
    const navigateActions = this.props.navigation.navigate;
    const token = this.props.screenProps.mmidToken;
    let url = 'https://apps.idealtech.com.my/api/member_enquiry_add.php?';
    let imagePath = image;
    let itemTitle = title
    let that = this;

    this.setState({
      modalVisible: false,
    })

    //console.log(token, 'feedback token')

    RNFetchBlob.fetch('POST', url, {
      Authorization: "Bearer access-token",
      otherHeader: "foo",
      'Content-Type': 'multipart/form-data',
    }, [
        { name: 'MMID', data: token },
        { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
        { name: 'message', data: itemTitle },
        { name: 'item', data: imagePath }
      ]).then((res) => res.json())
      .then((myJson) => {
        //console.log('myJson', myJson);
        if (myJson[0].status === 1) {
          navigateActions('enquiry')
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => that.setState({
                  modalVisible: false,
                })
              },
            ],
            { cancelable: false },
          );
        }
      }).catch((err) => {
        console.log('error upload img', err);
      })
  }

  render() {
    const { lasterData, isShowToTop, renderPage, isLoading, modalVisible, modalData } = this.state;
    const token = this.props.screenProps.mmidToken
    //console.log('lasterData', lasterData)
    return (
      <View style={{ backgroundColor: '#EFEFEF', flex: 1 }}>
        {/**************************************** Promotion Products ***************************************/}
        {
          renderPage &&
            isLoading ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" animating={isLoading} />
            </View>
            :
            <FlatList
              data={lasterData}
              renderItem={this.renderItemLaster}
              numColumns={numColumns}
              style={styles.Flatcontainer}
              // onEndReached={this.handleLoadMore}
              // onEndReachedThreshold={0.5}
              ListFooterComponent={lasterData.length > 19 ? this.renderFooter : null}
              ListEmptyComponent={this.ListEmpty}
              ref="promotion"
            // onScroll={this.handleScroll}
            // contentContainerStyle={{
            //   paddingBottom: 50
            // }}

            />
        }
        {/************************** scroll top buttom ****************************/}
        {
          isShowToTop ?
            <TouchableOpacity onPress={this._scrollTop} style={styles.scrollBtn}>
              <ANT name='up' style={styles.iconTop} />
            </TouchableOpacity>
            : null
        }

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeImage} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>

            <View style={{ borderBottomWidth: 3, borderBottomColor: '#fff', width: '100%', marginTop: 40 }}>
              <Text style={styles.titlemodal}>{modalData.title}</Text>
            </View>

            <ImageZoom
              cropWidth={Dimensions.get('window').width}
              cropHeight={Dimensions.get('window').height - 300}
              imageWidth={Dimensions.get('window').width}
              imageHeight={'100%'}
              pinchToZoom={true}
              panToMove={true}
              enableSwipeDown={true}
              enableCenterFocus={true}
              style={{ backgroundColor: '#000', paddingTop: 20 }}
            >
              <Image source={{ uri: modalData.image }} style={styles.imgModal} />
            </ImageZoom>

            {
              token === null ? null :
                <View style={{ marginTop: 30, }}>
                  <Button rounded primary onPress={() => this.goEnquiry(modalData.image, modalData.title)}>
                    <Text style={{ fontWeight: 'bold' }}>ASK ME ?</Text>
                  </Button>
                  {/* <Text style={{ color: '#fff', padding: 20, fontWeight: 'bold', marginTop: 20, fontSize: 16 }}>ASK ME ?</Text> */}
                </View>
            }
          </View>
        </Modal>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  loadingFooter: { marginTop: 10, alignItems: 'center' },
  MainContainer: { justifyContent: 'center', marginTop: 200, },
  scrollBtn: {
    position: 'absolute', flex: 0.1, right: 10, bottom: -10, borderRadius: 50,
    backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
    color: 'red', textAlign: 'center'
  },
  iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: 'gray', alignItems: 'center', justifyContent: 'center' },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: '100%', resizeMode: 'contain', },
  titlemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 22, backgroundColor: '#333',
    width: '100%', textAlign: 'center',
  },

});

export default LatestProductScreen;
