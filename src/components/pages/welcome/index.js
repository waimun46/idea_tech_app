import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Dimensions, Image, TouchableOpacity } from 'react-native';
import ANT from 'react-native-vector-icons/AntDesign';
import Carousel from 'react-native-looped-carousel';
import logo from '../../../assets/logo/logowhite.png';
import { FetchApiNoParam } from '../../api';

const { width, height } = Dimensions.get('window');


class WelcomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      size: { width, height },
      imageData: []
    }
  }

  /********************************************************** componentDidMount ************************************************************/
  componentDidMount() {
    this.fetchWelcome()
  }

  /********************************************************** fetchWelcome ************************************************************/
  fetchWelcome() {
    let path = "landing";
    let ApiData = FetchApiNoParam(path);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          imageData: fetchData,
        })
      })
  }

  /********************************************************** renderPage slider ************************************************************/
  renderPage(item, i) {
    const { size } = this.state;
    return (
      <View style={[size]} key={i}>
        <Image source={{ uri: item.land_path }} style={styles.imgcontain} />
        {/* <View style={styles.textwarper}>
          <Text style={styles.texttitle}>{item.title}</Text>
        </View> */}
      </View>
    );
  }



  render() {
    const { size, imageData } = this.state;
    /********************************* Carousel array fix crash ******************************/
    const images = imageData;
    if (!images || images.length === 0) {
      return null;
    }
    // console.log('imageData', imageData)

    return (
      <View>
        <Carousel
          delay={3000}
          style={size}
          onAnimateNextPage={(p) => console.log(p)}
          pageInfo
          bullets
          autoplay
          bulletsContainerStyle={{ bottom: 100 }}
          bullets={false}
          pageInfo={false}
        >
          {
            images.map((item, i) => this.renderPage(item, i))
          }
        </Carousel>

        <View style={styles.nextwarp} >
          <TouchableOpacity style={styles.presswarp} onPress={() => this.props.navigation.navigate('home')}>
            <Text style={styles.textsty}>Next</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.textenterwarp} >
          <View style={styles.textenterwarpinner}>
            <Image source={logo} style={styles.logo} />
          </View>
        </View>

      </View>
    )
  }

}




const styles = StyleSheet.create({
  imgcontain: { width: "100%", height: '100%', resizeMode: 'cover', },
  textwarper: { position: 'absolute', top: '30%', padding: 20 },
  texttitle: {
    color: '#fff', fontSize: 40, fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 2, textShadowColor: '#000',
  },
  textenterwarp: { position: 'absolute', bottom: 30, width: '100%', },
  textenterwarpinner: { alignItems: 'flex-end', paddingRight: 10 },
  logo: { width: 100, height: 100, resizeMode: 'contain', },
  nextwarp: { position: 'absolute', top: 50, width: '100%' },
  presswarp: { alignItems: 'flex-end' },
  textsty: { color: '#fff', fontWeight: 'bold', fontSize: 20, paddingRight: 15 }
});

export default WelcomeScreen;