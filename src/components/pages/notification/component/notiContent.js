import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, } from 'react-native';
import { H3, Text } from 'native-base';
import { FetchApi } from '../../../api';
import HTML from 'react-native-render-html';



class NotiContent extends Component {
  static navigationOptions = {
    title: 'Notification',
  };

  constructor(props) {
    super(props);
    this.state = {
      notiContent: []
    };
  }

  /************************************************* componentDidMount ***************************************************/
  componentDidMount() {
    this.fetchArticleContent();
  }

  /************************************************* fetchArticleContent ***************************************************/
  fetchArticleContent() {
    let path = "notification";
    let parameters = { noti_id: this.props.navigation.state.params.notiID }
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          notiContent: fetchData[0]
        })
      })
  }



  render() {
    const { notiContent } = this.state;
    const htmlContent = notiContent.noti_content;
    let navigateActionsData = this.props.navigation.state.params;
    console.log(navigateActionsData.notiID, 'notiID')
    console.log(notiContent, 'notiContent-----')
    return (
      <ScrollView>
        <View>
          <Image source={{ uri: notiContent.noti_path }} style={styles.imgSty} />
        </View>
        <View style={styles.container}>
          <View style={styles.titleWarp}>
            <H3 style={styles.title}>{notiContent.noti_title}</H3>
            <Text note style={{ marginTop: 10 }}>{notiContent.create_dt}</Text>
          </View>
          {/* <HTML
            html={htmlContent}
            tagsStyles={{
              img: { width: '100%', resizeMode: 'contain' },
              // p: {padding: 20,}
            }}
            ignoredStyles={['width', 'height']}
          /> */}
          <Text style={{ padding: 20, paddingTop: 0, color: 'gray' }}>{notiContent.noti_content}</Text>
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  imgSty: { width: '100%', height: 200, resizeMode: 'cover' },
  container: { marginTop: 30, marginBottom: 30 },
  titleWarp: { paddingRight: 20, paddingLeft: 20, marginBottom: 20 },
  title: { fontWeight: 'bold', lineHeight: 28 },
  subcontentSty: { marginTop: 10, color: '#444444' },
  flatlistContent: { color: '#444444' },
  flatlistTitle: { fontWeight: 'bold', marginBottom: 10, },

});


export default NotiContent;
