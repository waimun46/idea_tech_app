import React, { Component } from 'react';
import { View, FlatList, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { FetchApiNoParam, FetchApi } from '../../api';
import ANT from 'react-native-vector-icons/AntDesign';

class NotificationScreen extends Component {
  static navigationOptions = {
    title: 'Notification Center',
  };

  constructor(props) {
    super(props);
    this.state = {
      notificationData: [],
      offsetValue: 1,
      loadingFetch: false,
      isShowToTop: false,
      renderPage: false,
      isLoading: true,
    };
  }

  /************************************************* componentDidMount ***************************************************/
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
    this.fetchNotificationData();
  }

  /************************************************* fetchArticleData ***************************************************/
  fetchNotificationData() {
    let path = "notification_list";
    let parameters = { limit: '20', offset: this.state.offsetValue }
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          notificationData: this.state.notificationData.concat(fetchData),
          loadingFetch: false,
          isLoading: false,
          loadingScroll: false,
        })
      })
  }

  /******************************************************** handleLoadMorePromotion ********************************************************/
  handleLoadMore = () => {
    console.warn('handleLoadMore');
    this.setState({
      offsetValue: this.state.offsetValue + 1,
      loadingFetch: true,
    }, this.fetchNotificationData)

  }

  /************************************************************ ListEmpty ***********************************************************/
  handleScroll = (e) => {
    //console.log(e.nativeEvent.contentOffset.y);
    let offsetY = e.nativeEvent.contentOffset.y;
    if (offsetY > 50) {
      this.setState({
        isShowToTop: true
      })
    } else {
      this.setState({
        isShowToTop: false
      })
    }
  }

  /************************************************************ _scrollTop ***********************************************************/
  _scrollTop = () => {
    this.refs.article.scrollToOffset({ offset: 0, animated: true })
  }

  /******************************************************** renderFooter ********************************************************/
  renderFooter = () => {
    return (
      <TouchableOpacity style={{ marginTop: 20 }} onPress={this.handleLoadMore}>
        <View style={[styles.loadingFooter]}>
          {
            this.state.loadingFetch ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                <ActivityIndicator color="#ccc" />
              </View>
              : null
          }
          <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
        </View>
      </TouchableOpacity>
    )
  }


  /************************************************************ ListEmpty ***********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>No data</Text>
      </View>
    );
  };

  /************************************************* _renderItem ***************************************************/
  _renderItem = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <List style={{ backgroundColor: '#fff' }}>
        <ListItem thumbnail onPress={() => navigateActions('notificationContent', { notiID: item.noti_id })}>
          <Left>
            <Thumbnail square source={{ uri: item.noti_path }} />
          </Left>
          <Body>
            <Text style={{ marginBottom: 5 }}>{item.noti_title}</Text>
            <View style={{ flexDirection: 'row', marginBottom: 3 }}>
              <Text note style={{ fontSize: 12 }}>Create Date :</Text>
              <Text note style={{ fontSize: 12 }}>{item.create_dt}</Text>
            </View>
            {/* <View style={{ flexDirection: 'row' }}>
              <Text note style={{ fontSize: 12 }}>Update Date :</Text>
              <Text note style={{ fontSize: 12 }}>{item.update_dt}</Text>
            </View> */}
          </Body>
          <Right>
            <ANT name="right" size={15} style={{ color: '#ccc' }} />
          </Right>
        </ListItem>
      </List>

    )
  }

  render() {
    const { notificationData, isShowToTop, renderPage, isLoading } = this.state;
    console.log('notificationData', notificationData)
    return (
      renderPage &&
        isLoading ?
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="large" animating={isLoading} />
        </View>
        :
        <View style={{ backgroundColor: '#EFEFEF', flex: 1 }}>
          <FlatList
            data={notificationData}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            // onEndReached={this.handleLoadMore}
            // onEndReachedThreshold={0.5}
            ListFooterComponent={notificationData.length > 19 ? this.renderFooter : null}
            ListEmptyComponent={this.ListEmpty}
            ref="article"
            onScroll={this.handleScroll}
            contentContainerStyle={{ paddingBottom: 30, }}
          />

          {/************************** scroll top buttom ****************************/}
          {
            isShowToTop ?
              <TouchableOpacity onPress={this._scrollTop} style={styles.scrollBtn}>
                <ANT name='up' style={styles.iconTop} />
              </TouchableOpacity>
              : null
          }
        </View>
    );
  }
}

const styles = StyleSheet.create({
  loadingFooter: { marginTop: 30, alignItems: 'center', marginBottom: 100 },
  MainContainer: { justifyContent: 'center', marginTop: 200, },
  scrollBtn: {
    position: 'absolute', flex: 0.1, right: 10, bottom: -10, borderRadius: 50,
    backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
    color: 'red', textAlign: 'center'
  },
  iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: 'gray', alignItems: 'center', justifyContent: 'center' },



});

export default NotificationScreen;
