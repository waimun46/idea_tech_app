import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, ScrollView, FlatList, ActivityIndicator, Modal } from 'react-native';
import { Card, Text, Tab, Tabs, ScrollableTab, Button } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar } from 'react-native-scrollable-tab-view-forked';
import ImageZoom from 'react-native-image-pan-zoom';
import { FetchApi } from '../../api';
import test from '../../../assets/images/test02.jpeg';
import RNFetchBlob from 'rn-fetch-blob';

const numColumns = 2;


class ProductsScreen extends Component {
  static navigationOptions = {
    title: 'Our Products',
  };

  constructor(props) {
    super(props);
    this.state = {
      promotionData: [],
      lasterData: [],
      offsetValue: 1,
      offsetValuePromo: 1,
      loadingScroll: false,
      dataEmpty: [],
      isShowToTop: false,
      activeKey: '',
      renderPage: false,
      isLoading: true,
      modalVisible: false,
      modalData: { image: '', title: '' },
      activeTabs: ''
    }

  }

  /********************************************************** componentDidMount ************************************************************/
  componentDidMount() {
    setTimeout(() => { this.setState({ renderPage: true }) }, 0);
    this.fetchPromotionData();
    this.fetchLasterData();

  }

  /********************************************************** fetchLasterData ************************************************************/
  fetchLasterData() {
    let path = "latest_product";
    let parameters = { limit: '20', offset: this.state.offsetValue }
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          lasterData: this.state.lasterData.concat(fetchData),
          loadingScroll: false,
          dataEmpty: fetchData[0],
          isLoading: false
        })
      })
  }

  /********************************************************** fetchPromotionData ************************************************************/
  fetchPromotionData() {
    let path = "promotion";
    let parameters = { limit: '20', offset: this.state.offsetValuePromo }
    let ApiData = FetchApi(path, parameters);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          promotionData: this.state.promotionData.concat(fetchData),
          loadingScroll: false,
          dataEmpty: fetchData[0],
        })
      })
  }

  /************************************************* Render Item in renderItemLaster *************************************************/
  renderItemLaster = ({ item }) => {
    return (
      <Card style={{ width: '50%', padding: 1, marginLeft: 0, marginRight: 0 }}>
        <TouchableOpacity onPress={() => this.openImageProduct(item.last_id)}>
          <View style={{}}>
            <Image source={{ uri: item.last_path }} style={{ height: 250, width: '100%', resizeMode: 'cover' }} />
          </View>
          <View style={{ backgroundColor: '#000', padding: 10, }}>
            <Text style={{ textAlign: 'center', color: '#fff', fontWeight: 'bold' }} numberOfLines={1}>{item.last_name}</Text>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  /********************************************************* Render Item in renderItemPromotion *************************************************/
  renderItemPromotion = ({ item }) => {
    return (
      <Card style={{ width: '50%', padding: 1, marginLeft: 0, marginRight: 0 }}>
        <TouchableOpacity onPress={() => this.openImagePromotion(item.promo_id)}>
          <View style={{ paddingTop: 10, }}>
            <Text note style={{ textAlign: 'center', color: 'red', fontWeight: 'bold', marginBottom: item.promo_frmDt === "" ? 0 : 5 }}>Promotion Sales</Text>
            <Text note style={{ textAlign: 'center', color: 'gray', }}>{item.promo_frmDt} {item.promo_frmDt === "" ? null : '-'} {item.promo_toDt}</Text>
          </View>
          <View style={{}}>
            <Image source={{ uri: item.promo_path }} style={{ height: 250, width: '100%', resizeMode: 'cover' }} />
          </View>
          <View style={{ backgroundColor: '#000', padding: 10 }}>
            <Text style={{ textAlign: 'center', color: '#fff', fontWeight: 'bold' }} numberOfLines={1}>{item.promo_name}</Text>
          </View>
        </TouchableOpacity>
      </Card>
    )
  }

  /******************************************************** handleLoadMorePromotion ********************************************************/
  handleLoadMorePromotion = () => {
    console.warn('handleLoadMore---PromotionData');
    this.setState({
      offsetValuePromo: this.state.offsetValuePromo + 1,
      loadingScroll: true
    }, this.fetchPromotionData)
    // setTimeout(() => {
    //   this.setState({
    //     offset: this.state.offset + 1,
    //     loadingScroll: true
    //   }, this.fetchLasterData)

    // }, 1000)

  }

  /******************************************************** handleLoadMoreProductData ********************************************************/
  handleLoadMoreProductData = () => {
    console.warn('handleLoadMore---ProductData');
    this.setState({
      offsetValue: this.state.offsetValue + 1,
      loadingScroll: true
    }, this.fetchLasterData)

    // setTimeout(() => {
    //   this.setState({
    //     offset: this.state.offset + 1,
    //     loadingScroll: true
    //   }, this.fetchLasterData)

    // }, 1000)

  }

  /******************************************************** renderFooterProductData ********************************************************/
  renderFooterProductData = () => {
    return (
      <TouchableOpacity style={{}} onPress={this.handleLoadMoreProductData}>
        <View style={[styles.loadingFooter]}>
          {
            this.state.loadingScroll ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                <ActivityIndicator color="#ccc" />
              </View>
              : null
          }
          <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
        </View>
      </TouchableOpacity>
    )
  }

  /******************************************************** renderFooterPromotionData ********************************************************/
  renderFooterPromotionData = () => {
    return (
      <TouchableOpacity style={{}} onPress={this.handleLoadMorePromotion}>
        <View style={[styles.loadingFooter]}>
          {
            this.state.loadingScroll ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                <ActivityIndicator color="#ccc" />
              </View>
              : null
          }
          <Text style={styles.TextMore}>View More<ANT name='right' size={15} /></Text>
        </View>
      </TouchableOpacity>
    )
  }

  /************************************************************ ListEmpty ***********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>No data</Text>
      </View>
    );
  };

  /************************************************************ activeKey ***********************************************************/
  activeKey(obj) {
    console.log('activeKey----', obj)
    this.setState({
      activeKey: obj.i
    })
  }

  /************************************************************ ListEmpty ***********************************************************/
  handleScroll = (e) => {
    //console.log(e.nativeEvent.contentOffset.y);
    let offsetY = e.nativeEvent.contentOffset.y;
    if (offsetY > 50) {
      this.setState({
        isShowToTop: true
      })
    } else {
      this.setState({
        isShowToTop: false
      })
    }
  }

  /************************************************************ _scrollTop ***********************************************************/
  _scrollTop = () => {
    // this.refs.product.scrollToOffset({ offset: 0, animated: true })
    if (this.state.activeKey === 1) {
      this.refs.promotion.scrollToOffset({ offset: 0, animated: true })
    } else {
      this.refs.product.scrollToOffset({ offset: 0, animated: true })
    }
  }

  /************************************************************ openImageProduct ***********************************************************/
  openImageProduct(last_id) {
    const modalData = this.state.lasterData.find(element => {
      return element.last_id === last_id
    })
    this.setState({
      modalVisible: true,
      modalData: { image: modalData.last_path, title: modalData.last_name }
    });
  };

  /************************************************************ openImagePromotion ***********************************************************/
  openImagePromotion(promo_id) {
    const modalData = this.state.promotionData.find(element => {
      return element.promo_id === promo_id
    })
    this.setState({
      modalVisible: true,
      modalData: { image: modalData.promo_path, title: modalData.promo_name }
    });
  };

  /************************************************************ closeImage ***********************************************************/
  closeImage = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false
    })
  }

  /************************************************************ onChangeTab ***********************************************************/
  onChangeTab(obj) {
    console.log('change', obj);
    this.setState({
      activeTabs: obj.ref.props.heading,
    })
  }

  /************************************************************ goEnquiry ***********************************************************/
  goEnquiry(image, title) {
    const navigateActions = this.props.navigation.navigate;
    const token = this.props.screenProps.mmidToken;
    let url = 'https://apps.idealtech.com.my/api/member_enquiry_add.php?';
    let imagePath = image;
    let itemTitle = title
    let that = this;

    this.setState({
      modalVisible: false,
    })

    //console.log(token, 'feedback token')

    RNFetchBlob.fetch('POST', url, {
      Authorization: "Bearer access-token",
      otherHeader: "foo",
      'Content-Type': 'multipart/form-data',
    }, [
        { name: 'MMID', data: token },
        { name: 'security_code', data: '90af0fc8532a32f41b69d7097f2f1ca6' },
        { name: 'message', data: itemTitle },
        { name: 'item', data: imagePath }
      ]).then((res) => res.json())
      .then((myJson) => {
        //console.log('myJson', myJson);
        if (myJson[0].status === 1) {
          navigateActions('enquiry')
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => that.setState({
                  modalVisible: false,
                })
              },
            ],
            { cancelable: false },
          );
        }
      }).catch((err) => {
        console.log('error upload img', err);
      })
  }



  render() {
    const { promotionData, lasterData, dataEmpty, activeKey, isShowToTop, renderPage, isLoading, activeTabs, modalData, modalVisible } = this.state;
    const token = this.props.screenProps.mmidToken
    // console.log('promotionData', promotionData.length)
    // console.log('lasterData', lasterData.length)
    // console.log('activeTabs', activeTabs)
    //console.log('token---product', token)

    return (

      <View style={{ backgroundColor: '#EFEFEF', flex: 1, }}>

        {/**************************************** Laster Products ***************************************/}
        {
          renderPage &&
            isLoading ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" animating={isLoading} />
            </View>
            :
            // <FlatList
            //   data={lasterData}
            //   renderItem={this.renderItemLaster} 
            //   numColumns={numColumns}
            //   style={styles.Flatcontainer}
            //   // onEndReached={this.handleLoadMore}
            //   // onEndReachedThreshold={0.5}
            //   ListFooterComponent={this.renderFooter}
            //   ListEmptyComponent={this.ListEmpty}
            //   ref="product"
            //   onScroll={this.handleScroll}
            //   // contentContainerStyle={{
            //   //   paddingBottom: 50
            //   // }}
            // />

            // <ScrollableTabView
            //   renderTabBar={() => (
            //     <DefaultTabBar
            //       style={styles.scrollStyle}
            //       tabStyle={styles.tabStyle}
            //     />
            //   )}
            //   tabBarTextStyle={styles.tabBarTextStyle}
            //   tabBarInactiveTextColor={'black'}
            //   tabBarActiveTextColor={'#2979FF'}
            //   tabBarUnderlineStyle={styles.underlineStyle}
            //   initialPage={0}
            //   style={{ width: '100%', }}
            //   onChangeTab={(obj) => this.activeKey(obj)}
            // >

            //   <View key={'1'} tabLabel='Laster Products' >
            // <FlatList
            //   data={lasterData}
            //   renderItem={this.renderItemLaster}
            //   numColumns={numColumns}
            //   style={styles.Flatcontainer}
            //   // onEndReached={this.handleLoadMore}
            //   // onEndReachedThreshold={0}
            //   ListFooterComponent={this.renderFooter}
            //   ListEmptyComponent={this.ListEmpty}
            //   ref="product"
            //   // onScroll={this.handleScroll}
            //   contentContainerStyle={{
            //     paddingBottom: 20
            //   }}
            //   // initialNumToRender={0}
            // />
            //   </View>

            //   <View key={'2'} tabLabel='Promotion Products' >
            //     {/* {
            //       dataEmpty.empty ? (
            //         <View style={styles.MainContainer}>
            //           <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
            //           <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>List Empty</Text>
            //         </View>
            //       ) : (
            //           <FlatList
            //             data={promotionData}
            //             renderItem={this.renderItemPromotion}
            //             numColumns={numColumns}
            //             style={styles.Flatcontainer}
            //             onEndReached={this.handleLoadMorePromotion}
            //             onEndReachedThreshold={0}
            //             ListFooterComponent={this.renderFooter}
            //             contentContainerStyle={{
            //               marginBottom: 50
            //             }}
            //           />
            //         )
            //     } */}
            // <FlatList
            //   data={promotionData}
            //   renderItem={this.renderItemPromotion}
            //   numColumns={numColumns}
            //   style={styles.Flatcontainer}
            //   // onEndReached={this.handleLoadMore}
            //   // onEndReachedThreshold={0}
            //   ListFooterComponent={this.renderFooter}
            //   ListEmptyComponent={this.ListEmpty}
            //   ref="promotion"
            //   // onScroll={this.handleScroll}
            //   contentContainerStyle={{
            //     paddingBottom: 20
            //   }}
            //   // initialNumToRender={0}
            // />
            //   </View>

            // </ScrollableTabView>
            <Tabs renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#fff' }} />}>
              <Tab heading="Laster Products" tabStyle={{ backgroundColor: '#fff' }}  >
                <FlatList
                  data={lasterData}
                  renderItem={this.renderItemLaster}
                  numColumns={numColumns}
                  style={styles.Flatcontainer}
                  // onEndReached={this.handleLoadMore}
                  // onEndReachedThreshold={0}
                  ListFooterComponent={lasterData.length > 19 ? this.renderFooterProductData : null}
                  ListEmptyComponent={this.ListEmpty}
                  ref="product"
                  // onScroll={this.handleScroll}
                  contentContainerStyle={{
                    paddingBottom: 20,
                  }}
                // initialNumToRender={0}
                />
              </Tab>
              <Tab heading="Promotion Products" tabStyle={{ backgroundColor: '#fff' }} >
                <FlatList
                  data={promotionData}
                  renderItem={this.renderItemPromotion}
                  numColumns={numColumns}
                  style={styles.Flatcontainer}
                  // onEndReached={this.handleLoadMore}
                  // onEndReachedThreshold={0}
                  ListFooterComponent={promotionData.length > 19 ? this.renderFooterPromotionData : null}
                  ListEmptyComponent={this.ListEmpty}
                  ref="promotion"
                  // onScroll={this.handleScroll}
                  contentContainerStyle={{
                    paddingBottom: 20
                  }}
                // initialNumToRender={0}
                />
              </Tab>
            </Tabs>
        }


        {/************************** scroll top buttom ****************************/}
        {
          isShowToTop ?
            <TouchableOpacity onPress={this._scrollTop} style={styles.scrollBtn}>
              <ANT name='up' style={styles.iconTop} />
            </TouchableOpacity>
            : null
        }

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeImage} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>

            <View style={{ borderBottomWidth: 3, borderBottomColor: '#fff', width: '100%', marginTop: 40 }}>
              <Text style={styles.titlemodal}>{modalData.title}</Text>
            </View>

            <ImageZoom
              cropWidth={Dimensions.get('window').width}
              cropHeight={Dimensions.get('window').height - 300}
              imageWidth={Dimensions.get('window').width}
              imageHeight={'100%'}
              pinchToZoom={true}
              panToMove={true}
              enableSwipeDown={true}
              enableCenterFocus={true}
              style={{ backgroundColor: '#000', paddingTop: 20 }}
            >
              <Image source={{ uri: modalData.image }} style={styles.imgModal} />
            </ImageZoom>

            {
              token === null ? null :
                <View style={{ marginTop: 30, }}>
                  <Button rounded primary onPress={() => this.goEnquiry(modalData.image, modalData.title)}>
                    <Text style={{ fontWeight: 'bold' }}>ASK ME ?</Text>
                  </Button>
                  {/* <Text style={{ color: '#fff', padding: 20, fontWeight: 'bold', marginTop: 20, fontSize: 16 }}>ASK ME ?</Text> */}
                </View>
            }
          </View>
        </Modal>

      </View >

    )
  }

}




const styles = StyleSheet.create({
  activeTabStyle: { color: '#2979FF' },
  tabStyle: { paddingRight: 0, width: '100%', color: '#2979FF', },
  scrollStyle: {
    backgroundColor: 'white', width: '100%', paddingRight: 0, height: 50
    // justifyContent: 'center',
  },
  tabBarTextStyle: { fontSize: 14, fontWeight: 'bold', lineHeight: 50 },
  underlineStyle: { height: 3, backgroundColor: '#2979FF', borderRadius: 3, width: 200, },
  slide: { width: '100%', height: '100%', backgroundColor: '#fff', justifyContent: 'center', },
  imageSty: { width: '100%', height: 350, resizeMode: 'contain' },
  // Flatcontainer: { marginVertical: 10, },
  loadingFooter: { marginTop: 30, alignItems: 'center', marginBottom: 20 },
  MainContainer: { justifyContent: 'center', marginTop: 200, },
  scrollBtn: {
    position: 'absolute', flex: 0.1, right: 10, bottom: -10, borderRadius: 50,
    backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
    color: 'red', textAlign: 'center'
  },
  iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: 'gray', alignItems: 'center', justifyContent: 'center' },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: '100%', resizeMode: 'contain', },
  titlemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 22, backgroundColor: '#333',
    width: '100%', textAlign: 'center',
  },

});

export default ProductsScreen;