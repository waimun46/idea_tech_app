import React, { Component } from 'react';
import { View, FlatList, StyleSheet, TouchableOpacity, Alert, RefreshControl, Modal, ActivityIndicator } from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button, } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import { FetchApi } from '../../api';
import RNRestart from 'react-native-restart';


class MyPost extends Component {
  static navigationOptions = {
    title: 'My Gallery',
  };
  constructor(props) {
    super(props);
    this.state = {
      postList: [],
      refreshing: false,
      isSubmit: false
    };
  }

  /******************************************************* componentDidMount *********************************************************/
  componentDidMount() {
    this.fetchPostList();
  }

  /******************************************************* fetchPostList *********************************************************/
  fetchPostList() {
    const token = this.props.navigation.state.params.token;
    let path = "member_gallery_list";
    let parameters = { MMID: token, };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');
    //console.log(token, 'token')
    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          postList: fetchData,
        })
      })
  }

  /******************************************************* reflashData *********************************************************/
  reflashData() {
    this.setState({
      refreshing: false,
      isSubmit: false,
    })
  }

  /******************************************************* deletePost *********************************************************/
  deletePost(item) {
    console.log('deletePost')
    const token = this.props.navigation.state.params.token;
    let path = "member_gallery_del";
    let parameters = { MMID: token, photo_id: item.photo_id };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');
    //console.log(token, 'token')

    this.setState({
      isSubmit: true
    })

    fetch(ApiData).then((res) => res.json())
      .then((myJson) => {
        console.log(myJson, 'myJson');
        if (myJson[0].status === 1) {
          console.log(myJson, 'remove-------------remove');
          Alert.alert('Remove Successful', '',
            [
              { text: 'OK', onPress: () => this.reflashData() },
            ],
            { cancelable: false },
          );
        }
        else {
          Alert.alert(myJson[0].error, '',
            [
              {
                text: 'OK', onPress: () => this.setState({ isSubmit: false, })
              },
            ],
            { cancelable: false },
          );
        }
      })
  }

  /******************************************************* _renderItem *********************************************************/
  _renderItem = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <List style={{ backgroundColor: '#fff' }}>
        <ListItem thumbnail  onPress={() => navigateActions('galleryDetails', { itemid: item.photo_id })}>
          <Left>
            <Thumbnail square source={{ uri: item.photo }} />
          </Left>
          <Body >
            <Text style={{ marginBottom: 5 }}>{item.title}</Text>
            {/* <View style={{ marginBottom: 5, flexDirection: 'row' }}>
              <Text style={{ color: '#ACACAC', fontWeight: 'bold' }}>Comment :</Text>
              <Text numberOfLines={1} style={{ color: '#0095ff', fontWeight: 'bold' }}>{item.totalComment}</Text>
            </View> */}
            {/* <Text note style={styles.date}>{item.create_dt}</Text> */}
          </Body>
          <Right>
            <TouchableOpacity onPress={this.deletePost.bind(this, item)}>
              <ANT name="closecircle" size={25} style={{ color: 'red' }} />
            </TouchableOpacity>
          </Right>
        </ListItem>
      </List >
    )
  }

  /******************************************************* ListEmpty *********************************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <ANT name="unknowfile1" size={60} style={styles.iconsty} />
        <Text style={styles.emptysty}>Post is Empty</Text>
      </View>
    );
  };


  render() {
    const { postList, refreshing, isSubmit } = this.state;
    //console.log('postList', postList);

    const postEmpty = postList[0] || []

    return (
      <View style={{ backgroundColor: '#EFEFEF', flex: 1 }}>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }
        {/************************** list data ****************************/}
        <FlatList
          data={postList}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{ paddingBottom: 30, }}
          ListEmptyComponent={this.ListEmpty}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.fetchPostList()}
            />}
        />
        {/* {
          postEmpty.empty === "No Record Found." ?
            <View style={styles.MainContainer}>
              <ANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
              <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>Post is Empty</Text>
            </View>
            :
            <FlatList
              data={postList}
              extraData={this.state}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ paddingBottom: 130, }}
              ListEmptyComponent={this.ListEmpty}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={this.fetchPostList()}
                />}
            />
        } */}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: { justifyContent: 'center', flex: 1, alignItems: 'center', marginTop: 200, },
  addComment: {
    position: 'absolute', bottom: 50, right: 20, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 20,
  },
  addBtn: {
    backgroundColor: '#0095ff', width: 50, height: 50, justifyContent: 'center',
    alignItems: 'center', borderRadius: 50
  },
  iconsty: { textAlign: 'center', marginBottom: 10, color: '#ccc' },
  emptysty: { textAlign: 'center', color: '#a9a9a9' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


});

export default MyPost;
