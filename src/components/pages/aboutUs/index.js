import React, { Component } from 'react';
import { View, ImageBackground, StyleSheet, ScrollView, Image, FlatList, Modal } from 'react-native';
import { Text, Card, CardItem, } from 'native-base';
import bg from '../../../assets/background/bgw.jpg';
import label from '../../../assets/images/label.png';
import cer from '../../../assets/images/cer.png';
import ETP from 'react-native-vector-icons/Entypo';
import { FetchApiNoParam, FetchApi } from '../../api';
import HTML from 'react-native-render-html';

const numColumns = 2;

const data = [
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019 sdsdsdsd' },
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019' },
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019' },
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019' },
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019' },
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019' },
  { image: require('../../../assets/images/cer.png'), title: 'INTEL PLATINUM 2019' },
]

class AboutUsScreen extends Component {
  static navigationOptions = {
    title: 'About Us',
  };
  constructor(props) {
    super(props);
    this.state = {
      dataAbout: [],
      dataAwards: [],
      isLoading: false,

    };
  }

  componentDidMount() {
    this.fetchAboutData();
    this.fetchAwardsData()
  }

  /************************************************* fetchArticleData ***************************************************/
  fetchAboutData() {
    let path = "about_us";
    let ApiData = FetchApiNoParam(path);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          dataAbout: fetchData[0],
          isLoading: false,
        })
      })
  }

  /************************************************* fetchAwardsData ***************************************************/
  fetchAwardsData() {
    let path = "awards";
    let ApiData = FetchApiNoParam(path);
    console.log(ApiData, 'ApiData');

    fetch(ApiData).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          dataAwards: fetchData
        })
      })

  }



  /************************************************* _renderItem *************************************************/
  _renderItem = ({ item }) => {
    return (
      <View style={{ padding: 2, width: '50%', marginBottom: 20 }}>
        <Image source={{ uri: item.awar_path }} style={{ width: '100%', resizeMode: 'cover', height: 230 }} />
        <View style={{ backgroundColor: '#dbc895', padding: 10, }}>
          <Text numberOfLines={1} note style={{ textAlign: 'center', flexWrap: 'wrap', flex: 1, color: '#000' }}>{item.awar_title}</Text>
        </View>
      </View>
    )
  }


  render() {
    const { dataAbout, dataAwards, modalVisiblePromo, modalDataPromo } = this.state;
    const htmlContent = dataAbout.article_content;
    console.log('dataAbout', dataAbout)
    console.log('dataAwards', dataAwards)
    return (
      <ScrollView>
        <View style={{ backgroundColor: '#000' }}>
          <ImageBackground source={bg} style={styles.imagebg}>
            <View style={styles.overlay} />
            <View style={styles.content} >
              <Text style={{ color: '#dbc895', fontSize: 28, fontWeight: 'bold', marginBottom: 5, }}>{dataAbout.article_title}</Text>
              <Text style={{ color: '#dbc895' }} note>MISSION, HISTORY & VISION</Text>
            </View>
          </ImageBackground>

          <HTML
            html={htmlContent}
            tagsStyles={{
              img: { width: '100%', resizeMode: 'contain' },
              p: { padding: 20, }
            }}
            ignoredStyles={['width', 'height']}
            classesStyles={{
              about: { padding: 24, paddingBottom: 0, paddingTop: 5 },
              warp: { marginBottom: 34, },
              titlewarp: { fontSize: 26, fontWeight: 'bold', marginBottom: 4, color: '#fafafb' },
              subtitle: { fontSize: 16, color: 'gray', fontWeight: 'bold', },
              content: { marginTop: 8, color: '#a2a7ab' },
              hrstyle: { width: '40%', backgroundColor: '#dbc895', height: 2.4 }
            }}
          />

          <View style={{ marginBottom: 40, paddingLeft: 24, paddingRight: 24, }}>
            <Text style={{ fontWeight: 'bold', marginBottom: 5, textAlign: 'left', fontSize: 26, color: '#fafafb' }}>PORTFOLIO</Text>
            <Text style={{ marginBottom: 5, textAlign: 'left', fontSize: 16, color: 'gray', fontWeight: 'bold', }} >Awards & Achievement</Text>
            <View style={{ height: 2.4, backgroundColor: '#dbc895', width: '40%', }} />
            <Image source={label} style={{ width: '100%', resizeMode: 'contain' }} />
            <FlatList
              data={dataAwards}
              numColumns={numColumns}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          {/* <View style={{ padding: 20, }}>
          <View style={{ marginBottom: 40 }}>
            <Text style={{ fontWeight: 'bold', marginBottom: 5, textAlign: 'left', fontSize: 20, }}>OUR HISTORY</Text>
            <Text style={{ marginBottom: 5, textAlign: 'left', fontSize: 16, color: 'gray', marginBottom: 5 }} >13 Years of Experience</Text>
            <View style={{ height: 2, backgroundColor: '#dbc895', width: '50%', marginBottom: 10 }} />
            <Text note>
              IDEAL TECHNOLOGY TRADING started its business at Malaysia on 5th July 2007, as a trading company that sell’s and distribute Gadgets,
              GPS and Computer Component. Our passion towards computers leads us to be more focus on building Gaming Rigs, so we decide to fully
              focus on Customize PC’s business. After business restructuring, we change our company name to IDEAL TECH PC SDN BHD on 11th March 2014
            </Text>
          </View>

          <View style={{ marginBottom: 40 }}>
            <Text style={{ fontWeight: 'bold', marginBottom: 5, textAlign: 'left', fontSize: 20, }}>THE MISSION</Text>
            <Text style={{ marginBottom: 5, textAlign: 'left', fontSize: 16, color: 'gray', marginBottom: 5 }} >Always Striving For More </Text>
            <View style={{ height: 2, backgroundColor: '#dbc895', width: '50%', marginBottom: 10 }} />
            <Text note>
              Who we are? We are a team of serious gamers and overclockers with passion towards computers and love to own and customize a fast PC.
              With years of experience in this field, we provide professional consultancy services to all of our clients, ensuring they receive the
              best quality products and services, as part of our commitment to our clients.
            </Text>
          </View>

          <View style={{ marginBottom: 40 }}>
            <Text style={{ fontWeight: 'bold', marginBottom: 5, textAlign: 'left', fontSize: 20, }}>PORTFOLIO</Text>
            <Text style={{ marginBottom: 5, textAlign: 'left', fontSize: 16, color: 'gray', marginBottom: 5 }} >Awards & Achievement</Text>
            <View style={{ height: 2, backgroundColor: '#dbc895', width: '50%', }} />
            <Image source={label} style={{ width: '100%', resizeMode: 'contain' }} />
            <FlatList
              data={data}
              numColumns={numColumns}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

        </View> */}
        </View>



      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imagebg: { width: '100%', height: 200, resizeMode: 'cover', },
  // overlay: { backgroundColor: '#000', opacity: 0.25, height: 200, width: '100%', },
  content: { position: 'absolute', height: 200, width: '100%', justifyContent: 'center', alignItems: 'center', },
  Iconabout: { color: '#dbc895', paddingTop: 2, marginRight: 10 },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative', },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: '100%', resizeMode: 'contain', },
  titlemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 22, backgroundColor: '#333',
    width: '100%', textAlign: 'center',
  },
  datemodal: {
    color: '#fff', padding: 20, fontWeight: 'bold', fontSize: 16, backgroundColor: '#333',
    width: '100%', textAlign: 'center',
  },



});

export default AboutUsScreen;
