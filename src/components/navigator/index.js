import React, { Component } from 'react';
import {
  StyleSheet, Text, Dimensions, View, TouchableOpacity, ImageBackground, ScrollView, Image, Button, Modal,
  ActivityIndicator
} from 'react-native';
// import SafeAreaView from 'react-native-safe-area-view';
// import { SafeAreaProvider } from 'react-native-safe-area-context';
import RNRestart from 'react-native-restart';
import { createAppContainer, NavigationActions, StackActions, SafeAreaView, } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems, DrawerActions } from 'react-navigation-drawer';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
import ANT from 'react-native-vector-icons/AntDesign';
import FA from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
///////////////////////////////////////////////////// welcome ///////////////////////////////////////////////////// 
import WelcomeScreen from '../pages/welcome';

///////////////////////////////////////////////////// home ///////////////////////////////////////////////////// 
import HomeScreen from '../pages/home';

///////////////////////////////////////////////////// profile ///////////////////////////////////////////////////// 
import ProfileScreen from '../pages/profile';
import EditProfileScreen from '../pages/profile/component/editProfile';
import ResetPassword from '../pages/profile/component/resetPassword';

///////////////////////////////////////////////////// FeedWall ///////////////////////////////////////////////////// 
import FeedWallScreen from '../pages/feed';

///////////////////////////////////////////////////// Gallery ///////////////////////////////////////////////////// 
import GalleryScreen from '../pages/gallery';
import GalleryDetails from '../pages/gallery/component/galleryDetials';
import GalleryPost from '../pages/gallery/component/galleryPost';

///////////////////////////////////////////////////// Products ///////////////////////////////////////////////////// 
// import ProductsScreen from '../pages/products';
import LatestProductScreen from '../pages/latestProducts';

///////////////////////////////////////////////////// Promotion ///////////////////////////////////////////////////// 
import PromotionScreen from '../pages/promotion';

///////////////////////////////////////////////////// Camera ///////////////////////////////////////////////////// 
import CameraScreen from '../pages/camera';

///////////////////////////////////////////////////// Login ///////////////////////////////////////////////////// 
import LoginScreen from '../pages/login';
import ForgetPassword from '../pages/login/component/forgetPassword';
import VerifyCode from '../pages/login/component/veriflyCode';
import NewPassword from '../pages/login/component/newPassword';
import SmsLogin from '../pages/login/component/smsLogin';
import SmsTacCode from '../pages/login/component/smsTacCode';

///////////////////////////////////////////////////// Signup ///////////////////////////////////////////////////// 
import SignupScreen from '../pages/signup';
import RegisterTacCode from '../pages/signup/component/tacCode';

///////////////////////////////////////////////////// Article ///////////////////////////////////////////////////// 
import ArticleScreen from '../pages/article';
import ArticleContent from '../pages/article/component/articleContent';

///////////////////////////////////////////////////// Feedback ///////////////////////////////////////////////////// 
import FeedbackScreen from '../pages/feedback';
import IssueStatus from '../pages/feedback/component/issue';

///////////////////////////////////////////////////// Purchase ///////////////////////////////////////////////////// 
import PurchaseScreen from '../pages/purchase';
import AddPurchase from '../pages/purchase/component/addPurchase';
import TimeLineStatus from '../pages/purchase/component/timeline';

///////////////////////////////////////////////////// ShowPost ///////////////////////////////////////////////////// 
import MyPost from '../pages/post';

///////////////////////////////////////////////////// Enquiry ///////////////////////////////////////////////////// 
import Enquiry from '../pages/enquiry';

///////////////////////////////////////////////////// AboutUs ///////////////////////////////////////////////////// 
import AboutUsScreen from '../pages/aboutUs';

///////////////////////////////////////////////////// TermsConditions ///////////////////////////////////////////////////// 
import TermsConditions from '../pages/T_C';

///////////////////////////////////////////////////// OfficialSites ///////////////////////////////////////////////////// 
import OfficialSites from '../pages/official';

///////////////////////////////////////////////////// Notification ///////////////////////////////////////////////////// 
import NotificationScreen from '../pages/notification';
import NotiContent from '../pages/notification/component/notiContent';

///////////////////////////////////////////////////// image and icon ///////////////////////////////////////////////////// 
import drawerbg from '../../assets/background/d01.jpg';
import imgNull from '../../assets/icon/non.png';
import imgPadding from '../../assets/icon/error.png';
import imgProcessing from '../../assets/icon/fill.png';
import imgCompleted from '../../assets/icon/ok.png';
import emptyProfile from '../../assets/icon/empty.png';


const heightWindow = Dimensions.get('window').height;


////////////////////////////////////////////////////////////// StyleSheet ////////////////////////////////////////////////////////////// 
const styles = StyleSheet.create({
  menustyWarp: { justifyContent: 'center', flex: 1, paddingLeft: 10, paddingRight: 100, height: '100%', },
  menusty: { color: '#fff', },
  menustyClose: { color: '#fff', paddingLeft: 10, },
  usersty: { color: '#fff', paddingRight: 10 },
  containerDrawer: { width: '100%', height: '100%', resizeMode: 'cover' },
  closeWarp: { height: 100, justifyContent: "center", alignItems: "flex-start", },
  closeText: { color: '#fff', fontSize: 15, padding: 10 },
  logoutWarp: { height: 100, alignItems: "center", backgroundColor: '#000', flexDirection: 'row', },
  activeSty: { width: 18, height: 18, },
  issueStatusIcon: { marginRight: 10, width: 18, height: 18, },
  back: { paddingRight: 70 }
});

////////////////////////////////////////////////////////////// Button drawer menu function ////////////////////////////////////////////////////////////// 
const DrawerButton = (props) => {
  return (
    <View>
      <TouchableOpacity onPress={props.navigation.toggleDrawer} style={styles.menustyWarp}>
        <ANT name="menu-unfold" size={20} style={styles.menusty} />
      </TouchableOpacity>
    </View>
  );
};

////////////////////////////////////////////////////////////// Button user login function ////////////////////////////////////////////////////////////// 
const UserButton = (props, screenProps) => {
  //console.log(props.screenProps.statusIcon, 'statusIcon-navi')
  const statusDisplay = props.screenProps.statusIcon;
  // function logoutFuc() {
  //   //console.log('logoutFuc-navi')
  //   AsyncStorage.removeItem('MMID_TOKEN_LOGIN');
  //   props.navigation.navigate('home', RNRestart.Restart());
  //   props.navigation.dispatch(DrawerActions.closeDrawer())
  // }

  return (
    <View style={{ flexDirection: 'row' }}>
      {
        props.screenProps.mmidToken === null ? null :
          props.screenProps.loadingStatus ? (
            <ActivityIndicator size="small" color="#ccc" style={{ marginRight: 10 }} />
          ) : (
              <TouchableOpacity onPress={() => statusDisplay === 1 || statusDisplay === 2 ? props.navigation.navigate('issue') :
                props.navigation.navigate('feedback')} style={{ paddingLeft: 70 }}
              >
                <Image source={statusDisplay === 1 ? imgPadding : statusDisplay === 2 ? imgProcessing : statusDisplay === 3 ? imgCompleted : imgCompleted}
                  style={[styles.activeSty, { marginRight: props.screenProps.mmidToken === null ? 20 : 10 }]}
                />
              </TouchableOpacity>
            )
      }

      {
        props.screenProps.mmidToken === null ? null :
          <TouchableOpacity onPress={() => props.navigation.navigate('profile')} style={{ paddingLeft: 10 }}>
            <FA name="user" size={20} color='#fff' style={styles.usersty} />
          </TouchableOpacity>
      }

      {
        props.screenProps.mmidToken === null ? (
          <TouchableOpacity onPress={() => props.navigation.navigate('login')} style={{ paddingLeft: 70 }}>
            <FA name="user" size={20} color='#fff' style={styles.usersty} />
          </TouchableOpacity>
        ) : (null
            // <TouchableOpacity onPress={logoutFuc} style={{ paddingLeft: 10 }}>
            //   <ANT name="logout" size={20} color='#fff' style={styles.usersty} />
            // </TouchableOpacity>
          )
      }
    </View >
  );
};

////////////////////////////////////////////////////////////// Button issue status function ////////////////////////////////////////////////////////////// 
const IssueStatusView = (props, screenProps) => {
  const statusDisplay = props.screenProps.statusIcon;
  return (
    <Image source={statusDisplay === 1 ? imgPadding : statusDisplay === 2 ? imgProcessing : statusDisplay === 3 ? imgCompleted : imgCompleted}
      style={styles.issueStatusIcon}
    />
  );
};

////////////////////////////////////////////////////////////// HomeScreenStack ////////////////////////////////////////////////////////////// 
const HomeScreenStack = createStackNavigator({
  // welcome: {
  //   screen: WelcomeScreen,
  //   navigationOptions: {
  //     header: null,
  //     headerStyle: { backgroundColor: '#000', },
  //     headerBackTitle: null,
  //   }
  // },
  home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  product: {
    screen: LatestProductScreen,
  },
  login: {
    screen: LoginScreen,
  },
  smslogin: {
    screen: SmsLogin
  },
  smsTacCode: {
    screen: SmsTacCode
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
  galleryDetails: {
    screen: GalleryDetails,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Gallery Details'
    })
  },
  articlecontent: {
    screen: ArticleContent
  },
  enquiry: {
    screen: Enquiry
  },

}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    }
  });

////////////////////////////////////////////////////////////// AdoutUsScreenStack ////////////////////////////////////////////////////////////// 
const AdoutUsScreenStack = createStackNavigator({
  aboutus: {
    screen: AboutUsScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  editProfile: {
    screen: EditProfileScreen,
  },
  resetpass: {
    screen: ResetPassword
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
  purchase: {
    screen: PurchaseScreen
  },
  addPurchase: {
    screen: AddPurchase
  },
  timeline: {
    screen: TimeLineStatus
  },
  post: {
    screen: MyPost,
  },
  galleryDetails: {
    screen: GalleryDetails,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Gallery Details'
    })
  },
  enquiry: {
    screen: Enquiry
  }
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    },
  });

////////////////////////////////////////////////////////////// TCcreenStack ////////////////////////////////////////////////////////////// 
const TCcreenStack = createStackNavigator({
  terms: {
    screen: TermsConditions,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  editProfile: {
    screen: EditProfileScreen,
  },
  resetpass: {
    screen: ResetPassword
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
  purchase: {
    screen: PurchaseScreen
  },
  addPurchase: {
    screen: AddPurchase
  },
  timeline: {
    screen: TimeLineStatus
  },
  post: {
    screen: MyPost,
  },
  galleryDetails: {
    screen: GalleryDetails,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Gallery Details'
    })
  },
  enquiry: {
    screen: Enquiry
  }
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    },
  });

////////////////////////////////////////////////////////////// OfficialSitesStack ////////////////////////////////////////////////////////////// 
const OfficialSitesStack = createStackNavigator({
  official: {
    screen: OfficialSites,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  editProfile: {
    screen: EditProfileScreen,
  },
  resetpass: {
    screen: ResetPassword
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
  purchase: {
    screen: PurchaseScreen
  },
  addPurchase: {
    screen: AddPurchase
  },
  timeline: {
    screen: TimeLineStatus
  },
  post: {
    screen: MyPost,
  },
  galleryDetails: {
    screen: GalleryDetails,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Gallery Details'
    })
  },
  enquiry: {
    screen: Enquiry
  }
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    },
  });

////////////////////////////////////////////////////////////// ProfileScreenStack ////////////////////////////////////////////////////////////// 
const ProfileScreenStack = createStackNavigator({
  profile: {
    screen: ProfileScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  notification: {
    screen: NotificationScreen,
  },
  notificationContent: {
    screen: NotiContent,
  },
  editProfile: {
    screen: EditProfileScreen,
  },
  resetpass: {
    screen: ResetPassword
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
  purchase: {
    screen: PurchaseScreen
  },
  addPurchase: {
    screen: AddPurchase
  },
  timeline: {
    screen: TimeLineStatus
  },
  post: {
    screen: MyPost,
  },
  galleryDetails: {
    screen: GalleryDetails,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Gallery Details'
    })
  },
  enquiry: {
    screen: Enquiry
  }
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    },
  });

////////////////////////////////////////////////////////////// FeedScreenStack ////////////////////////////////////////////////////////////// 
const FeedScreenStack = createStackNavigator({
  feed: {
    screen: FeedWallScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 1000
    })
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    }
  });

////////////////////////////////////////////////////////////// GalleryScreenStack ////////////////////////////////////////////////////////////// 
const GalleryScreenStack = createStackNavigator({
  gallery: {
    screen: GalleryScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  galleryPost: {
    screen: GalleryPost
  },
  galleryDetails: {
    screen: GalleryDetails,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Gallery Details'
    })
  },

  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    }
  });

////////////////////////////////////////////////////////////// ProductScreenStack ////////////////////////////////////////////////////////////// 
const ProductScreenStack = createStackNavigator({
  product: {
    screen: LatestProductScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
  enquiry: {
    screen: Enquiry
  }
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000' },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    }
  });


////////////////////////////////////////////////////////////// PromotionScreenStack ////////////////////////////////////////////////////////////// 
const PromotionScreenStack = createStackNavigator({
  promotion: {
    screen: PromotionScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },

}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000' },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    }
  });


////////////////////////////////////////////////////////////// ArticleScreenStack ////////////////////////////////////////////////////////////// 
const ArticleScreenStack = createStackNavigator({
  article: {
    screen: ArticleScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <UserButton navigation={navigation} screenProps={screenProps} />,
      minSwipeDistance: 100
    })
  },
  articlecontent: {
    screen: ArticleContent
  },
  login: {
    screen: LoginScreen,
  },
  forgotpass: {
    screen: ForgetPassword
  },
  verifycode: {
    screen: VerifyCode
  },
  newpassword: {
    screen: NewPassword,
  },
  signup: {
    screen: SignupScreen,
  },
  registertaccode: {
    screen: RegisterTacCode
  },
  feedback: {
    screen: FeedbackScreen,
  },
  issue: {
    screen: IssueStatus,
    navigationOptions: ({ navigation, screenProps }) => ({
      headerRight: <IssueStatusView screenProps={screenProps} />,
    })
  },
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#000', },
      headerTintColor: '#fff',
      headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: null,
    }
  })

////////////////////////////////////////////////////////////// CustomDrawerComponent ////////////////////////////////////////////////////////////// 
const CustomDrawerComponent = (props, screenProps) => {
  // console.log(props, 'props-navi')
  // console.log(props.screenProps.mmidToken, 'mmidToken-navi')

  // let profileData = props.screenProps.profile || [];
  // //console.log(profileData, 'profile')

  // function logoutFuc() {
  //   //console.log('logoutFuc-navi')
  //   AsyncStorage.removeItem('MMID_TOKEN_LOGIN');
  //   props.navigation.navigate('home', RNRestart.Restart());
  //   props.navigation.dispatch(DrawerActions.closeDrawer())
  // }

  return (
    // <SafeAreaProvider >
    // <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'never', bottom: 'never' }}>
    <SafeAreaView forceInset={{ top: 'never', bottom: 'never' }}>
      {/* <ImageBackground source={drawerbg} style={styles.containerDrawer} > */}
      <View style={{ height: '100%' }}>
        <View style={styles.closeWarp}>
          <TouchableOpacity onPress={props.navigation.closeDrawer} style={{ flexDirection: 'row', width: '100%' }}>
            <ANT name="close" size={30} style={styles.menustyClose} />
            <Text style={styles.closeText}>Close</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          <DrawerNavigatorItems {...props}
            onItemPress={({ route, focused }) => {
              if (!focused) {
                setTimeout(() => {
                  props.navigation.navigate(route.routeName)
                }, 0)
              }
              // props.navigation.navigate('DrawerClose');
            }}
          />
        </ScrollView>
        {/* {
          props.screenProps.mmidToken === null ? (
            null
          ) : (
              <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                <View style={[styles.logoutWarp,]}>
                  <View style={{ width: '70%', paddingLeft: 30 }}>
                    <TouchableOpacity onPress={logoutFuc}>
                      <Text style={{ color: '#fff', fontSize: 25, marginBottom: 5 }} >Logout</Text>
                    </TouchableOpacity>
                    <Text style={{ color: '#ea8c8c', fontWeight: 'bold' }}>{profileData.name}</Text>
                  </View>
                  <View style={{ width: '30%' }}>
                    {
                      profileData.photo === "" || profileData.photo === null ?
                        <Image source={emptyProfile} style={{ width: '100%', height: '100%' }} />
                        :
                        <Image source={{ uri: profileData.photo }} style={{ width: '100%', height: '100%' }} />
                    }
                  </View>
                </View>
              </View>
            )
        } */}
      </View>
      {/* </ImageBackground > */}
    </SafeAreaView>
    // </SafeAreaProvider>

  )

};



////////////////////////////////////////////////////////////// AppNavigator ////////////////////////////////////////////////////////////// 
const AppNavigator = createDrawerNavigator({
  home: {
    name: 'Home',
    screen: HomeScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Home',
    })
  },
  profile: {
    name: 'Profile',
    screen: ProfileScreenStack,
    navigationOptions: () => ({
      drawerLabel: () => null,
    })
  },
  about: {
    name: 'About Us',
    screen: AdoutUsScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'About Us',
    })
  },
  product: {
    name: 'Product',
    screen: ProductScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Latest Products',
    })
  },
  promotion: {
    name: 'Promotion',
    screen: PromotionScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Promotion',
    })
  },
  gallery: {
    name: 'Gallery',
    screen: GalleryScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Our Memories',
    })
  },
  article: {
    name: 'Article',
    screen: ArticleScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Article',
    })
  },
  feed: {
    name: 'Feed',
    screen: FeedScreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Feed Wall',
    })
  },
  officialSites: {
    name: 'Official Sites',
    screen: OfficialSitesStack,
    navigationOptions: () => ({
      drawerLabel: 'Official Sites',
    })
  },
  tc: {
    name: 'Terms & Conditions',
    screen: TCcreenStack,
    navigationOptions: () => ({
      drawerLabel: 'Terms & Conditions',
    })
  },
}, {
    initialRouteName: 'home',
    // contentComponent: props => <CustomDrawerComponent {...props} />,
    // contentComponent: props => {
    //   let AuthLogin = Object.assign({}, props);
    //   AuthLogin.items = AuthLogin.items.filter(item => props.screenProps.mmidToken === null ? item.key !== 'profile' : item.key)
    //   return (<CustomDrawerComponent {...AuthLogin} />)
    // },
    // contentComponent: props => {
    //   let AuthLogin = Object.assign({}, props);
    //   AuthLogin.items = AuthLogin.items.filter(item => props.screenProps.mmidToken === null ? item.key !== 'profile' : item.key) 
    //   return (<DrawerNavigatorItems {...AuthLogin} /> )
    // },
    drawerType: 'front',
    drawerBackgroundColor: '#000000a6',
    //lazy: 'false',
    drawerWidth: 328,
    drawerPosition: 'left',
    contentOptions: {
      labelStyle: { color: '#ccc', fontSize: 20, fontWeight: 'normal', paddingLeft: 20, },
      activeLabelStyle: { color: '#0095ff' },
      itemsContainerStyle: { marginTop: 30, },
      itemStyle: { borderBottomWidth: .5, borderBottomColor: '#ffffff2e', }
    }

  })





export default createAppContainer(AppNavigator);