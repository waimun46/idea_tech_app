export function FetchApi(path, parameters) {
  let query = "";
  let security = "90af0fc8532a32f41b69d7097f2f1ca6"
  let url = "http://apps.idealtech.com.my/api/";
  for (let key in parameters) {
    let value = parameters[key];
    query += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
  }
  if (query.length > 0) {
    query = query.substring(0, query.length - 1);
    url = url + path + ".php" + "?" + "security_code" + "=" + security + "&" + query;
  }
  return url;
}


export function FetchApiNoParam(path) {
  let security = "90af0fc8532a32f41b69d7097f2f1ca6"
  let url = "http://apps.idealtech.com.my/api/";
  url = url + path + ".php" + "?" + "security_code" + "=" + security

  return url;
}





