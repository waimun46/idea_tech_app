/**
 * @format
 */

import { AppRegistry, DeviceEventEmitter, Platform } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import Pushwoosh from 'pushwoosh-react-native-plugin';

AppRegistry.registerComponent(appName, () => App);

Pushwoosh.init({
  "pw_appid": "8B806-18686",
  "project_number": "844231435196"
});

Pushwoosh.register();

DeviceEventEmitter.addListener('pushReceived', (e: Event) => {
  console.warn("pushReceived: " + JSON.stringify(e));
  // alert(JSON.stringify(e));
});

// this event is fired when user clicks on notification
DeviceEventEmitter.addListener('pushOpened', (e: Event) => {
  console.warn("pushOpened: " + JSON.stringify(e));
  // alert(JSON.stringify(e));
  // shows a user tapped the notification. Implement user interaction, such as showing push details
});

